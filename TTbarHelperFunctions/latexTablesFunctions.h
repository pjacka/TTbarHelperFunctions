//  Peter Berta, 16.10.2012

#ifndef latexTablesFunctions_h
#define latexTablesFunctions_h 

#include "TString.h"
#include "TH1D.h"
#include "TMatrixT.h"
#include "TGraphAsymmErrors.h"

class HistogramNDto1DConverter;

namespace latexTablesFunctions{

  void printCovarianceLatex(const TMatrixT<double> &matrix, const TString& name,  const TString& fileName);
  void printTableWithDiffCrossSection(const TString& fileName, const TString& variable, TH1D* centralValues, TGraphAsymmErrors* stat, TGraphAsymmErrors* allunc, const TString& normalization="absolute");
  void printTableWithDiffCrossSectionND(const TString& fileName, const TString& variable, TH1D* centralValues, TGraphAsymmErrors* stat, TGraphAsymmErrors* allunc, HistogramNDto1DConverter* histogramConverter, const TString& normalization="absolute");
  TString getDifferentialCrossSectionLatex(const TString& variable, const TString& normalization="absolute");
  
}


#endif
