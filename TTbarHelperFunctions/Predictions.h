#ifndef Predictions_H
#define Predictions_H

#include <vector>
#include <map>
#include <memory>
#include <cfloat>
#include "TString.h"

class TH1D;
class TLegend;
class TGraphAsymmErrors;
class HistogramNDto1DConverter;

class Predictions {
  std::unique_ptr<TH1D> m_data;
  std::vector<std::unique_ptr<TH1D>> m_histos;
  std::vector<std::unique_ptr<TGraphAsymmErrors>> m_graphs;
  std::vector<std::unique_ptr<TGraphAsymmErrors>> m_unc;
  
  std::map<TString,std::vector<TString>> m_legendNames;
  
  public:
  
  Predictions() = default;
  virtual ~Predictions() = default;
  Predictions (const Predictions&) = default;
  Predictions& operator= (const Predictions&) = default;
  Predictions (Predictions&&) = default;
  Predictions& operator= (Predictions&&) = default;
  
  
  Predictions makeRatios(const TH1D* h) const;
  Predictions clone() const;
  std::vector<Predictions> makeProjections(const HistogramNDto1DConverter* histogramConverter) const;
  
  void addHistogram(TH1D*,bool clone=true);
  void addGraph(TGraphAsymmErrors* gr,bool clone=true);
  void addUnc(TGraphAsymmErrors* gr,bool clone=true);
  
  void setData(TH1D* h,bool clone=true);
  void setHistograms(const std::vector<TH1D*>& histos,bool clone=true);
  void setGraphs(const std::vector<TGraphAsymmErrors*>& graphs,bool clone=true);
  void setUnc(const std::vector<TGraphAsymmErrors*>& graphs,bool clone=true);
  
  void setRangeUser(const bool useLogScale=false, const double bottomSpace=0.05,const double upSpace=0.6);
  void setRangeUser(const double ymin, const double ymax);
  void setXtitle(const TString& title);
  void setYtitle(const TString& title);
  
  inline void setLegendNames(const TString& key, const std::vector<TString>& names) {m_legendNames[key] = names;}
  inline void setLegendNames(const std::map<TString,std::vector<TString>>& names) {m_legendNames = names;}
  void fillLegend(TLegend* leg) const;
  
  double getMinimum(const double minVal=-FLT_MAX) const;
  double getMaximum(const double maxVal=FLT_MAX) const;
  inline TH1D* getData() {return m_data.get();}
  inline std::vector<std::unique_ptr<TH1D>>& getHistos() {return m_histos;}
  inline std::vector<std::unique_ptr<TGraphAsymmErrors>>& getGraphs() {return m_graphs;}
  inline std::vector<std::unique_ptr<TGraphAsymmErrors>>& getUnc() {return m_unc;}
  
  ClassDef(Predictions,1)
};

#endif
