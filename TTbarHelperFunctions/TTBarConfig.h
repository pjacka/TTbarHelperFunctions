//  Peter Berta, 15.10.2015

#ifndef TTBarConfig_h
#define TTBarConfig_h

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <math.h>
#include <iomanip>      // std::setprecision
#include <utility>
#include <unordered_map>
#include <map>

#include "TString.h"
#include "TTree.h"
#include "TEnv.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TRandom.h"


enum LargeRJetType {
    kLJET=1,
    kRCJET,
    kVarRCJET
  };


class TTBarConfig {

private:
  std::unique_ptr<TEnv> m_env; // Used to read variables from the config file
  
  bool m_runReco;  // Swith to run over reco level trees
  bool m_runParticle;  // Swith to run over particle level tree
  bool m_runParton;  // Swith to run over truth level tree
  
  bool m_useEWSF;  // Activates electroweak corrections
  bool m_useTruth; // Activates filling truth level spectra and migration matrices
  
  bool m_usePDF; // Switch to activate pdf systematics calculation
  bool m_doSignalModeling; 
  
  bool m_useRecoTruthMatching; // Activates matching between reco and unfolded-level when filling migration matrices
  
  bool m_normalizeToNominalCrossSection;  // Samples will be normalized to fixed cross-section. This aplies to mc_generator_weights as well.
  
  TString m_topPartonsDefinition; // Parton level top/antitop quarks definitions, default is "afterFSR", alternatives: "beforeFSR"
  
  // Names of directories with signal modeling histograms created in unfolding input files 
  TString m_hardScatteringDirectory;
  TString m_partonShoweringDirectory;
  TString m_PhPy8MECoffDirectory;
  TString m_PhH704Directory;
  TString m_FSRupDirectory;
  TString m_FSRdownDirectory;
  TString m_ISRupDirectory;
  TString m_ISRdownDirectory;
  TString m_ISRmuRupDirectory;
  TString m_ISRmuRdownDirectory;
  TString m_ISRmuFupDirectory;
  TString m_ISRmuFdownDirectory;
  TString m_ISRVar3cUpDirectory;
  TString m_ISRVar3cDownDirectory;
  
  std::map<std::string,int> m_FSRbranches;
  std::map<std::string,std::vector<int> > m_ISRbranches;
  std::map<std::string,int> m_PDFbranches;
  
  TString m_qcd_method; // Option for matrix method configuration
  
  TString m_histos1D_config;  // path to config file with 1D reco level histograms definitions
  TString m_histos2D_config;  // path to config file with 2D reco level histograms definitions        
  TString m_unfolding_histos_config;  // path to config file with 1D histograms for unfolding definitions
  TString m_unfolding_histosND_config; // path to config file with ND histograms for unfolding definitions
  bool m_useFineHistograms; //Option to activate histograms with very fine binning
  bool m_fillClosureTestHistos; //Option to add closure test histograms to the output
  
  std::vector<int> m_signalDSIDs;	// DSIDs of signal samples, unfolding corrections and truth level histos will be filled
  std::vector<int> m_singleTopTChanDSIDs;	// DSIDs of single-top t-channel samples
  std::vector<int> m_WtSingleTopDSIDs;	// DSIDs of Wt single-top samples
  std::vector<int> m_ttWDSIDs;		// DSIDs of ttW samples
  std::vector<int> m_ttZDSIDs;		// DSIDs of ttZ samples
  std::vector<int> m_ttHDSIDs;		// DSIDs of ttH samples
  
  
  bool m_applyHtFilterCut;	        // Activates HtFiltering based on filter_HT variable
  std::vector<int> m_samplesToApplyHtFilterCut; // Samples to apply Ht filter cut
  double m_HtFilterCutValue;	                // Ht filter cut value  (filter_HT < cut value)
  
  bool m_useFilterEfficienciesCorrections;   // Activates corrections of filter-HT efficiencies when running with different weights 
  TString m_filterEfficienciesCorrectionsFileName;  // Root file with filter-HT efficiencies corrections
  
  TString m_sumWeightsFile; // File containing sum weights for all MC samples
  
  
  LargeRJetType m_largeRJetType;         // Switch between standard antikt10 jets (LJETS), reclustered jets (RCJETS) and variable-R reclustered jets (VarRCJETS)
  
  bool m_useTrackJets; // Activates track jets
  
  bool m_useVarRCJets;             // Variable-R reclustered jets will be used only if it is set to True
  bool m_useVarRCJetsSubstructure;  // Activates VarRCJETS substructure variables. These variables are not stored in all ntuples. This option must be set to false if they are not available
  TString m_varRCJetBranchName;    // Branch name to be used when VarRCJETS are used. We can store several VarRCJETS branches with different configuration
  TString m_varRCJetsHistString;   // All histogram names with variable-R RC jets variables begin with this string. Useful to control creation of histograms.
  
  
  bool m_useRCJets;                // Reclustered jets will be used only if it is set to True
  bool m_useRCJetsSubstructure;        // Activates RCJets substructure variables. These variables are not stored in all ntuples. This option must be set to false if they are not available
  TString m_RCJetsHistString;      // All histogram names with RC jets variables begin with this string. Useful to control creation of histograms.
  
  TString m_bTaggingInfoCarriers;     // B-tagged small-R jets are delta-R matched with large-R jets. Two small-R jets collections are available with the same set of b-tagging working points at the moment: CaloJets=AntiKt4EMTopoJets and TrackJets=AntiKt2PV0TrackJets
  
  TString m_bTaggerName;           // B-taggers branch name. Available B-taggers: MV2c10_70, MV2c10_77, MV2c10_HybBEff_70, MV2c10_HybBEff_77, DL1_70, DL1_77, DL1_HybBEff_70, DL1_HybBEff_77
  
  TString m_topTaggerName;   // Top-tagger name. Available top-taggers: JSSWTopTaggerDNN_DNNTaggerTopQuarkInclusive50, JSSWTopTaggerDNN_DNNTaggerTopQuarkInclusive80, JSSWTopTaggerDNN_DNNTaggerTopQuarkContained50, JSSWTopTaggerDNN_DNNTaggerTopQuarkContained80
  bool m_useTopTaggerSF;           // Activates usage of top-tagging SF. Not available for all taggers.
  
  
  bool m_useTriggers;           // Activates usage of triggers.
  
  //--------------------------------------------------------------------
  TString m_recoLevelToolsLoaderName;
  TString m_recoLevelToolsLoaderConfigString;
  
  TString m_particleLevelToolsLoaderName;
  TString m_particleLevelToolsLoaderConfigString;
  
  TString m_partonLevelToolsLoaderName;
  TString m_partonLevelToolsLoaderConfigString;
  
  
  // Event reconstruction tool settings
  TString m_recoToolConfigString;
  
  // Event classification tool settings
  TString m_eventClassificationToolConfigString;
  
  // Event selection tool settings
  TString m_eventSelectionToolConfigString;
  
  // Event selection tool settings
  TString m_partonLevelSelectionToolName;
  TString m_partonLevelSelectionToolConfigString;
  
  // ------------------------------------------------------------
  // Object Selection
  // small-R jets
  double m_jet_eta_max;
  double m_jet_pt_min;
  
  // Large-R jets
  double m_ljet_eta_max;
  double m_ljet_m_min;
  double m_ljet_pt_min;
  double m_ljet_pt_max;
  double m_ljet_mOverPt_max;
  double m_ljet_particle_useRapidity;
  
  // Reclustered jets
  double m_rcjet_eta_max;
  double m_rcjet_m_min;
  double m_rcjet_pt_min;
  double m_rcjet_pt_max;
  double m_rcjet_mOverPt_max;
  
  // Variable-R reclustered jets
  double m_vrcjet_eta_max;
  double m_vrcjet_m_min;
  double m_vrcjet_pt_min;
  double m_vrcjet_pt_max;
  double m_vrcjet_mOverPt_max;
  
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  
  // Variables related with luminosity and its uncertainty
  double m_lumi;
  double m_lumi_uncert;
  TString m_lumi_string;
  
  double m_lumi_data1516;
  double m_lumi_data1516_uncert;
  TString m_lumi_data1516_string;
  
  double m_lumi_data17;
  double m_lumi_data17_uncert;
  TString m_lumi_data17_string;
  
  double m_lumi_data18;
  double m_lumi_data18_uncert;
  TString m_lumi_data18_string;
  
  double m_lumi_dataAllYears;
  double m_lumi_dataAllYears_uncert;
  TString m_lumi_dataAllYears_string;
  // --------------------------------------------------------------------------
  
  double m_ttbar_SF; // ttbar SF can be used to scale normalization of ttbar samples
  double m_factor_to_ttbar_cross_section;  // Inversed branching ratio to all-hadronic channel
  // -------------------------------------------------------------------
  
  TString m_samplesXsectionsFile;   // Path to samples cross-sections file. It stores cross-sections with K-factors for all samples.
  
  std::vector<int> m_reweightingIndexes;
  TString m_reweightingHistosFile;    // Path to file with histograms used to reweight signal samples for stress tests
 
  TString m_mc_samples_production;  // MC16a, MC16d, MC16e or All.  One can switch between productions to analyse only one year. MC16a is for 2015+2016 data, MC16d is for 2017 data, MC16e is for 2018 data and all is for full run II datasets.
  
  
public :

  TTBarConfig(const TString& configFileName);
  virtual ~TTBarConfig () = default;
  TTBarConfig (const TTBarConfig&) = default;
  TTBarConfig& operator= (const TTBarConfig&) = default;
  TTBarConfig (TTBarConfig&&) = default;
  TTBarConfig& operator= (TTBarConfig&&) = default;
  
  
  void init(const TString& configFileName);
  void readConfig();
  
  // Functions to access variables. Their description is above
  inline bool runReco() const {return m_runReco;}	
  inline bool runParticle() const {return m_runParticle;}	
  inline bool runParton () const {return m_runParton;}	
  
  inline bool useEWSF() const {return m_useEWSF;}	
  inline bool useTruth() const {return m_useTruth;}	
  
  inline bool usePDF() const {return m_usePDF;}
  inline bool doSignalModeling() const {return m_doSignalModeling;}
  
  inline bool useRecoTruthMatching() const {return m_useRecoTruthMatching;}
  inline bool normalizeToNominalCrossSection() const {return m_normalizeToNominalCrossSection;}
  
  inline const TString& topPartonsDefinition() const {return m_topPartonsDefinition;}
  
  inline const TString& hardScatteringDirectory() const {return m_hardScatteringDirectory;}
  inline const TString& partonShoweringDirectory() const {return m_partonShoweringDirectory;}
  inline const TString& PhPy8MECoffDirectory() const {return m_PhPy8MECoffDirectory;}
  inline const TString& PhH704Directory() const {return m_PhH704Directory;}
  inline const TString& FSRupDirectory() const {return m_FSRupDirectory;}
  inline const TString& FSRdownDirectory() const {return m_FSRdownDirectory;}
  inline const TString& ISRupDirectory() const {return m_ISRupDirectory;}
  inline const TString& ISRdownDirectory() const {return m_ISRdownDirectory;}
  
  inline const TString& ISRmuRupDirectory() const {return m_ISRmuRupDirectory;}
  inline const TString& ISRmuRdownDirectory() const {return m_ISRmuRdownDirectory;}
  inline const TString& ISRmuFupDirectory() const {return m_ISRmuFupDirectory;}
  inline const TString& ISRmuFdownDirectory() const {return m_ISRmuFdownDirectory;}
  inline const TString& ISRVar3cUpDirectory() const {return m_ISRVar3cUpDirectory;}
  inline const TString& ISRVar3cDownDirectory() const {return m_ISRVar3cDownDirectory;}
  

  inline const std::map<std::string,int>& getFSRbranches() const {return m_FSRbranches;}
  inline const std::map<std::string,std::vector<int>>& getISRbranches() const {return m_ISRbranches;}
  inline const std::map<std::string,int>& getPDFbranches() const {return m_PDFbranches;}

  inline const TString& qcd_method() const {return m_qcd_method;}
                     
  inline const TString& histos1D_config() const {return m_histos1D_config;}          
  inline const TString& histos2D_config() const {return m_histos2D_config;}          
  inline const TString& unfolding_histos_config() const {return m_unfolding_histos_config;}  
  inline const TString& unfolding_histosND_config() const {return m_unfolding_histosND_config;}
  inline bool useFineHistograms() const {return m_useFineHistograms;}
  inline bool fillClosureTestHistos() const {return m_fillClosureTestHistos;}

  inline const std::vector<int>& signalDSIDs() const {return m_signalDSIDs;}		
  inline const std::vector<int>& singleTopTChanDSIDs() const {return m_singleTopTChanDSIDs;}
  inline const std::vector<int>& WtSingleTopDSIDs() const {return m_WtSingleTopDSIDs;}
  inline const std::vector<int>& ttWDSIDs() const {return m_ttWDSIDs;}
  inline const std::vector<int>& ttZDSIDs() const {return m_ttZDSIDs;}
  inline const std::vector<int>& ttHDSIDs() const {return m_ttHDSIDs;}

  inline bool applyHtFilterCut() const {return m_applyHtFilterCut;}	
  inline const std::vector<int>& samplesToApplyHtFilterCut() const {return m_samplesToApplyHtFilterCut;}
  inline double HtFilterCutValue() const {return m_HtFilterCutValue;}
  
  inline bool useFilterEfficienciesCorrections() const {return m_useFilterEfficienciesCorrections;} 
  inline const TString& filterEfficienciesCorrectionsFileName() const {return m_filterEfficienciesCorrectionsFileName;}
  
  inline const TString& sumWeightsFile() const {return m_sumWeightsFile;}
  
  inline const LargeRJetType& largeRJetType() const {return m_largeRJetType;}
                     
  inline const TString& varRCJetBranchName() const {return m_varRCJetBranchName;}
  
  inline bool useTrackJets() const {return m_useTrackJets;}
  
  inline bool useVarRCJets() const {return m_useVarRCJets;}
  
  inline bool useVarRCJetsSubstructure() const {return m_useVarRCJetsSubstructure;}
  
  inline const TString& varRCJetsHistString() const {return m_varRCJetsHistString;}

  inline bool useRCJets() const {return m_useRCJets;}

  inline bool useRCJetsSubstructure() const {return m_useRCJetsSubstructure;}
  
  inline const TString& RCJetsHistString() const {return m_RCJetsHistString;}

  inline const TString& bTaggingInfoCarriers() const {return m_bTaggingInfoCarriers;}

  inline const TString& bTaggerName() const {return m_bTaggerName;}
                     
  inline const TString& topTaggerName() const {return m_topTaggerName;}
  inline bool useTopTaggerSF() const {return m_useTopTaggerSF;}
  inline bool useTriggers() const {return m_useTriggers;}

  // Event reconstruction
  inline const TString& recoLevelToolsLoaderName() const {return m_recoLevelToolsLoaderName;}
  inline const TString& recoLevelToolsLoaderConfigString() const {return m_recoLevelToolsLoaderConfigString;}

  inline const TString& particleLevelToolsLoaderName() const {return m_particleLevelToolsLoaderName;}
  inline const TString& particleLevelToolsLoaderConfigString() const {return m_particleLevelToolsLoaderConfigString;}

  inline const TString& partonLevelToolsLoaderName() const {return m_partonLevelToolsLoaderName;}
  inline const TString& partonLevelToolsLoaderConfigString() const {return m_partonLevelToolsLoaderConfigString;}

  // Event reconstruction
  inline const TString& recoToolConfigString() const {return m_recoToolConfigString;}

  // Event selection tool settings
  inline const TString& eventClassificationToolConfigString() const {return m_eventClassificationToolConfigString;}

  // Event selection
  // Event selection tool settings
  inline const TString& eventSelectionToolConfigString() const {return m_eventSelectionToolConfigString;}

  inline const TString& partonLevelSelectionToolName() const {return m_partonLevelSelectionToolName;}
  inline const TString& partonLevelSelectionToolConfigString() const {return m_partonLevelSelectionToolConfigString;}
  
  // Object selection
  // Small-R jets
  inline double jet_eta_max() const {return m_jet_eta_max;}
  inline double jet_pt_min() const {return m_jet_pt_min;}
  
  // Large-R jets
  inline double ljet_eta_max() const {return m_ljet_eta_max;}
  inline double ljet_m_min() const {return m_ljet_m_min;}
  inline double ljet_pt_min() const {return m_ljet_pt_min;}
  inline double ljet_pt_max() const {return m_ljet_pt_max;}
  inline double ljet_mOverPt_max() const {return m_ljet_mOverPt_max;}
  inline double ljet_particle_useRapidity() const {return m_ljet_particle_useRapidity;}
  
  // Reclustered jets
  inline double rcjet_eta_max() const {return m_rcjet_eta_max;}
  inline double rcjet_m_min() const {return m_rcjet_m_min;}
  inline double rcjet_pt_min() const {return m_rcjet_pt_min;}
  inline double rcjet_pt_max() const {return m_rcjet_pt_max;}
  inline double rcjet_mOverPt_max() const {return m_rcjet_mOverPt_max;}
  
  // Variable-R reclustered jets
  inline double vrcjet_eta_max() const {return m_vrcjet_eta_max;}
  inline double vrcjet_m_min() const {return m_vrcjet_m_min;}
  inline double vrcjet_pt_min() const {return m_vrcjet_pt_min;}
  inline double vrcjet_pt_max() const {return m_vrcjet_pt_max;}
  inline double vrcjet_mOverPt_max() const {return m_vrcjet_mOverPt_max;}
  
  
  
  inline double lumi() const {return m_lumi;}
  inline double lumi_uncert() const {return m_lumi_uncert;}
  inline const TString& lumi_string() const {return m_lumi_string;}
  
  inline double lumi_data1516() const {return m_lumi_data1516;}
  inline double lumi_data1516_uncert() const {return m_lumi_data1516_uncert;}
  inline const TString& lumi_data1516_string() const {return m_lumi_data1516_string;}
  
  inline double lumi_data17() const {return m_lumi_data17;}
  inline double lumi_data17_uncert() const {return m_lumi_data17_uncert;}
  inline const TString& lumi_data17_string() const {return m_lumi_data17_string;}
  
  inline double lumi_data18() const {return m_lumi_data18;}
  inline double lumi_data18_uncert() const {return m_lumi_data18_uncert;}
  inline const TString& lumi_data18_string() const {return m_lumi_data18_string;}
  
  inline double lumi_dataAllYears() const {return m_lumi_dataAllYears;}
  inline double lumi_dataAllYears_uncert() const {return m_lumi_dataAllYears_uncert;}
  inline const TString& lumi_dataAllYears_string() const {return m_lumi_dataAllYears_string;}
                
  inline double ttbar_SF() const {return m_ttbar_SF;}
  inline double factor_to_ttbar_cross_section() const {return m_factor_to_ttbar_cross_section;}

  inline const TString& samplesXsectionsFile() const {return m_samplesXsectionsFile;}
  
  inline const std::vector<int>& reweightingIndexes() const {return m_reweightingIndexes;}
  
  inline const TString& reweightingHistosFile() const {return m_reweightingHistosFile;}
  
  inline const TString& mc_samples_production() const {return m_mc_samples_production;}
  
  inline double GetValue(const char* x,const double y) const {return m_env->GetValue(x,y);}
  inline long GetValue(const char* x,const int y) const {return m_env->GetValue(x,y);}
  inline const char* GetValue(const char* x,const char* y) const {return m_env->GetValue(x,y);}
  
  inline const TEnv& getTEnv() const {return *m_env;}
  
  //ClassDef(TTBarConfig, 1)
  
};
#endif

