#ifndef MultiDimensionalPlotsSettings_H
#define MultiDimensionalPlotsSettings_H

#include "TLorentzVector.h"
#include <vector>


#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iomanip>      // std::setprecision

#include "TEnv.h"
#include "TCanvas.h"
#include "TPaveText.h"
#include "TH1.h"
#include "TH2.h"
#include "TLegend.h"

#include "TTbarHelperFunctions/plottingFunctions.h"


class MultiDimensionalPlotsSettings {
  private:
    const int m_debug;
    std::string m_configFileName;
    std::unique_ptr<TEnv> m_config;
    
    int m_nlines;
    int m_nPerLine;
    int m_nTextInfoLines;
    double m_canvas_xsize;
    double m_canvas_ysize;
    
    double m_textSizeInfo;
    double m_textSizePad;
    double m_labelSizePadX;
    double m_labelOffsetPadX;
    double m_titleSizePadX;
    double m_titleOffsetPadX;
    double m_labelSizePadY;
    double m_labelOffsetPadY;
    double m_titleSizePadY;
    double m_titleOffsetPadY;
    double m_grid_pad_spacex;
    double m_grid_pad_spacey;
    int m_axisNdivisionsX;
    int m_axisNdivisionsY;
    
    double m_textInfoYmax;
    
    // These variables are setup in config file
    double m_textSizeInfoConfig;
    double m_textSizePadConfig;
    double m_labelSizeConfig;
    double m_grid_pad_xmin;
    double m_grid_pad_ymin;
    double m_grid_pad_xmax;
    double m_grid_pad_ymax;
    double m_grid_pad_spacex_config;
    double m_grid_pad_spacey_config;
    double m_pad_leftMargin;
    double m_pad_rightMargin;
    double m_pad_topMargin;
    double m_pad_bottomMargin;
    
  public:
    
    virtual ~MultiDimensionalPlotsSettings() = default;
    MultiDimensionalPlotsSettings (const MultiDimensionalPlotsSettings&) = default;
    MultiDimensionalPlotsSettings& operator= (const MultiDimensionalPlotsSettings&) = default;
    MultiDimensionalPlotsSettings (MultiDimensionalPlotsSettings&&) = default;
    MultiDimensionalPlotsSettings& operator= (MultiDimensionalPlotsSettings&&) = default;
    MultiDimensionalPlotsSettings( const std::string& configFileName ,int debugLevel=0);

    void initialize(const double canvas_xsize, const double canvas_ysize, const int nlines, const int nPerLine, const int nTextInfoLines);
    void print() const;
    std::vector<std::vector<std::shared_ptr<TPad>>> makeArrayOfPads(const int nprojections) const;

    TPaveText* makePaveText(const std::vector<TString>& generalInfo,const double xmin=0.133,const double xmax=0.283) const;
    void setLegendAttributes(TLegend* leg,const double xmin=0.3,const double xmax=0.98) const;
    TLegend* makeLegend(const double xmin=0.3,const double xmax=0.98) const;
    
    inline TCanvas* makeCanvas(const TString& name="c") const {return new TCanvas(name,"",0,0,m_canvas_xsize,m_canvas_ysize);}
    
    inline double textSizeInfo() const {return m_textSizeInfo;}
    inline double textSizePad() const {return m_textSizePad;}
    inline double labelSizePadX() const {return m_labelSizePadX;}
    inline double labelOffsetPadX() const {return m_labelOffsetPadX;}
    inline double titleSizePadX() const {return m_titleSizePadX;}
    inline double titleOffsetPadX() const {return m_titleOffsetPadX;}
    inline double labelSizePadY() const {return m_labelSizePadY;}
    inline double labelOffsetPadY() const {return m_labelOffsetPadY;}
    inline double titleSizePadY() const {return m_titleSizePadY;}
    inline double titleOffsetPadY() const {return m_titleOffsetPadY;}

    inline double grid_pad_xmin() const {return m_grid_pad_xmin;}
    inline double grid_pad_xmax() const {return m_grid_pad_xmax;}
    inline double grid_pad_ymin() const {return m_grid_pad_ymin;}
    inline double grid_pad_ymax() const {return m_grid_pad_ymax;}
    inline double grid_pad_spacex() const {return m_grid_pad_spacex;}
    inline double grid_pad_spacey() const {return m_grid_pad_spacey;}
    
    inline double pad_leftMargin() const {return m_pad_leftMargin;}
    inline double pad_rightMargin() const {return m_pad_rightMargin;}
    inline double pad_topMargin() const {return m_pad_topMargin;}
    inline double pad_bottomMargin() const {return m_pad_bottomMargin;}
    
    inline int nlines() const {return m_nlines;}
    inline int nPerLine() const {return m_nPerLine;}
    inline int nTextInfoLines() const {return m_nTextInfoLines;}
    inline double canvas_xsize() const {return m_canvas_xsize;}
    inline double canvas_ysize() const {return m_canvas_ysize;}
    
    template <class T> void setHistLabelStyle(T* h) const {
      functions::setAxisLabelStyle(h->GetXaxis(), m_titleSizePadX, m_titleOffsetPadX, m_labelSizePadX, m_labelOffsetPadX,m_axisNdivisionsX); 
      functions::setAxisLabelStyle(h->GetYaxis(), m_titleSizePadY, m_titleOffsetPadY, m_labelSizePadY, m_labelOffsetPadY,m_axisNdivisionsY); 
    }
    
    ClassDef(MultiDimensionalPlotsSettings,1)

};

#endif
