#ifndef NamesAndTitles_h
#define NamesAndTitles_h 

#include <vector>
#include <iostream>

#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraph.h"
#include "TGraphAsymmErrors.h"

class TTBarConfig;

namespace NamesAndTitles {
  
  
  inline TString getUnit(const TString& xtitle){return xtitle(xtitle.First("[")+1 ,xtitle.First("]")-xtitle.First("[") - 1); }
  
  TString getLumiString(const TTBarConfig& ttbarConfig);
  TString getResultsStatus();
  TString ATLASString(const TString& lumi_string="");
  TString energyString();
  TString energyLumiString(const TString& lumi_string="");
  TString analysisLabel();
  std::vector<TString> getLevelInfo(const TString& level);
  TString getDifferentialVariableUnit(const TString& histname);
  TString getInverseUnit(const TString& histname);
  TString getDifferentialVariableLatex(const TString& histname);
  TString getVariableLatex(const TString& histname);
  
  TString convertPDFset(const TString& name);
  TString convertPDFsetForLHAPDF(const TString& name);
  
  // Axes info
  std::vector<TString> getXtitles(const TString& variable_name);
  TString getXtitle(const TString& variableName);
  template<typename T> void setXtitle(T* h,const TString& histname);
  template<typename T> void setYtitle(T* h,const TString& histname,const TString& unit,const TString& option);
  void setLabelForNormalized(TH1* hist, const TString& label="", const TString& unit="");
  

// Template functions definitions

  template<typename T> void setXtitle(T* h,const TString& histname) {
    TString xtitle = getXtitle(histname);
    if (xtitle!="") h->GetXaxis()->SetTitle(xtitle);
  }
  
//----------------------------------------------------------------------
        
  template<typename T> void setYtitle(T* h,const TString& histname,const TString& unit,const TString& option) {
    
    TString prefix="";
    bool isAbsolute=(option=="absolute" || option=="Absolute");
    bool isRelative=(option=="relative" || option=="Relative" || option=="normalized" || option=="Normalized");
    if(isAbsolute) prefix = "d#sigma/";
    if(isRelative) prefix = "1/#sigma#upoint d#sigma/";
    
    TString variable=getDifferentialVariableLatex(histname);
    
    TString ytitle=prefix+variable;
    TString suffix="";
    if(isAbsolute) suffix=" [fb / "+unit+"]";
    if(isRelative) suffix=" [1 / "+unit+"]";

    h->GetYaxis()->SetTitle(ytitle+suffix);
  }
  
  
}

#endif
