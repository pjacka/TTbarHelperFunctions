#ifndef stringFunctions_h
#define stringFunctions_h 

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <iomanip>      // std::setprecision
#include <tuple>
#include <algorithm>

#include "TString.h"

#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"
#include "TTbarHelperFunctions/TTBarConfig.h"

namespace functions{
  
  std::vector<std::string> splitString(std::string input,const std::string& delimiter);
  std::vector<TString> splitTString(TString input,const TString& delimiter);
  
  std::string convertNumberToString(const int number);
  std::string convertNumberToString(double number, const int rounding=-1);
  
  void replaceFirst(TString& str,const TString& from,const TString& to);
  void replaceLast(TString& str,const TString& from,const TString& to);
  
  bool compareWildcard(const std::string& input, const std::string& filter);
  
//----------------------------------------------------------------------
  
  
  inline void sortVectorOfStrings(std::vector<std::string>& vec) {
    std::sort(vec.begin(),vec.end(),[](const std::string& a,const std::string& b) {
      return a.compare(b)<0;
    });
  }
  
//----------------------------------------------------------------------
    
  inline void sortVectorOfTStrings(std::vector<TString>& vec) {
    std::sort(vec.begin(),vec.end(),[](const TString& a,const TString& b) {
      return a.CompareTo(b)<0;
    });
  }
    
//---------------------------------------------------------------------- 

  inline void removeSpaces(std::string& s) {
    s.erase(std::remove_if(s.begin(), s.end(), [](unsigned char x){return std::isspace(x);}), s.end());
  }
  
//---------------------------------------------------------------------- 
 
  // trim from start (in place)
  inline void ltrim(std::string &s) {
      s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
	  return !std::isspace(ch);
      }));
  }
  
//----------------------------------------------------------------------   

  // trim from end (in place)
  inline void rtrim(std::string &s) {
      s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
	  return !std::isspace(ch);
      }).base(), s.end());
  }
  
//---------------------------------------------------------------------- 
    
  // trim from both ends (in place)
  inline void trim(std::string &s) {
      ltrim(s);
      rtrim(s);
  }
  
  
  
  // Replace first occurency
  inline void replaceFirst(std::string& str, const std::string& from, const std::string& to) {
    size_t pos = str.find(from);
    if(pos!=std::string::npos) str.replace(pos,from.length(),to);
  }

//----------------------------------------------------------------------
  // Replace last occurency
  inline void replaceLast(std::string& str, const std::string& from, const std::string& to) {
    size_t pos = str.rfind(from);
    if(pos!=std::string::npos) str.replace(pos,from.length(),to);
  }

//----------------------------------------------------------------------
  // Replace all occurencies in string
  inline void replaceAll(std::string& str, const std::string& from, const std::string& to) {
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
      str.replace(start_pos, from.length(), to);
      start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
    }
  }
  
//----------------------------------------------------------------------

  inline bool endsWith(const std::string& str, const std::string& suffix) {
    return str.size() >= suffix.size() && 0 == str.compare(str.size()-suffix.size(), suffix.size(), suffix);
  }

//----------------------------------------------------------------------

  inline bool beginsWith(const std::string& str, const std::string& prefix) {
    return str.size() >= prefix.size() && 0 == str.compare(0, prefix.size(), prefix);
  }
  
//----------------------------------------------------------------------

  template <typename T> inline const char* intToString(const T i) {
    return Form("%i",i);
  }

//----------------------------------------------------------------------

  template <typename T> T joinVectorOfStrings(const std::vector<T>& vec, const char* delim) {
    if(vec.size()==0) return "";
    
    T res("");
    const int limit = vec.size()-1;
    for(int i=0;i<limit;i++) {
      res += vec[i] + delim;
    }
    res += vec.back();
    return res;
  }

}

#endif
