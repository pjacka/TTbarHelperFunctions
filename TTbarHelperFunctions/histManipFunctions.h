#ifndef histManipFunctions_h
#define histManipFunctions_h 

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <iomanip>      // std::setprecision
#include <tuple>
#include <sys/stat.h>  // To access struct stat

#include "TString.h"
#include "TKey.h"
#include "TTree.h"
#include "TDirectory.h"
#include "TCollection.h"
#include "TClass.h"
#include "TLorentzVector.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TLegend.h"
#include "TPad.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TFile.h"
#include "TMatrixT.h"
#include "THnSparse.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "TGraphAsymmErrors.h"
#include "TEnv.h"
#include "THStack.h"

#include "BootstrapGenerator/TH1Bootstrap.h"
#include "BootstrapGenerator/TH1DBootstrap.h"
#include "BootstrapGenerator/TH2Bootstrap.h"
#include "BootstrapGenerator/TH2DBootstrap.h"

#include "src/RooUnfoldResponse.h"

#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"
#include "TTbarHelperFunctions/TTBarConfig.h"

using namespace std;

namespace functions{
    
  void smearHistogram(TH1D* nominal,TH1D* smeared); // Function to fluctuate histogram
  void coherentSmearing(const TH1D* reco, const TH2D* migra, const TH1D* truth, TH1D* recoSmeared, TH2D* migraSmeared, TH1D* truthSmeared); // Functions to smear unfolding corrections, reco, and truth level spectra coherently
  void getAccNumerator(const TH2D* migra,TH1D* accNumerator);
  void getEffNumerator(const TH2D* migra,TH1D* effNumerator);
  
  TH1D* getEfficiency(TH1* passed, TH1* total);
  THStack* makeTHStack(const TString& name,const std::vector<TH1D*> histos);
  
  void copyHistStyle(const TH1* input,TH1* output);
  
  std::pair<double,double> getYRange(const vector<TH1D*>& hist,bool useLogScale=false,const double& spaceBelow=0.,const double& spaceAbove=0.);
  void ChangeYRange(TH1* hist, float &yaxis_min, float &yaxis_max);
  double getMaximum(const TH1& hist,const double maxval=FLT_MAX);
  double getMinimum(const TH1& hist,const double minval=-FLT_MAX);
  double getMaximum(const TH1* hist,const double maxval=FLT_MAX);
  double getMinimum(const TH1* hist,const double minval=-FLT_MAX);
  double getMaximum(const vector<TH1D*>& hist,const double maxval=FLT_MAX);
  double getMinimum(const vector<TH1D*>& hist,const double minval=-FLT_MAX);
  double getMaximum(const TGraph* graph,const double maxval=FLT_MAX);
  double getMinimum(const TGraph* graph,const double minval=-FLT_MAX);
  double getMaximum(const vector<TGraphAsymmErrors*>& graph,const double maxval=FLT_MAX);
  double getMinimum(const vector<TGraphAsymmErrors*>& graph,const double minval=-FLT_MAX);
  std::pair<double,double> getRange(const TAxis* axis);
  std::pair<double,double> getXRange(const vector<TH1D*>& histos);

  // Functions removing bad or not needed content from histogram
  void RemoveNegatives(TH1* hist);
  void RemoveNegatives(TH2* hist);
  void SetOverflowBinsToZero(TH2D* hist);
  void SetErrorsToZero(TH1* hist);
  void setBinErrors(TH1* hist, const TMatrixT<double>* covariance);


  // Getting momments from histograms
  double getMean(TH1*);
  double getMeanFromInterpolateFunction(TH1* h);
  
  // Functions to get vector of bins
  vector<double> getArrayOfBins(int nBins,Double_t low, Double_t up);
  vector<double> getArrayOfBins(const string& s);
  double* getArrayOfBins(vector<double>& vec);
  int getVectorOfBinEdges(TAxis* axis,vector<double>& vec);
  // Getting content of histogram
  void getVectorOfBinContents(TH1D* h,vector<double>& vec);
  vector<double> binContentsFromHistogram(TH1* h); // getting vector with bin contents
  vector<double> binErrorsFromHistogram(TH1* h); // getting vector with bin errors
  TMatrixT<double> tMatrixFromHistogram(const TH1* h);
  TMatrixT<double> tMatrixFromVector(const vector<double>& vec);
  TMatrixT<double> tMatrixFromVector(const TVectorD& vec);
  
  // Modifying content of histogram
  void DivideByBinWidth(TH1* hist);
  void MultiplyByBinWidth(TH1* hist);
  void ScaleInRange(TH1* hist,double sf, int imin,int imax);
  void NormalizeConcatenatedHistogram(TH1* hist,vector<int>& nbins);
  void ConvertToCrossSection(TH1* hist,float lumi, TString label="", TString unit="");
  void NormalizeRows(TH2 *hist);
  void NormalizeColumns(TH2D *hist, bool use_overlow_underflow);
  
  // Function to create n-dimensional histogram with non-equidistant binning
  THnSparseD* makeTHnSparseDHistogram(const TString& name, const vector<TString>& axes_titles,const vector<vector<double> >& binnings);
  // Rebinning functions
  TH2D* Rebin2D(TH2* hist, int nbinsx, double* x, int nbinsy, double* y, TString name);
  THnSparseD* RebinND(THnSparseD* hist,vector<vector<double> >& bins,TString newname="");
  void getBinID(long bin,int dimension,const vector<int>& nbins,vector<int>& binID);
  // Loading vectors with 2D binning stored in root files
  void load2DBinning(TFile* f, TString variable_name,vector<double>& binning_x,vector<vector<double> >& binning_y,vector<double>& binning_x_truth,vector<vector<double> >& binning_y_truth);
  // Input histogram has bin sizes required in unfolding chain. Output histogram has bin widths proportional to internal variables binning.
  void makeConcatenatedHistogram(TH1D* input,TH1D*& output,vector<double>& binning_x,vector<vector<double> >& binning_y);
  // Checking binning before histogram is created. Should be in increasing order.
  bool checkBinning(const int& nbins,const double* binning);
  bool checkEquidistantBinning(const double& nbins,const double& xmin,const double& xmax);
  // Checking binning before rebinning
  bool checkNewBinning1D(TAxis* axis, vector<double>& binning,const double max_dev=1E-10);
  bool checkNewBinning2D(TAxis* xaxis, TAxis* yaxis, vector<double>& binning_x, vector<vector<double> >& binning_y);
  // Checking unfolding histograms
  bool checkForEmptyBins(TH2D* migration,TH1D* effOfTruthLevelCutsNominator,TH1D* signal_reco,TH1D* effOfRecoLevelCutsNominator,TH1D* signal_truth);
  bool checkForEmptyBins(TH1D* nominator,TH1D* denominator);
  bool checkForEmptyRow(TH2D* migration);
  
  bool checkBinEdgeConsistency(const TAxis* ax, const double edge, const double relPrec=1e-7);
 
  // Converting multi-D histograms
  void create1DBinningFrom2DHistogram(TAxis* xaxis,TAxis* yaxis,vector<double>& binning);
  void create1DBinningFrom2DHistogram(vector<double>& binning, TVectorD& binning_x, vector<TVectorD>& binning_y);
  TH1D* convert2Dto1D(TH2D* input,TH1D* htemplate,TVectorD& binning_x, vector<TVectorD>& binning_y);
  TH1D* convert2Dto1D(TH2D* input,TH1D* htemplate);
  TH2D* convert1Dto2D(TH1D* input,TH2D* htemplate);
  TH2D* convert4Dto2D(THnSparseD* input,TH2D* htemplate);
  THnSparseD* convert2Dto4D(TH2D* input,THnSparseD* htemplate);
  
  // Making concatenated histogram from vector of 1D histograms. Used to create a big covariance matrix from multiple 1D spectra.
  TH1D* createEmptyConcatenatedHistogram(vector<TH1D*>& histos);
  void FillConcatenatedHistogram(TH1D* h,vector<TH1D*>& histos);
  TH2D* createEmptyConcatenatedHistogram(vector<TH2D*>& histos);
  void FillConcatenatedHistogram(TH2D* h,vector<TH2D*>& histos);
  TH1DBootstrap* createEmptyConcatenatedHistogram(vector<TH1DBootstrap*>& histos);
  void FillConcatenatedHistogram(TH1DBootstrap* h,vector<TH1DBootstrap*>& histos);
  
  // Functions for fast cloning of histogram content without memory allocation
  void copyBinContents(TH1* hInput,TH1* hOutput);
  void copyBinContents(TH2* hInput,TH2* hOutput);
  void copyBinContentsAndErrors(TH1* hInput,TH1* hOutput);
  void copyBinContentsAndErrors(TH2* hInput,TH2* hOutput);
  void copyBinContentsAndErrors(TH1* hMeas,TH1* hTruth,TH2* hMig,RooUnfoldResponse* response);

  
  // Switching axis of histogram
  TH2D* ProduceHistogramWithSwitchedAxis(const TH2* hist);
  // Creates graph with uncertainties for ratio plot
  TGraphAsymmErrors* graphFromHist(const TH1* h);
  TH1D* histFromGraph(const TGraphAsymmErrors& h);
  TGraphAsymmErrors* getUnitGraph(TGraphAsymmErrors* graph);
  void scaleGraphY(TGraphAsymmErrors* graph,const double sf);
  void scaleGraphErrors(TGraphAsymmErrors* graph,const double sf);
  void changeUnits(TH1& h,const double sf,const TString& option="");
  void changeUnits(TGraphAsymmErrors* graph,const double sf);
  void divideGraphByHistogram(TGraphAsymmErrors* graph,const TH1* h);
  void divideHistogramByGraph(TH1& h,const TGraphAsymmErrors& graph);
  void divideByBinWidth(TGraphAsymmErrors* graph);
  void normalizeGraph(TGraphAsymmErrors* graph);
  void setErrors(TGraphAsymmErrors& graph_unc,const TMatrixT<double>& cov);
  
  
}

#endif
