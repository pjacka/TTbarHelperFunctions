#ifndef HistogramNDto1DConverter_H
#define HistogramNDto1DConverter_H

#include "TLorentzVector.h"
#include <vector>


#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iomanip>      // std::setprecision

#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "THnSparse.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TAxis.h"
#include "TSystem.h"
#include "TEnv.h"
#include "TVectorT.h"
#include "TMatrixT.h"
#include "TGraph.h"
#include "TGraphAsymmErrors.h"

enum binningConvention {
  kVolume,
  kInternalWidth,
  kUnit
};

class HistogramNDto1DConverter {
  private:
    std::vector<std::string> m_axisIDs;
    std::vector<std::string> m_internalAxisIDs;
    std::unordered_map<std::string, std::vector<double> > m_optBinning;
    std::unordered_map<std::string, std::vector<TString> > m_optBinningTString;
    std::vector<std::string> m_axisIndexes;
    
    std::vector<TString> m_xtitles;

    std::vector<double> m_binning_1D;

    int m_dim;
    const TEnv m_config;
    std::ifstream m_configFile;
    //const THnSparseD* m_origHist;
    
    int m_debug;
    
    std::string m_internalBinPattern;

  public:
    
    virtual ~HistogramNDto1DConverter() = default;
    HistogramNDto1DConverter (const HistogramNDto1DConverter&) = default;
    HistogramNDto1DConverter& operator= (const HistogramNDto1DConverter&) = default;
    HistogramNDto1DConverter (HistogramNDto1DConverter&&) = default;
    HistogramNDto1DConverter& operator= (HistogramNDto1DConverter&&) = default;
    HistogramNDto1DConverter( const std::string& optBinningConfigFile ,int debugLevel=0);

    void initializeOptBinning(const std::string& histNamePattern);
    void initialize1DBinning(const binningConvention binSizeConvention=kVolume);
    int getDimension(const std::vector<std::string>& axisIDs) const;
    std::string getInternalBinPattern(const std::vector<std::string>& axisIDs) const;
    void makeListOfInternalAxisIDs(const std::vector<std::string>& axisIDs);
    void makeAxisIndexes();

    std::vector<double> getBinLowEdges(const std::string& axisID,int ibin) const; // Get lower bin edges of multi-dimensional bin
    std::vector<double> getBinUpEdges(const std::string& axisID,int ibin) const; // Get upper bin edges of multi-dimensional bin
    double getBinVolume(const std::string& axisID,int bin,const binningConvention binSizeConvention=kVolume) const;// Get bin volume of multi-dimensional bin
    
    
    std::string findParrentAxisID(const std::string& axisID,int& iParrentBin) const;
    std::pair<double,double> findParrentBin(const std::string& axisID) const; // Not yet implemented. Not sure if needed
    std::vector<double> get1DBinning(const binningConvention binSizeConvention=kVolume) const;
    const std::vector<double>& get1DBinningFast() const {return m_binning_1D;}
    
    TH1D* makeConcatenatedHistogram(const THnSparseD* input,const char* name) const;
    std::vector<TH1D*> makeEmptyProjections(const char* name=0) const; // Make empty projection histograms with correct binning
    std::vector<TH1D*> makeProjections(const THnSparseD* input,const char* name=0) const; // Make projections from concatenated histogram
    std::vector<TH1D*> makeProjections(const TH1D* input,const char* name=0) const; // Make projections from concatenated histogram
    std::vector<TGraphAsymmErrors*> makeProjections(const TGraphAsymmErrors* input,const char* name=0) const; // Make projections from concatenated graph
    
    TH1D* makeConcatenatedHistogram(const TH1D* input,const char* name,const binningConvention binSizeConvention=kVolume) const;
    TH1D* makeConcatenatedHistogramFromProjections(const std::vector<TH1D*>& projections,const char* name,const binningConvention binSizeConvention=kVolume) const;
    std::string getProjectionName(int i, const char* name=0) const;
    std::string getProjectionName(const std::string& id, const char* name=0) const;
    
    // Functions to convert migration matrix
    TH2D* make2DConcatenatedHistogram(const THnSparseD* input,const HistogramNDto1DConverter* truthBinningConverter,const char* name) const;
    
    void divideByBinVolume(std::vector<TH1D*>& projections,const binningConvention binSizeConvention=kVolume) const;
    void scaleProjections(std::vector<TH1D*>& projections,const double sf) const;
    std::vector<TString> getExternalBinsInfo(int iproj, const TString& option="") const;
    
    
    // Get bin index in 1D concatenated histogram
    std::string getAxisID(const std::vector<double>& x,int& bin) const;
    std::string getAxisID(const int index,int& bin) const;
    int getBinIndex(const std::vector<double>& x) const;
    int getBinIndex(const std::vector<int>& multiIndex) const;
    int getBinIndex(const std::string& axisID,const int ibin) const; 
     
    bool checkConsistency(const THnSparseD* input) const;
    bool checkConsistency(const TH1D* input) const;
    
    inline double getDimension() const {return m_dim;}
    
    void print() const;
    
    inline double getBinWidth(const std::string& axisID,int ibin) const {return m_optBinning.at(axisID).at(ibin) - m_optBinning.at(axisID).at(ibin-1);}// Get bin with of the 1D bin
    inline const std::vector<std::string>& getInternalAxisIDs() const {return m_internalAxisIDs;}
    inline const std::vector<double>& getBinning(const std::string& id) const {return m_optBinning.at(id);}
    inline const std::vector<TString>& getBinningTString(const std::string& id) const {return m_optBinningTString.at(id);}
    
    ClassDef(HistogramNDto1DConverter,1)
};

#endif
