#ifndef plottingFunctions_h
#define plottingFunctions_h 

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <iomanip>      // std::setprecision
#include <tuple>
#include <sys/stat.h>  // To access struct stat

#include "TString.h"
#include "TKey.h"
#include "TTree.h"
#include "TDirectory.h"
#include "TCollection.h"
#include "TClass.h"
#include "TLorentzVector.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TLegend.h"
#include "TPad.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TFile.h"
#include "TMatrixT.h"
#include "THnSparse.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "TGraphAsymmErrors.h"
#include "TEnv.h"
#include "THStack.h"
#include "TPaveText.h"

#include "BootstrapGenerator/TH1Bootstrap.h"
#include "BootstrapGenerator/TH1DBootstrap.h"
#include "BootstrapGenerator/TH2Bootstrap.h"
#include "BootstrapGenerator/TH2DBootstrap.h"

#include "../RooUnfold/src/RooUnfoldResponse.h"
#include "../RooUnfold/src/RooUnfoldBayes.h"
#include "../RooUnfold/src/RooUnfoldBinByBin.h"
#include "../RooUnfold/src/RooUnfoldSvd.h"
#include "../RooUnfold/src/RooUnfoldInvert.h"

#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"
#include "TTbarHelperFunctions/TTBarConfig.h"

using namespace std;

namespace functions{
  
  void PrintCanvas(TCanvas *can, const TString& sampleDir, const TString& mc_production, const TString& filename,Bool_t logy=false);
  
  void setStyle();
  
  void drawStackHistos(const TString& figurename, TH1D* data, const vector<TH1D*>& mc, TGraphAsymmErrors* stat, TGraphAsymmErrors* unc, const std::vector<TString>& info, TLegend* leg = 0, bool drawRatio=false ,bool logy=false);
  void drawStackHistos(TPad* pad, TH1D* data, TH1D* pseudodata, THStack* stack, const vector<TGraphAsymmErrors*>& unc);
  void plotHistogram(TPad* pad, TH1D* h, const vector<TGraphAsymmErrors*>& unc, const TString& option);
  void getGridSize(const int nProjections,int& nLines,int& nPerLine);
  
  vector<vector<shared_ptr<TPad>>> makeArrayOfPads(const int ntotal, int nPerLine,const double xmin=0.1,const double ymin=0.1,const double xmax=0.9,const double ymax=0.9,const double spacex=0.02, const double spacey=0.02, const double leftMargin=0.15,const double rightMargin=0.05,const double topMargin=0.03,const double bottomMargin=0.3);
  
  void setAxisLabelStyle(TAxis* axis, const double titleSize, const double titleOffset, const double labelSize, const double labelOffset, const int nDivisions=0);
  TPaveText* makePaveText(const std::vector<TString>& info, const double size, const double xmin, const double xmax, const double ymax);
  
  void plotHistograms(vector<TH1D*>& hist,TLegend* leg,TString dirname,TString name,TH1D* hd, TString y2name, double y2min, double y2max, TString lumi,bool use_logscale=0);
  void plotHistograms(vector<TH1D*>& hist,TLegend* leg,TString dirname,TString name,TString lumi,bool use_logscale=0,TString info="",TString info2="");
  void plotHistogramsWithFixedYRange(vector<TH1D*>& hist,TLegend* leg,TString dirname,TString name,TString lumi,double ymin,double ymax,bool use_logscale=0,TString info="",TString info2="");
  void plotHistograms2DConcatenated(vector<TH1D*>& hist,vector<double>& binning_x,vector<vector<double> >& binning_y,TLegend* leg,TString dirname,TString name,TString lumi,bool splitVerticalLines,bool use_logscale=0,TString info="",TString info2="");
  void plotHistograms2DConcatenatedWithFixedYRange(vector<TH1D*>& hist,vector<double>& binning_x,vector<vector<double> >& binning_y,TLegend* leg,TString dirname,TString name,TString lumi,bool splitVerticalLines,double ymin,double ymax,bool use_logscale=0,TString info="",TString info2="");
  void drawBinLabels2DConcatenated(TH1* hist,vector<double>& binning_x,vector<vector<double> >& binning_y,TString precision, TString precision2,TString variable1_name,double y1=0.,double y2=0.,const double size1=0.02,const double size2=0.025);
  void drawBinLabels2DConcatenated_v1(TH1* hist,vector<double>& binning_x,vector<vector<double> >& binning_y,TString precision, TString precision2,TString variable1_name,double y1=0.,double y2=0.,const double size1=0.02,const double size2=0.025);
  void drawBinLabels2DConcatenated_v2(TH1* hist,vector<double>& binning_x,vector<vector<double> >& binning_y,TString precision, TString precision2,TString variable1_name,double y1=0.,double y2=0.,const double size1=0.02,const double size2=0.025);
  
  void drawVerticalLines(TH1* h,vector<double>& binning_x,vector<vector<double> >& binning_y,double ymin,double ymax,int lineColor,int lineStyle,double lineWidth);
  void drawVerticalLinesTwoStyles(vector<TH1D*>& hist,vector<double>& binning_x,vector<vector<double> >& binning_y,double ymin,double ymax,int lineColor1,int lineStyle1,int lineColor2,int lineStyle2,double lineWidth);
  void drawTicksX(TH1* h,vector<double>& binning_x,vector<vector<double> >& binning_y,double ymin,double ymax,double size,bool isLogScale=false);

  void WriteGeneralInfo(TString cut_label, TString lumi="", float size = 0.039, float x = 0.22, float y = 0.955,float shift=0.55);
  void WriteInfo(TString info, float size=0.039, float x=0.13, float y=0.76, int color=1);
  void ATLASLabel(Double_t x,Double_t y,const char* text, float tsize=-1, Color_t color=1);
  
}

#endif
