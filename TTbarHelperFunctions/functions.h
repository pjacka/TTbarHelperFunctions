//  Peter Berta, 16.10.2012

#ifndef functions_h
#define functions_h 

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <iomanip>      // std::setprecision
#include <tuple>
#include <sys/stat.h>  // To access struct stat

#include "TString.h"
#include "TKey.h"
#include "TTree.h"
#include "TDirectory.h"
#include "TCollection.h"
#include "TClass.h"
#include "TLorentzVector.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TLegend.h"
#include "TPad.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TFile.h"
#include "TMatrixT.h"
#include "THnSparse.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "TGraphAsymmErrors.h"
#include "TEnv.h"

#include "BootstrapGenerator/TH1Bootstrap.h"
#include "BootstrapGenerator/TH1DBootstrap.h"
#include "BootstrapGenerator/TH2Bootstrap.h"
#include "BootstrapGenerator/TH2DBootstrap.h"

#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"
#include "TTbarHelperFunctions/TTBarConfig.h"

using namespace std;

namespace functions{

  void writeObject(const TObject* obj, const TString& objName, const TString& fileName, const TString& fileOption="RECREATE");
  std::vector<TString> getVectorOfKeys(TFile* f, const TString& filter="");
  
  TString printNumber(const double value,const double uncert);
  double getLumi(const TEnv& configLumi,const TString& mcProduction);
  double getLumi(const TTBarConfig& configLumi);
  
  int getDSID(const std::string& filename);
  
  //this functions returns a vector of TStrings from a string containing smaller strings separated by ;
  vector<TString> MakeVector(string text);
  
  //this functions returns a vector of floats from a string containing floats separated by ;
  vector<Float_t> MakeVectorFloat(string text);
  vector<Double_t> MakeVectorDouble(string text);
  vector<int> MakeVectorInt(string text);
  vector<unsigned int> MakeVectorUnsignedInt(string text);
  vector<TString> MakeVectorTString(string text);
  
  //returns a vector of subdirectories in the current directory
  vector<TString> GetSubdirectories(int debug);
  vector<TString> GetTTrees(int debug=0);
  
  //returns inverted value, if the input is too small (zero) than the returned value is 1e8
  Double_t invert(Double_t *x, Double_t *par);
  
  void GetListOfTH1DNames(vector<TString>& names);
  
  float getFloor(float number, int nDigits=-1);
  void CopyArray(Double_t* in,Double_t* out, int size);

  
  void PrintPtEtaPhiE(TString info, TLorentzVector vec);
  float AngularDistance(TLorentzVector jet1, TLorentzVector jet2);
  double deltaPhi(double phi1, double phi2);
  
  tuple<TString,TString> setPrecision(TString variableName);
  
  //--------------------------------------------------------------------------------
  // Functions to calculate reco level systematic uncertainties from histograms in systematicHistos format
  void calculateSystematicUncertainties(TGraphAsymmErrors* unc,TFile *f,TString variable_name);
  void getOneSysUncertainty(TH1D* nominal,TFile *f,const pair<TString,TString>& sysName,const TString& variable_name,vector<double>& sigma_up,vector<double>& sigma_down);
  void getOneSysUncertainty(TH1D* nominal,TFile *f,const TString& sysName,const TString& variable_name,vector<double>& sigma_up,vector<double>& sigma_down);
  //TH1D* GetRecoSys(TString sys, TString name, TString sel,TString level, bool doSym);
  //--------------------------------------------------------------------------------
  
  
  double calculateFSRsys(const double up,const double down);
  
  
  // Functions to classify systematic trees
  bool isNominalTree(const TString& treename);
  bool isSignalModelingSys(const TString& sysname);
  bool isPDFSys(const TString& sysname);
  bool isLargeRJERSys(const TString& treename);
  bool isDetectorSys(const TString& treename);
  bool isReweightedTree(const TString& treename);
  bool isPairedSys(vector<TString>& all,const TString& left,const TString& right,int i,int& j);
  vector<TString> getReweightedTrees(const vector<TString>& all);
  vector<TString> getSignalModelingSysNames(vector<TString>& all); // Signal modeling systematic trees
  vector<TString> getPDFSysNames(vector<TString>& all); // PDF systematic trees
  vector<TString> getSingleSysNames(vector<TString>& all, const TString& left="down",const TString& right="up"); // Detector systematic sources only with 1 shift available (up or down) 
  vector<pair<TString,TString> > getPairedSysNames(vector<TString>& all, const TString& left="down",const TString& right="up"); // Detector systematic sources with up and down shifts
  void classifySysNames(vector<TString>& all,vector<pair<TString,TString> >& pairs, vector<TString>& single, vector<TString>& pdf, const TString& left="down",const TString& right="up"); // This one is for interaction with root files from produce_hists jobs
  void classifySysNames(vector<TString>& all,vector<pair<TString,TString> >& pairs, vector<TString>& single, vector<TString>& pdf, vector<TString>& signalModeling ,const TString& left="down",const TString& right="up"); // This one is for interaction with systematics histos

  void splitDouble(double x,double &z, int& k);
  void Calculate_ABCD16_estimate(const vector<TH1D*>& data,const vector<TH1D*>& MC,TH1D* estimate,bool calculate_errors=false);
  
  TMatrixT<double> covarianceFromPrediction(TH1D* h,const TString& diffXsec, const int niter=100000);
  TMatrixT<double> covarianceForNormalizedPrediction(const TH1D& hAbs,const TMatrixT<double>& covAbs,const int niter=100000,bool divideByBinWidth=true);
  double chi2TestNormalized(TMatrixT<double>& x1,TMatrixT<double>& x2, TMatrixT<double>& cov,double &chi2,int &ndf,TString diffXsec);
  double chi2Statistics(const TMatrixT<double>& difference, const TMatrixT<double>& inverseCov);
  void scaleCovariance(TMatrixT<double>& cov, const double sf);
  
  map<TString, TH1F*> GetTH1Fs(int debug);
  map<TString, TH1D*> GetTH1Ds(int debug);
  map<TString, TH2F*> GetTH2Fs(int debug);
  map<TString, TH2D*> GetTH2Ds(int debug);
  map<TString, TProfile*> GetTProfiles(int debug);
  
  TMatrixT<double>* makeCorrelationMatrix(const TMatrixT<double>& covarianceMatrix);
  

//----------------------------------------------------------------------
  // Function to join two vectors
  template<typename T> void joinVectors(const vector<T>& a,const vector<T>& b, vector<T>& ab){
    ab.clear();
    ab.reserve(a.size()+b.size()); // preallocate memory
    ab.insert(ab.end(),a.begin(),a.end());
    ab.insert(ab.end(),b.begin(),b.end());
  }

//----------------------------------------------------------------------
  
  inline bool checkIfFileExists (const std::string& name) {
    struct stat buffer;   
    return (stat (name.c_str(), &buffer) == 0); 
  }

//----------------------------------------------------------------------  
  
  inline double getRandomContent(const double mean,const double error){
    if(error <= 0.) return mean;
    const double relUnc=mean/error;
    const double Neff=relUnc*relUnc;
    double x;
    if(Neff > 5.)do x=gRandom->Gaus(mean,error); while(x<0.);
    else x=(double)gRandom->Poisson(Neff)*error/relUnc;
    return x;
  }

//----------------------------------------------------------------------
  
  template<typename T> Double_t* GetArrayOfBins(vector<T>& bins){
    Int_t size=bins.size();
    Double_t *array=new Double_t[size];
    for (int i=0;i<size;++i){
      array[i]=bins[i];
    }
    return(array);
  }
  
//----------------------------------------------------------------------
  
  template<typename T,typename U> void GetListOfNames(vector<TString>& names,U* f,T Class ,TString path=""){
    TIter nextkey( f->GetListOfKeys()); 
    TKey* key=0;
    if(path!="" && (!path.EndsWith("/")))path+="/";
    
    if(Class==TDirectory::Class())while( (key = (TKey*)nextkey())){            
      TObject* obj = key->ReadObj();
      if (obj->IsA()->InheritsFrom(Class) && !(obj->IsA()->InheritsFrom(THnSparseD::Class())))names.push_back(path + key->GetName());
      delete obj;
    }
    else while( (key = (TKey*)nextkey())){
      TObject* obj = key->ReadObj();
      if (obj->IsA()->InheritsFrom(Class) || (obj->IsA() == Class)  )names.push_back(path + key->GetName());
      delete obj;
    }
    std::sort(names.begin(),names.end(),[](const TString& a,const TString& b){
      const std::string x=a.Data();
      const std::string y=b.Data();
      return x.compare(y)<0;
    });
  }
  
//----------------------------------------------------------------------
   
  template<typename T> inline void calculateCorrelationMatrix(TMatrixT<T>& cov,TMatrixT<T>& cor){
    const int ncols=cov.GetNcols();
    const int nrows=cov.GetNrows();
    cor.ResizeTo(nrows,ncols);
    for(int i=0;i<nrows;i++) for(int j=0;j<ncols;j++) {
      cor[i][j]= cov[i][i]>0 && cov[j][j] > 0 ? cov[i][j]/sqrt(cov[i][i]*cov[j][j]) : 0.;
    }
  }
  
//----------------------------------------------------------------------
  
  template<typename T> inline void AddCovariances(vector<TMatrixT<T> >& covariances,TMatrixT<T>& covariance){
    const int size=covariances.size();
    if(size<=0) {
      cout << "AddCovariances: Error: Trying to sum up empty vector!" << endl;
      return;
    }
    const int nbins=covariances[0].GetNcols();
    covariance.ResizeTo(nbins,nbins);
    covariance=covariances[0];
    for(int i=1;i<size;i++)covariance+=covariances[i];
  }
  
//----------------------------------------------------------------------  
  
  template<typename T> bool TopSubstructureTagger(T& jet){
    
    //static double pt_bins[]{250.000,325.000,375.000,425.000,475.000,525.000,575.000,625.000,675.000,725.000,775.000,850.000,950.000,1100.000,1300.000,1680.000};
    //static double tau32_cuts[]{ 0.773,0.713,0.672,0.637,0.610,0.591,0.579,0.574,0.573,0.574,0.576,0.578,0.580,0.580,0.577,0.571};
    //static double mass_cuts[]{ 85.052983,98.775422,107.807048,115.186721,120.365410,123.510000,125.010000,125.662377,126.075960,126.389113,126.537840,126.803137,127.322903,128.379386,130.241032,133.778159};
    
    const double jet_pt=jet.Pt();
    const double jet_m=jet.M();
    const double jet_tau32=jet.Tau32();
    
    // Cuts from BoostedJetTaggers/share/SmoothedTopTaggers/SmoothedTopTagger_AntiKt10LCTopoTrimmed_MassTau32FixedSignalEfficiency80_MC15c_20161209.dat
    const double cut_m = jet_pt > 2500 ? 136.87500559 : (126.587815488)*pow(jet_pt,0.0)+(-0.0172569674565)*pow(jet_pt,1.0)+(2.82580295107e-05)*pow(jet_pt,2.0)+(-1.73095031048e-08)*pow(jet_pt,3.0)+(3.77031450404e-12)*pow(jet_pt,4.0);
    const double cut_tau32 = jet_pt > 2500 ? 0.68761674944 : (0.426292282785)*pow(jet_pt,0.0)+(0.000556956315176)*pow(jet_pt,1.0)+(-4.01859339185e-07)*pow(jet_pt,2.0)+(1.25332422887e-10)*pow(jet_pt,3.0)+(-1.47907727101e-14)*pow(jet_pt,4.0);
    
    //// Top-tagger from paper
    //const double cut_m = Interpolate(jet_pt,pt_bins, mass_cuts ,16);
    //const double cut_tau32 = Interpolate( jet_pt, pt_bins, tau32_cuts ,16);
    
    const bool pass_mass_cut  = jet_m > cut_m;
    const bool pass_tau32_cut = jet_tau32 < cut_tau32;
    return pass_mass_cut && pass_tau32_cut;
  }
  
//----------------------------------------------------------------------
  
  template<typename T> T Interpolate(T x, T* xvalues, T* yvalues,const int size){
    int i=0;
    
    if(x<xvalues[0]) return yvalues[0];
    if(x>=xvalues[size-1]) return yvalues[size-1];
    while(xvalues[i+1]<x )i++;
    
    return (x - xvalues[i])*((yvalues[i+1] - yvalues[i])/(xvalues[i+1] - xvalues[i])) + yvalues[i];
  }
}


#endif
