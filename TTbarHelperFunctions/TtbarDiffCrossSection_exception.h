#ifndef TtbarDiffCrossSection_exception_h
#define TtbarDiffCrossSection_exception_h
#include <exception>
#include <iostream>
#include "TString.h"

class TtbarDiffCrossSection_exception: public std::exception{
    public:
        TtbarDiffCrossSection_exception(const TString& s):m_message(s){}
        ~TtbarDiffCrossSection_exception() throw() {}
        void printMessage() const {std::cout << m_message << std::endl;}
        virtual const char* what() const throw() {
          printMessage();
          return std::exception::what();
        }
    private:
        TString m_message;
	
	ClassDef(TtbarDiffCrossSection_exception,1)
};


#endif
