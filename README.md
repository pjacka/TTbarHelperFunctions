Table of Contents
=========================

**[Description](#description)**

**[Installation](#installation)**

**[Setup environment](#setup-environment)**

**[Instructions for developers](#instructions-for-developers)**

**[Preparing ntuples](#preparing-input-ntuples)**

**[Running the code](#running-the-code)**

**[Histograms definitions](#histograms-definitions)**

**[Submiting jobs on fzu farm](#submiting-jobs-on-fzu-farm)**

**[Producing unfolded plots](#producing-unfolded-plots)**

**[Producing Event displays with ATLANTIS](#producing-event-displays-with-atlantis)**

# Authors : 
- Petr Jacka <petr.jacka@cern.ch>



Description
=======================
This is a supporting package for ttbar differential cross-section analyses. It contains helper functions for manipulation with histograms. Package depends on BootstrapGenerator package only. 
It is used in TtbarDiffCrossSection and TTbarCovarianceCalculator packages.



## Installation

**1. Make a directory for the Analysis top packages:**

```
mkdir AnalysisTop-21.2
cd AnalysisTop-21.2
```

**2. Install athena (sparse checkout)**

```
setupATLAS
lsetup git
git atlas init-workdir -b 21.2 https://:@gitlab.cern.ch:8443/<$CERN_USER_NAME>/athena.git
```

**3. Setup and build AnalysisTop**
```
mkdir build && cd build
asetup AnalysisTop,21.2.29,here
cmake ../athena/Projects/WorkDir && cmake --build ./ && source x86_64-*/setup.sh
cd ..
```



**4. To check out and set up the code, simply do:**
```
cd athena
git clone https://:@gitlab.cern.ch:8443/pjacka/TTbarHelperFunctions.git
```

**5. Download the necessary packages:**
```
git clone https://:@gitlab.cern.ch:8443/cjmeyer/BootstrapGenerator.git
```

**6. setup:**
```
source TtbarDiffCrossSection/scripts/setup.sh 
```

**7. compile all packages:**
```
cd ../build
cmake ../athena/Projects/WorkDir && cmake --build ./ && source x86_64-*/setup.sh
```

**8. Setup environment variables in**
```
TtbarDiffCrossSection/scripts/setup.sh
```
setup the variables in a similar way as other users do


Setup environment
=======================

```
setupATLAS
```
or
```
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
```
Then
```
cd AnalysisTop-21.2/build
asetup AnalysisTop,21.2.29,here && source x86_64-*/setup.sh
```
If needed, compile packages using
```
cmake ../athena/Projects/WorkDir && cmake --build ./ && source x86_64-*/setup.sh
```
Afterwards, go to athena directory and setup the package
```
cd ../athena
source TtbarDiffCrossSection/scripts/setup.sh
```` 

Instructions for developers
=======================

**1. Create your own branch** 
```
git checkout -b Rel21-<YOUR_NAME> Rel21 --no-track
git remote add upstream origin
```
Your branch is based on Rel21 branch. You can develop your code in there

**2. Merging your branch with Rel21**
```
git fetch
git merge origin/Rel21
``` 
It will attempt to merge Rel21 with your local changes.

  - **Resolving merge conflicts**
  
  Open the file with conflict, resolve the conflict and use
  ```
  git add <path-to-file>
  git commit
  ```
  this let git know the conflict is resolved 

**3. Sending your changes into your personal branch**

Use
```
git status -uno
```
to list your local changes (-uno is used to skip not-tracked files)

Select files you want to send to your branch
```
git add <path-to-files>
```
Commit changes to your branche
```
git commit -m "<Write info about update here>"
git push -u origin Rel21-<YOUR_NAME>
```

**4. Creating merge request to Rel21 branch**

After you finish the point 3, you will see link to create merge request. You can also create MR directly from
https://gitlab.cern.ch/pjacka/TtbarDiffCrossSection/merge_requests/

Go to link and create MR. **Always check if your MR goes to the Rel21 and not to master and if you are merging your branch!!!** 

Since you are developer, you can approve your MR by yourself or you can ask somebody to check your MR before the approval.

Sent mail to other developers with the link to the merge request.



Preparing input ntuples
=======================
**1: [Setup environment](#setup-environment)**

**2: download datasets made by TTbarDiffXsTools from the grid using 'rucio'**

  or from lxplus manually using 'rsyncFromlxplus'
  
**3: make soft links to downloaded ntuples**
```
ntpDir=/mnt/nfs17/ATLASDiffXsecAllHadBoosted/ntuples/TOPQ_2016_09_PAPER_ntuples
./TtbarDiffCrossSection/scripts/makeLinksToNtps.sh $ntpDir
```
note: this will create the soft links to individual datasets in $ntpDir/DATASET_LINKS

**4: merge ntuples corresponding to data:**
```
 ./TtbarDiffCrossSection/scripts/mergeDownloadedFiles.sh $ntpDir/DATASET_LINKS >& data_merge.log < /dev/null & 
```
 note: merging takes some time, so run in the background. Sometimes happens that some sample have too many ntuples and it is needed to merge them.

Running the code
=======================

**1: [Setup environment](#setup-environment)**

**2: make filelists:**
```
cd TtbarDiffCrossSection/filelists/
./MakeFilelists.sh
cd ../../
```
  You should see several files in the directory TtbarDiffCrossSection/filelists/ . Try to open one. You should see the path to certain input minitree.


**3: Testing run:**

Open script
```
TtbarDiffCrossSection/scripts/run.sh
```
and set sample to 'all'
```
sample=all 
```
to run over all important samples. You can also select other options to run only one sample

Now execute the command
```
./TtbarDiffCrossSection/scripts/run.sh
```
 - run the script for 
	- 'all' (for)
	- 'reweighting1', 'reweighting2' (needed for Unfolding.sh)
 - If you want to run with doSys=1, please follow the instructions to run jobs on fzu farm

 - Script will create output rootfiles in $TtbarDiffCrossSection_output_path directory


**4: Merge samples**

merge produced samples (merge samples which needs to be merged, not only QCD...)
```
./TtbarDiffCrossSection/scripts/mergeQCDsamples.sh
```
 
**5: run ABCD_tools to get ABCD multijet background estimate**
```
./ABCD_Tools/scripts/ABCD_Tools.sh
```
Many ABCD estimates are created in 
```
$TtbarDiffCrossSection_output_path/ABCD16_*
$TtbarDiffCrossSection_output_path/ABCD9_*
```

The current nominal estimate is saved in 
```
$TtbarDiffCrossSection_output_path/ABCD16_BBB_S9_tight
```
Information about ABCD inputs is saved in textfiles in
```
$TtbarDiffCrossSection_output_path/ABCD_hist_info
```

Results of many tests of the ABCD procedure are saved in 
```
$TtbarDiffCrossSection_output_path/ABCD_tests
```

**6: Draw Data/MC comparison at detector level for all kind of distributions**

```
./TtbarDiffCrossSection/macros/DrawAll.sh 
```
Plots are saved in 
```
$TtbarDiffCrossSection_output_path/*/Leading_1t_1b_Recoil_1t_1b_tight/controlPlots_ABCD16_BBB_S9_tight
```
where * is pdf or png
 


**7: Create unfolding plots**
```
./TtbarDiffCrossSection/macros/Unfolding.sh
```
Creates testing plots with unfolding variables

Perform several tests of the unfolding procedure

Results are saved in 
```
$TtbarDiffCrossSection_output_path/*/Leading_1t_1b_Recoil_1t_1b_tight/Unfolding_reweighted*
```
Order of the following scripts is important!!!

All scripts must use the same nominal signal sample!!!

**8: Prepare histograms in a compact form**
```
./TtbarDiffCrossSection/scripts/PrepareSystematicHistos.sh
```
Nominal and shifted histograms will be saved in 
```
$TtbarDiffCrossSection_output_path/root/systematics_histos
```
Bootstrap histos will be saved in 
```
$TtbarDiffCrossSection_output_path/root/bootstrap
```
Stress test histos will be saved in 
```
$TtbarDiffCrossSection_output_path/root/stress_test_histos
```

**9: Create concatenated histograms with all spectra for big covariance**
```
./TtbarDiffCrossSection/scripts/PrepareConcatenatedHistograms.sh 
```
The concatenated histogram are stored in 
```
$TtbarDiffCrossSection_output_path/root/systematics_histos with name concatenated_<level>.root
```

**10: produce systematic tables and covariance matrices for generator systematics**
```
./TtbarDiffCrossSection/scripts/runGeneratorSystematics.sh
```
Using rootfiles in 
```
$TtbarDiffCrossSection_output_path/root/systematics_histos as input
```
Will produce modeling covarinces - will be saved in  
```
$TtbarDiffCrossSection_output_path/root/generatorSystematics/genSystematics_using_signal_410506
```
**11: Run pseudoexperiments to propagate uncertainties through unfolding**

```
./TtbarDiffCrossSection/scripts/ProducePseudoexperiments.sh
```
!!!!! Don't run ```./TtbarDiffCrossSection/scripts/CalculateCovariances.sh``` - no longer required!
  
Using $TtbarDiffCrossSection_output_path/root/systematics_histos, $TtbarDiffCrossSection_output_path/root/bootstrap 
and $TtbarDiffCrossSection_output_path/root/generatorSystematics/genSystematics_using_signal_410506 as an input
  
Program will create covariances for differential cross-sections at parton and particle level..
Output is saved in 
```
$TtbarDiffCrossSection_output_path/root/covariances
```
Latex tables with final results also created - saved in 
```
$TtbarDiffCrossSection_output_path/tex/final_results
```

**12: make data vs. theory comparison plots and chi-square tests**
```  
./TtbarDiffCrossSection/macros/Theory_vs_data_comparison.sh
```  
Creates final plots at parton and particle level and creates inputs for Riccardos scripts. Plots are saved in 
```
$TtbarDiffCrossSection_output_path/pdf/Theory_vs_data_comparison
```
Inputs for Riccardos scripts are saved in 
```
$TtbarDiffCrossSection_output_path/root/Theory_vs_data_comparison
```
**13: make nice style plots using Riccardos scripts (see below for instruction)**

**14: make sumarry tables with uncertainties, full breakdown tables and plots with uncertainties**
```
./TtbarDiffCrossSection/scripts/CalculateAsymmetricErrors.sh
```
Tables are saved in 
```
$TtbarDiffCrossSection_output_path/tex/SystematicUncertainties
```

## Histograms definitions

The binning and basic properties of histograms are defined in the config files accessed by TEnv class. Histograms will be created and saved automatically. 
However, filling of the histograms is not automatic. It has to be implemented explicitely in
```
TtbarDiffCrossSection/Root/HistogramManagerAllHadronic.cxx
```

**1. 1D histograms for reco level studies only are defined in the config file "data/histos.env"**
```
histname_binning: 0.;0.2;0.6;1.2
histname_type: unequal
histname_xtitle: #Delta R(b_{1},b_{2})
histname_b1_b2_ytitle:  N
```
Histogram will be saved with name="histname". histname_type: equidistant/unequal is not required unless you want to have histograms with two unequal bins. 

**2. 2D histograms for reco level studies only are defined in the config file "data/histos2D.env"**
```
ljet1_mass_x_ljet1_pt_xbinning: 5;122.5;222.5
ljet1_mass_x_ljet1_pt_ybinning: 5;500;1000
ljet1_mass_x_ljet1_pt_xtitle: ljet1 mass
ljet1_mass_x_ljet1_pt_ytitle: ljet2 pt
```
**3. 1D histograms used for 1D unfolding are defined in the config file "data/unfolding_histos.env"**
```
leadingTop_pt_Particle_reco:          500;550;600;650;700;750;800;1000;1200
leadingTop_pt_Particle_truth:          500;550;600;650;700;750;800;1000;1200
leadingTop_pt_Parton_reco:          500;550;600;650;700;750;800;1000;1200
leadingTop_pt_Parton_truth:          500;550;600;650;700;750;800;1000;1200
leadingTop_pt_xtitle: p_{T}^{t,1} [GeV]
leadingTop_pt_ytitle: d #sigma_{t#bar{t}} / d p_{T}^{t,1} [pb GeV^{ -1 }]
```
**4. 2D histograms used for 2D unfolding are defined in the config file "data/unfolding_histos2D.env"**
```
t1_pt_vs_t2_pt_Particle_reco_xaxis:         500;550;600;650;700;750;800;1000;1200
t1_pt_vs_t2_pt_Particle_reco_yaxis:         350;400;450;500;550;600;800;1200
t1_pt_vs_t2_pt_Particle_truth_xaxis:         500;550;600;650;700;750;800;1000;1200
t1_pt_vs_t2_pt_Particle_truth_yaxis:         350;400;450;500;550;600;800;1200
t1_pt_vs_t2_pt_Parton_reco_xaxis:         500;550;600;650;700;750;800;1000;1200
t1_pt_vs_t2_pt_Parton_reco_yaxis:         350;400;450;500;550;600;800;1200
t1_pt_vs_t2_pt_Parton_truth_xaxis:         500;550;600;650;700;750;800;1000;1200
t1_pt_vs_t2_pt_Parton_truth_yaxis:         350;400;450;500;550;600;800;1200
t1_pt_vs_t2_pt_xtitle: p_{T}^{t,1} [GeV]
t1_pt_vs_t2_pt_ytitle: p_{T}^{t,2} [GeV]
```
Binning at reco level has to be the same for particle and parton level unfolding(so far). But binning at parton and particle level can be different. It is possible to setup different binning at truth at reco level.

## Submiting jobs on fzu farm

**- Split filelists for jobs (Assuming that filelists with all files exists)**
```
./TtbarDiffCrossSection/scripts/SplitFileLists_All.sh 
```
**- Submit condor jobs **

  a) All jobs
    Open 
    ```
    /TtbarDiffCrossSection/condor_scripts/SubmitFarm_All.sh
    ``` 
    and select jobs for run
    ```
    ./TtbarDiffCrossSection/condor_scripts/SubmitFarm_All.sh
    ```
    b) Specific jobs
    ```
    ./TtbarDiffCrossSection/condor_scripts/SubmitFarm.sh
    ```

    You can check status of your jobs using
    ```
    condor_q
    ```
    When job finish you will receive email with info. Please check whether exit status is zero. 
    
    
    Log files are stored in 
    ```
    $TtbarDiffCrossSection_output_path/batch/logs/
    ```
**- Merge produced rootfiles from jobs**

```
./TtbarDiffCrossSection/condor_scripts/MergeFilesFromFarmRunCondor.sh
```
**- Continue with point 4 from instructions to run the code**

## Producing unfolded plots

**1. get the code**
```
  svn co svn+ssh://svn.cern.ch/reps/atlasphys-top/Physics/Top/PhysAnalysis/Run2/TTbarDiffXsTools/trunk TTbarDiffXsTools
  cd TTbarDiffXsTools
```
**2. setup root**

**3. prepare running directory:**
```
   mkdir run; 
   cd scripts/TTAllHadBoosted/ 
   cp MakePlotsUnfolded.py PlottingToolkit.py rootlogon.C AtlasStyle.* AtlasUtils.C Datasets.py Defs.py ../../run
   cd ../../run
   #make link to unfolded historgram
   ln -s $TtbarDiffCrossSection_output_path/root/Theory_vs_data_comparison unfolding_results
```
**4.  make plots**
```
   ./MakePlotsUnfolded.py -c  plots_parton_abs.xml
   ./MakePlotsUnfolded.py -c  plots_parton_rel.xml
   #./MakePlotsUnfolded.py -c  plots_particle_rel.xml
   #./MakePlotsUnfolded.py -c  plots_particle_abs.xml
```


## Producing Event displays with ATLANTIS 

#to start Atlantis:
- **run directly from web** (need java webstart):
[http://atlantis.web.cern.ch/atlantis/AtlantisJava-09-16-06-07-webstart/atlantis.jnlp](http://atlantis.web.cern.ch/atlantis/AtlantisJava-09-16-06-07-webstart/atlantis.jnlp)


- **OR: install locally**: download from 
  [http://atlantis.web.cern.ch/atlantis/](http://atlantis.web.cern.ch/atlantis/)
  and unpack and run '
  ```
  java -jar atlantis.jar
  ```
- **OR:** 
```
setupATLAS
lsetup atlantis
runAtlantis
```
- **as an input, one needs .xml file** (contains info about a given event)
  - the input files for interesting event are here:
  ```
       ui8:/home/lysak/DiffXsecBoosted/EventDisplay
  ```
- **I also created the configuration files** (they are saved in same directories)

- **to run atlantis with a given config file and input XML file, execute, e.g.:**
   ``` 
   atlantis -c Atlantis-config_284285_v2.xml -s JiveXML_284285_3682216408.xml
   ```
   
**source of info:**
  
  http://atlantis.web.cern.ch/atlantis/
  
  https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/Atlantis


