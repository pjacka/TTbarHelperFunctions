#include "TTbarHelperFunctions/MultiDimensionalPlotsSettings.h"
#include "TTbarHelperFunctions/functions.h"
#include "TTbarHelperFunctions/stringFunctions.h"
#include "TTbarHelperFunctions/histManipFunctions.h"
#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"

#include <stdexcept>

ClassImp(MultiDimensionalPlotsSettings)

MultiDimensionalPlotsSettings::MultiDimensionalPlotsSettings( const std::string& configFileName, const int debugLevel) : m_debug(debugLevel), m_configFileName(configFileName) {
  if(m_debug > 0) {
    std::cout << "Creating MultiDimensionalPlotsSettings with config file name: " << m_configFileName << std::endl;
  }
  
  m_config = std::make_unique<TEnv>(m_configFileName.c_str());
  
  m_grid_pad_xmin = m_config->GetValue("grid_pad_xmin",0.001);
  m_grid_pad_ymin = m_config->GetValue("grid_pad_ymin",0.999);
  m_grid_pad_xmax = m_config->GetValue("grid_pad_xmax",0.001);
  m_grid_pad_spacex_config = m_config->GetValue("grid_pad_spacex",1.);
  m_grid_pad_spacey_config = m_config->GetValue("grid_pad_spacey",1.);
  m_pad_leftMargin = m_config->GetValue("pad_leftMargin",0.25);
  m_pad_rightMargin = m_config->GetValue("pad_rightMargin",0.1);
  m_pad_topMargin = m_config->GetValue("pad_topMargin",0.03);
  m_pad_bottomMargin = m_config->GetValue("pad_bottomMargin",0.3);
  m_textSizeInfoConfig = m_config->GetValue("textSizeInfo",1.);
  m_textSizePadConfig = m_config->GetValue("textSizePad",1.);
  m_labelSizeConfig = m_config->GetValue("labelSize",1.);
  m_axisNdivisionsX = m_config->GetValue("axisNdivisionsX",6);
  m_axisNdivisionsY = m_config->GetValue("axisNdivisionsY",6);
}

//----------------------------------------------------------------------

void MultiDimensionalPlotsSettings::print() const {
  
  std::cout << std::endl << "MultiDimensionalPlotsSettings: Printing settings: " << std::endl;
  
  std::cout << "m_canvas_xsize: " << m_canvas_xsize << std::endl;
  std::cout << "m_canvas_ysize: " << m_canvas_ysize << std::endl;
  std::cout << "m_nlines: " << m_nlines << std::endl;
  std::cout << "m_nPerLine: " << m_nPerLine << std::endl;
  std::cout << "m_nTextInfoLines: " << m_nTextInfoLines << std::endl;
  std::cout << "m_grid_pad_xmin: " << m_grid_pad_xmin << std::endl;
  std::cout << "m_grid_pad_ymin: " << m_grid_pad_ymin << std::endl;
  std::cout << "m_grid_pad_xmax: " << m_grid_pad_xmax << std::endl;
  std::cout << "m_grid_pad_ymax: " << m_grid_pad_ymax << std::endl;
  std::cout << "m_grid_pad_spacex: " << m_grid_pad_spacex << std::endl;
  std::cout << "m_grid_pad_spacey: " << m_grid_pad_spacey << std::endl;
  std::cout << "m_textSizePad: " << m_textSizePad << std::endl;
  std::cout << "m_labelSizePadX: " << m_labelSizePadX << std::endl;
  std::cout << "m_labelOffsetPadX: " << m_labelOffsetPadX << std::endl;
  std::cout << "m_titleSizePadX: " << m_titleSizePadX << std::endl;
  std::cout << "m_titleOffsetPadX: " << m_titleOffsetPadX << std::endl;
  std::cout << "m_labelSizePadY: " << m_labelSizePadY << std::endl;
  std::cout << "m_labelOffsetPadY: " << m_labelOffsetPadY << std::endl;
  std::cout << "m_titleSizePadY: " << m_titleSizePadY << std::endl;
  std::cout << "m_titleOffsetPadY: " << m_titleOffsetPadY << std::endl;
  
  std::cout << std::endl;
  
}

//----------------------------------------------------------------------

void MultiDimensionalPlotsSettings::initialize(const double canvas_xsize, const double canvas_ysize, const int nlines, const int nPerLine, const int nTextInfoLines) {
  
  m_canvas_xsize = canvas_xsize;
  m_canvas_ysize = canvas_ysize;
  m_nlines = nlines;
  m_nPerLine = nPerLine;
  m_nTextInfoLines = nTextInfoLines;
  
  
  m_textSizeInfo = m_textSizeInfoConfig*(21./m_canvas_ysize);
  m_grid_pad_ymax = 1.-m_nTextInfoLines*m_textSizeInfo*1.14;
  
  m_grid_pad_spacex = m_grid_pad_spacex_config*(-80./canvas_xsize);
  m_grid_pad_spacey = m_grid_pad_spacey_config*(-10.*m_nlines/m_canvas_ysize);
  
  //const double pad_xsize = m_canvas_xsize*(m_grid_pad_xmax - m_grid_pad_xmin - (m_nPerLine-1)*m_grid_pad_spacex)/m_nPerLine;
  const double pad_ysize = m_canvas_ysize*(m_grid_pad_ymax - m_grid_pad_ymin - (m_nlines-1)*m_grid_pad_spacey)/m_nlines;
  
  m_textSizePad = m_textSizePadConfig*(21./pad_ysize);
  m_labelSizePadX = m_labelSizeConfig*(21./pad_ysize);
  m_labelOffsetPadX = 0.01;
  if (m_nlines < 3) {
    m_titleSizePadX = m_labelSizeConfig*(21./pad_ysize);
    m_titleOffsetPadX =  1.1;
  } 
  else {
    m_titleSizePadX = m_labelSizeConfig*(18./pad_ysize);
    m_titleOffsetPadX =  1.05;
  }
 
  m_labelSizePadY = m_labelSizeConfig*(21./pad_ysize);
  m_labelOffsetPadY = 0.01;
  m_titleSizePadY = m_labelSizeConfig*(21./pad_ysize);
  m_titleOffsetPadY = 1.15;
  
  m_textInfoYmax = m_grid_pad_ymax + m_nTextInfoLines*m_textSizeInfo*1.14;
  
}

//----------------------------------------------------------------------

std::vector<std::vector<std::shared_ptr<TPad>>> MultiDimensionalPlotsSettings::makeArrayOfPads(const int nprojections) const {
  if( nprojections > m_nlines*m_nPerLine ) {
    std::runtime_error("Error in MultiDimensionalPlotsSettings::makeArrayOfPads: Too many projections"); 
    return {};
  }
  if( nprojections <= (m_nlines-1)*m_nPerLine ) {
    std::runtime_error("Error in MultiDimensionalPlotsSettings::makeArrayOfPads: Too little projections"); 
    return {};
  }
  
  return functions::makeArrayOfPads(nprojections, m_nPerLine,  m_grid_pad_xmin,  m_grid_pad_ymin,  m_grid_pad_xmax,
                    m_grid_pad_ymax, m_grid_pad_spacex,  m_grid_pad_spacey, m_pad_leftMargin,
                    m_pad_rightMargin, m_pad_topMargin, m_pad_bottomMargin);
}

//----------------------------------------------------------------------

void MultiDimensionalPlotsSettings::setLegendAttributes(TLegend* leg,const double xmin,const double xmax) const {
  std::cout << xmin << " " << xmax  << " " << m_grid_pad_ymax << " " << m_textInfoYmax << std::endl;
  leg->SetX1NDC(xmin);
  leg->SetX2NDC(xmax);
  leg->SetY1NDC(m_grid_pad_ymax);
  leg->SetY2NDC(m_textInfoYmax);
  leg->SetTextFont(42);
  leg->SetFillColor(0);
  leg->SetBorderSize(0);
  leg->SetTextSize(m_textSizeInfo);
}

//----------------------------------------------------------------------

TLegend* MultiDimensionalPlotsSettings::makeLegend(const double xmin,const double xmax) const {
  TLegend* leg = new TLegend(xmin,m_grid_pad_ymax,xmax,m_textInfoYmax);
  leg->SetTextSize(m_textSizeInfo);
  leg->SetFillColor(0);
  leg->SetBorderSize(0);
  return leg;
}


//----------------------------------------------------------------------

TPaveText* MultiDimensionalPlotsSettings::makePaveText(const std::vector<TString>& generalInfo,const double xmin,const double xmax) const {
  TPaveText* paveTextGeneralInfo = functions::makePaveText(generalInfo, m_textSizeInfo, xmin, xmax, m_textInfoYmax);
  paveTextGeneralInfo->SetFillStyle(0);
  return paveTextGeneralInfo;
}
