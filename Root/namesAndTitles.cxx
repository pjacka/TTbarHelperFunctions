#include "TTbarHelperFunctions/namesAndTitles.h"
#include "TTbarHelperFunctions/functions.h"
#include "TTbarHelperFunctions/TTBarConfig.h"


namespace NamesAndTitles {
  
//----------------------------------------------------------------------

  TString getLumiString(const TTBarConfig& ttbarConfig) {
    TString lumi("");
    TString mcProduction = ttbarConfig.mc_samples_production();
    if(mcProduction=="MC16a")lumi=ttbarConfig.lumi_data1516_string();
    else if (mcProduction=="MC16c" || mcProduction=="MC16d") lumi=ttbarConfig.lumi_data17_string();
    else if (mcProduction=="MC16e" || mcProduction=="MC16f") lumi=ttbarConfig.lumi_data18_string();
    else if (mcProduction=="All") lumi = ttbarConfig.lumi_dataAllYears_string();
    else cout << "Error: Unknown MC production" << endl;
    
    return lumi;
  }

//----------------------------------------------------------------------
  
  // This function should be used in all plotting functions
  TString getResultsStatus(){
    //return "Internal";
    //return "Preliminary"; // For approved plots on conferences
    return ""; // For publication
  }

//----------------------------------------------------------------------
  
  TString ATLASString(const TString& lumi_string) {
    TString x = "#bf{#it{ATLAS}}";
    if (lumi_string=="") x+=" Simulation";
    const TString status = NamesAndTitles::getResultsStatus();
    if(status!="") x+= " " + NamesAndTitles::getResultsStatus();
    return x;
  }

//----------------------------------------------------------------------

  TString energyString() {
    return "#sqrt{s}=13 TeV";
  }

//----------------------------------------------------------------------
  
  TString energyLumiString(const TString& lumi_string) {
    
    TString x = NamesAndTitles::energyString();
    if(lumi_string!="") x +=", " +lumi_string;
    if(!x.EndsWith("b^{-1}") && lumi_string!="") x+=" fb^{-1}";
    return x;
  }
  
//----------------------------------------------------------------------

  TString analysisLabel() {
    return "Boosted all-hadronic t#bar{t}";
  }

//----------------------------------------------------------------------

  std::vector<TString> getLevelInfo(const TString& level) {
    std::vector<TString> info;
    if (level.BeginsWith("Parton")) {
     // info.push_back("");
      //info.push_back("Parton level");
      //info.push_back("p^{t,1}_{T} > 0.5 TeV, p^{t,2}_{T} > 0.35 TeV");
      info.push_back("Fiducial parton level");
    }
    else if (level.BeginsWith("Particle"))  {
      //info.push_back("");
      info.push_back("Fiducial particle level");
    }
    
    return info;
  }

//----------------------------------------------------------------------

  TString getDifferentialVariableUnit(const TString& variableName) {
    TString unit="";
    
    if(variableName.Contains("t1_m_over_pt")) unit=""; 				
    if(variableName.Contains("t2_m_over_pt")) unit=""; 				
    if(variableName.Contains("delta_pt")) unit="TeV"; 				
    if(variableName.Contains("randomTop_pt")) unit="TeV"; 				
    if(variableName.Contains("inclusive_top_pt")) unit="TeV"; 				
    if(variableName.Contains("t1_pt")) unit="TeV"; 				
    if(variableName.Contains("leadingTop_pt")) unit="TeV"; 				
    if(variableName.Contains("subleadingTop_pt")) unit="TeV"; 				
    if(variableName.Contains("t2_pt")) unit="TeV"; 				
    if(variableName.Contains("randomTop_y")) unit="";         				
    if(variableName.Contains("inclusive_top_y")) unit="";         				
    if(variableName.Contains("leadingTop_y")) unit="";         				
    if(variableName.Contains("t1_y")) unit="";         				
    if(variableName.Contains("subleadingTop_y")) unit="";         				
    if(variableName.Contains("t2_y")) unit="";         				
    if(variableName.Contains("ttbar_mass")) unit="TeV";     		
    if(variableName.Contains("ttbar_pt")) unit="TeV"; 		
    if(variableName.Contains("ttbar_y")) unit="";    				
    if(variableName.Contains("ttbar_deltaphi")) unit=""; 
    if(variableName.Contains("pout")) unit="TeV"  ;   
    if(variableName.Contains("y_boost")) unit=""  ;       
    if(variableName.Contains("chi_ttbar")) unit="";          
    if(variableName.Contains("H_tt_pt")) unit="TeV";     	  
    if(variableName.Contains("cos_theta_star")) unit="";
    if(variableName.Contains("z_tt_pt")) unit="";      			  
    if(variableName.Contains("y_star")) unit="";
    // 2D spectra
    if(variableName.Contains("t1_pt_vs_t2_pt")) unit="TeV^{2}";
    if(variableName.Contains("t1_pt_vs_delta_pt")) unit="TeV^{2}";
    if(variableName.Contains("t1_pt_vs_ttbar_pt")) unit="TeV^{2}";
    if(variableName.Contains("t2_pt_vs_ttbar_pt")) unit="TeV^{2}";
    if(variableName.Contains("top_pt_vs_ttbar_pt")) unit="TeV^{2}";
    if(variableName.Contains("t1_pt_vs_ttbar_mass")) unit="TeV^{2}";
    if(variableName.Contains("t2_pt_vs_ttbar_mass")) unit="TeV^{2}";
    if(variableName.Contains("top_pt_vs_ttbar_mass")) unit="TeV^{2}";
    if(variableName.Contains("ttbar_y_vs_t1_pt")) unit="TeV";
    if(variableName.Contains("ttbar_y_vs_t2_pt")) unit="TeV";
    if(variableName.Contains("ttbar_y_vs_top_pt")) unit="TeV";
    if(variableName.Contains("ttbar_y_vs_ttbar_mass")) unit="TeV";
    if(variableName.Contains("ttbar_y_vs_ttbar_pt")) unit="TeV";
    if(variableName.Contains("top_y_vs_top_pt")) unit="TeV";
    if(variableName.Contains("ttbar_y_vs_top_y")) unit="";
    if(variableName.Contains("ttbar_y_vs_t1_y")) unit="";
    if(variableName.Contains("ttbar_y_vs_t2_y")) unit="";
    if(variableName.Contains("t1_y_vs_ttbar_mass")) unit="TeV";
    if(variableName.Contains("t2_y_vs_ttbar_mass")) unit="TeV";
    if(variableName.Contains("top_y_vs_ttbar_mass")) unit="TeV";
    if(variableName.Contains("ttbar_pt_vs_ttbar_mass")) unit="TeV^{2}";
    if(variableName.Contains("delta_eta_vs_ttbar_mass")) unit="TeV";
    if(variableName.Contains("delta_y_vs_ttbar_mass")) unit="TeV";
    if(variableName.Contains("delta_phi_vs_ttbar_mass")) unit="TeV";
    if(variableName.Contains("t1_y_vs_t2_y")) unit="";
    if(variableName.Contains("delta_y_vs_t1_y")) unit="";
    if(variableName.Contains("t1_phi_vs_t1_eta")) unit="";
    if(variableName.Contains("t2_phi_vs_t2_eta")) unit="";
    // 3D spectra
    if(variableName.Contains("ttbar_y_vs_ttbar_mass_vs_t1_pt")) unit="TeV^{2}";
    if(variableName.Contains("ttbar_y_vs_ttbar_pt_vs_ttbar_mass")) unit="TeV^{2}";
    
    
    
    return unit;
    
    
    
  }

//----------------------------------------------------------------------

  TString getInverseUnit(const TString& variableName) {
    TString unit="";
    
    if(variableName.Contains("t1_m_over_pt")) unit=""; 				
    if(variableName.Contains("t2_m_over_pt")) unit=""; 				
    if(variableName.Contains("delta_pt")) unit="TeV^{-1}"; 				
    if(variableName.Contains("randomTop_pt")) unit="TeV^{-1}"; 				
    if(variableName.Contains("inclusive_top_pt")) unit="TeV^{-1}"; 				
    if(variableName.Contains("leadingTop_pt")) unit="TeV^{-1}"; 				
    if(variableName.Contains("t1_pt")) unit="TeV^{-1}"; 				
    if(variableName.Contains("subleadingTop_pt")) unit="TeV^{-1}"; 				
    if(variableName.Contains("t2_pt")) unit="TeV^{-1}"; 				
    if(variableName.Contains("randomTop_y")) unit="";         				
    if(variableName.Contains("inclusive_top_y")) unit="";         				
    if(variableName.Contains("leadingTop_y")) unit="";         				
    if(variableName.Contains("t1_y")) unit="";         				
    if(variableName.Contains("subleadingTop_y")) unit="";         				
    if(variableName.Contains("t2_y")) unit="";         				
    if(variableName.Contains("ttbar_mass")) unit="TeV^{-1}";     		
    if(variableName.Contains("ttbar_pt")) unit="TeV^{-1}"; 		
    if(variableName.Contains("ttbar_y")) unit="";    				
    if(variableName.Contains("ttbar_deltaphi")) unit=""; 
    if(variableName.Contains("pout")) unit="TeV^{-1}"  ;   
    if(variableName.Contains("y_boost")) unit=""  ;       
    if(variableName.Contains("chi_ttbar")) unit="";          
    if(variableName.Contains("H_tt_pt")) unit="TeV^{-1}";     	  
    if(variableName.Contains("cos_theta_star")) unit="";
    if(variableName.Contains("z_tt_pt")) unit="";      			  
    if(variableName.Contains("y_star")) unit="";
    // 2D spectra
    if(variableName.Contains("t1_pt_vs_t2_pt")) unit="TeV^{-2}";
    if(variableName.Contains("t1_pt_vs_delta_pt")) unit="TeV^{-2}";
    if(variableName.Contains("t1_pt_vs_ttbar_pt")) unit="TeV^{-2}";
    if(variableName.Contains("t2_pt_vs_ttbar_pt")) unit="TeV^{-2}";
    if(variableName.Contains("top_pt_vs_ttbar_pt")) unit="TeV^{-2}";
    if(variableName.Contains("t1_pt_vs_ttbar_mass")) unit="TeV^{-2}";
    if(variableName.Contains("t2_pt_vs_ttbar_mass")) unit="TeV^{-2}";
    if(variableName.Contains("top_pt_vs_ttbar_mass")) unit="TeV^{-2}";
    if(variableName.Contains("ttbar_y_vs_t1_pt")) unit="TeV^{-1}";
    if(variableName.Contains("ttbar_y_vs_t2_pt")) unit="TeV^{-1}";
    if(variableName.Contains("t1_y_vs_t1_pt")) unit="TeV^{-1}";
    if(variableName.Contains("t2_y_vs_t2_pt")) unit="TeV^{-1}";
    if(variableName.Contains("ttbar_y_vs_top_pt")) unit="TeV^{-1}";
    if(variableName.Contains("ttbar_y_vs_ttbar_mass")) unit="TeV^{-1}";
    if(variableName.Contains("ttbar_y_vs_ttbar_pt")) unit="TeV^{-1}";
    if(variableName.Contains("top_y_vs_top_pt")) unit="TeV^{-1}";
    if(variableName.Contains("ttbar_y_vs_top_y")) unit="";
    if(variableName.Contains("ttbar_y_vs_t1_y")) unit="";
    if(variableName.Contains("ttbar_y_vs_t2_y")) unit="";
    if(variableName.Contains("t1_y_vs_ttbar_mass")) unit="TeV^{-1}";
    if(variableName.Contains("t2_y_vs_ttbar_mass")) unit="TeV^{-1}";
    if(variableName.Contains("top_y_vs_ttbar_mass")) unit="TeV^{-1}";
    if(variableName.Contains("ttbar_pt_vs_ttbar_mass")) unit="TeV^{-2}";
    if(variableName.Contains("delta_eta_vs_ttbar_mass")) unit="TeV^{-1}";
    if(variableName.Contains("delta_y_vs_ttbar_mass")) unit="TeV^{-1}";
    if(variableName.Contains("delta_phi_vs_ttbar_mass")) unit="TeV^{-1}";
    if(variableName.Contains("t1_y_vs_t2_y")) unit="";
    if(variableName.Contains("delta_y_vs_t1_y")) unit="";
    if(variableName.Contains("t1_phi_vs_t1_eta")) unit="";
    if(variableName.Contains("t2_phi_vs_t2_eta")) unit="";
    
    
    // 3D spectra
    if(variableName.Contains("ttbar_y_vs_ttbar_mass_vs_t1_pt")) unit="TeV^{-2}";
    if(variableName.Contains("ttbar_y_vs_ttbar_pt_vs_ttbar_mass")) unit="TeV^{-2}";
    
    
    
    return unit;
    
    
  }

//----------------------------------------------------------------------

  TString getDifferentialVariableLatex(const TString& variableName){
    
    TString variable="";
    if(variableName.Contains("t1_m_over_pt")) variable="d(m^{t,1}/p_{T}^{t,1})"; 				
    if(variableName.Contains("t2_m_over_pt")) variable="d(m^{t,2}/p_{T}^{t,2})"; 				
    if(variableName.Contains("delta_pt")) variable="d#Delta p_{T}(t_{1}, t_{2})"; 				
    if(variableName.Contains("randomTop_pt")) variable="dp_{T}^{t}";
    if(variableName.Contains("inclusive_top_pt")) variable="dp_{T}^{t,incl}"; 				
    if(variableName.Contains("leadingTop_pt")) variable="dp_{T}^{t,1}"; 				
    if(variableName.Contains("t1_pt")) variable="dp_{T}^{t,1}"; 				
    if(variableName.Contains("subleadingTop_pt")) variable="dp_{T}^{t,2}"; 				
    if(variableName.Contains("t2_pt")) variable="dp_{T}^{t,2}"; 				
    if(variableName.Contains("randomTop_y")) variable="d|y^{t}|";         				
    if(variableName.Contains("inclusive_top_y")) variable="d|y^{t,incl}|";         				
    if(variableName.Contains("leadingTop_y")) variable="d|y^{t,1}|";         				
    if(variableName.Contains("t1_y")) variable="d|y^{t,1}|";         				
    if(variableName.Contains("subleadingTop_y")) variable="d|y^{t,2}|";         				
    if(variableName.Contains("t2_y")) variable="d|y^{t,2}|";         				
    if(variableName.Contains("ttbar_mass")) variable="dm^{t#bar{t}}";     		
    if(variableName.Contains("ttbar_pt")) variable="dp_{T}^{t#bar{t}}"; 		
    if(variableName.Contains("ttbar_y")) variable="d|y^{t#bar{t}}|";    				
    if(variableName.Contains("ttbar_deltaphi")) variable="d|#Delta #phi(t_{1}, t_{2})|"; 
    if(variableName.Contains("pout")) variable="d|p_{out}^{t#bar{t}}|"  ;   
    if(variableName.Contains("y_boost")) variable="d|y_{B}^{t#bar{t}}|"  ;       
    if(variableName.Contains("chi_ttbar")) variable="d#chi^{t#bar{t}}";          
    if(variableName.Contains("H_tt_pt")) variable="dH_{T}^{t#bar{t}}";     	  
    if(variableName.Contains("cos_theta_star")) variable="d|cos#theta^{*}|";
    if(variableName.Contains("z_tt_pt")) variable="dz^{t#bar{t}}";      			  
    if(variableName.Contains("y_star")) variable="d|y^{*}|";
    // 2D spectra
    if(variableName.Contains("t1_pt_vs_t2_pt")) variable="dp_{T}^{t,1} dp_{T}^{t,2}";
    if(variableName.Contains("t1_pt_vs_delta_pt")) variable="dp_{T}^{t,1} d#Delta p_{T}(t_{1}, t_{2})";
    if(variableName.Contains("t1_pt_vs_ttbar_pt")) variable="dp_{T}^{t,1} dp_{T}^{t#bar{t}}";
    if(variableName.Contains("t1_pt_vs_ttbar_mass")) variable="dp_{T}^{t,1} dm^{t#bar{t}}";
    if(variableName.Contains("ttbar_y_vs_t1_pt")) variable="d|y^{t#bar{t}}| dp_{T}^{t,1}";
    if(variableName.Contains("ttbar_y_vs_t2_pt")) variable="d|y^{t#bar{t}}| dp_{T}^{t,2}";
    if(variableName.Contains("t1_y_vs_t1_pt")) variable="d|y^{t,1}| dp_{T}^{t,1}";
    if(variableName.Contains("t2_y_vs_t2_pt")) variable="d|y^{t,2}| dp_{T}^{t,2}";
    if(variableName.Contains("ttbar_y_vs_ttbar_mass")) variable="d|y^{t#bar{t}}| dm^{t#bar{t}}";
    if(variableName.Contains("ttbar_y_vs_ttbar_pt")) variable="d|y^{t#bar{t}}| dp_{T}^{t#bar{t}}";
    if(variableName.Contains("top_y_vs_top_pt")) variable="d|y^{t,rndm}| dp_{T}^{t,rndm}";
    if(variableName.Contains("ttbar_y_vs_top_y")) variable="d|y^{t#bar{t}}| d|y^{t,rndm}|";
    if(variableName.Contains("ttbar_y_vs_t1_y")) variable="d|y^{t#bar{t}}| d|y^{t,1}|";
    if(variableName.Contains("ttbar_y_vs_t2_y")) variable="d|y^{t#bar{t}}| d|y^{t,2}|";
    if(variableName.Contains("t1_y_vs_ttbar_mass")) variable="d|y^{t,1}| dm^{t#bar{t}}";
    if(variableName.Contains("t2_y_vs_ttbar_mass")) variable="d|y^{t,2}| dm^{t#bar{t}}";
    if(variableName.Contains("top_y_vs_ttbar_mass")) variable="d|y^{t,rndm}| dm^{t#bar{t}}";
    if(variableName.Contains("ttbar_pt_vs_ttbar_mass")) variable="dp_{T}^{t#bar{t}} dm^{t#bar{t}}";
    if(variableName.Contains("delta_eta_vs_ttbar_mass")) variable="d|#Delta#eta(t_{1}, t_{2})| dm^{t#bar{t}}";
    if(variableName.Contains("delta_y_vs_ttbar_mass")) variable="d|#Deltay(t_{1}, t_{2})| dm^{t#bar{t}}";
    if(variableName.Contains("delta_phi_vs_ttbar_mass")) variable="d|#Delta#phi(t_{1}, t_{2})| dm^{t#bar{t}}";
    if(variableName.Contains("t1_y_vs_t2_y")) variable="d|y^{t,1}| d|y^{t,2}|";
    if(variableName.Contains("delta_y_vs_t1_y")) variable="|d#Deltay(t_{1}, t_{2})| d|y^{t,1}|";
    if(variableName.Contains("t1_phi_vs_t1_eta")) variable="d#phi^{t,1} d#eta^{t,1}";
    if(variableName.Contains("t2_phi_vs_t2_eta")) variable="d#phi^{t,2} d#eta^{t,2}";
    // 3D spectra
    if(variableName.Contains("ttbar_y_vs_ttbar_mass_vs_t1_pt")) variable="d|y^{t#bar{t}}|dm^{t#bar{t}}dp_{T}^{t,1}";
    if(variableName.Contains("ttbar_y_vs_ttbar_pt_vs_ttbar_mass")) variable="d|y^{t#bar{t}}|dp_{T}^{t#bar{t}}dm^{t#bar{t}}";
    
    return variable;
  }
  
//----------------------------------------------------------------------

  TString getVariableLatex(const TString& variableName) {
    
    TString variable="";
    
    if(variableName.Contains("t1_m_over_pt")) variable="m^{t,1}/p_{T}^{t,1}"; 				
    if(variableName.Contains("t2_m_over_pt")) variable="m^{t,2}/p_{T}^{t,2}";
    if(variableName.Contains("delta_pt")) variable="#Delta p_{T}(t_{1}, t_{2})";
    if(variableName.Contains("total_cross_section")) variable="#sigma^{t#bar{t}}";
    if(variableName.Contains("randomTop_pt")) variable="p_{T}^{t}"; 				
    if(variableName.Contains("inclusive_top_pt")) variable="p_{T}^{t,incl}"; 				
    if(variableName.Contains("leadingTop_pt")) variable="p_{T}^{t,1}"; 				
    if(variableName.Contains("t1_pt")) variable="p_{T}^{t,1}"; 				
    if(variableName.Contains("subleadingTop_pt")) variable="p_{T}^{t,2}"; 				
    if(variableName.Contains("t2_pt")) variable="p_{T}^{t,2}"; 				
    if(variableName.Contains("randomTop_y")) variable="|y^{t}|";         				
    if(variableName.Contains("inclusive_top_y")) variable="|y^{t,incl}|";         				
    if(variableName.Contains("leadingTop_y")) variable="|y^{t,1}|";         				
    if(variableName.Contains("t1_y")) variable="|y^{t,1}|";         				
    if(variableName.Contains("subleadingTop_y")) variable="|y^{t,2}|";         				
    if(variableName.Contains("t2_y")) variable="|y^{t,2}|";         				
    if(variableName.Contains("ttbar_mass")) variable="m^{t#bar{t}}";     		
    if(variableName.Contains("ttbar_pt")) variable="p_{T}^{t#bar{t}}"; 		
    if(variableName.Contains("ttbar_y")) variable="|y^{t#bar{t}}|";    				
    if(variableName.Contains("ttbar_deltaphi")) variable="|#Delta #phi(t_{1}, t_{2})|"; 
    if(variableName.Contains("pout")) variable="|p_{out}^{t#bar{t}}|";
    if(variableName.Contains("y_boost")) variable="|y_{B}^{t#bar{t}}|";       
    if(variableName.Contains("chi_ttbar")) variable="#chi^{t#bar{t}}";          
    if(variableName.Contains("H_tt_pt")) variable="H_{T}^{t#bar{t}}";     	  
    if(variableName.Contains("cos_theta_star")) variable="|cos#theta^{*}|";
    if(variableName.Contains("z_tt_pt")) variable="z^{t#bar{t}}";      			  
    if(variableName.Contains("y_star")) variable="|y^{*}|";
    // 2D spectra
    if(variableName.Contains("t1_pt_vs_t2_pt")) variable="p_{T}^{t,1}#otimes p_{T}^{t,2}";
    if(variableName.Contains("t1_pt_vs_delta_pt")) variable="p_{T}^{t,1}#otimes #Delta p_{T}(t_{1}, t_{2})";
    if(variableName.Contains("t1_pt_vs_ttbar_pt")) variable="p_{T}^{t,1}#otimes p_{T}^{t#bar{t}}";
    if(variableName.Contains("t2_pt_vs_ttbar_pt")) variable="p_{T}^{t,2}#otimes p_{T}^{t#bar{t}}";
    if(variableName.Contains("top_pt_vs_ttbar_pt")) variable="p_{T}^{t,rndm}#otimes p_{T}^{t#bar{t}}";
    if(variableName.Contains("t1_pt_vs_ttbar_mass")) variable="p_{T}^{t,1}#otimes m^{t#bar{t}}";
    if(variableName.Contains("t2_pt_vs_ttbar_mass")) variable="p_{T}^{t,2}#otimes m^{t#bar{t}}";
    if(variableName.Contains("top_pt_vs_ttbar_mass")) variable="p_{T}^{t,rndm}#otimes m^{t#bar{t}}";
    if(variableName.Contains("ttbar_y_vs_t1_pt")) variable="|y^{t#bar{t}}|#otimes p_{T}^{t,1}";
    if(variableName.Contains("ttbar_y_vs_t2_pt")) variable="|y^{t#bar{t}}|#otimes p_{T}^{t,2}";
    if(variableName.Contains("t1_y_vs_t1_pt")) variable="|y^{t,1}|#otimes p_{T}^{t,1}";
    if(variableName.Contains("t2_y_vs_t2_pt")) variable="|y^{t,2}|#otimes p_{T}^{t,2}";
    if(variableName.Contains("ttbar_y_vs_top_pt")) variable="|y^{t#bar{t}}|#otimes p_{T}^{t,rndm}";
    if(variableName.Contains("ttbar_y_vs_ttbar_mass")) variable="|y^{t#bar{t}}|#otimes m^{t#bar{t}}";
    if(variableName.Contains("ttbar_y_vs_ttbar_pt")) variable="|y^{t#bar{t}}|#otimes p_{T}^{t#bar{t}}";
    if(variableName.Contains("top_y_vs_top_pt")) variable="|y^{t,rndm}|#otimes p_{T}^{t,rndm}";
    if(variableName.Contains("ttbar_y_vs_top_y")) variable="|y^{t#bar{t}}|#otimes |y^{t,rndm}|";
    if(variableName.Contains("ttbar_y_vs_t1_y")) variable="|y^{t#bar{t}}|#otimes |y^{t,1}|";
    if(variableName.Contains("ttbar_y_vs_t2_y")) variable="|y^{t#bar{t}}|#otimes |y^{t,2}|";
    if(variableName.Contains("t1_y_vs_ttbar_mass")) variable="|y^{t,1}|#otimes m^{t#bar{t}}";
    if(variableName.Contains("t2_y_vs_ttbar_mass")) variable="|y^{t,2}|#otimes m^{t#bar{t}}";
    if(variableName.Contains("top_y_vs_ttbar_mass")) variable="|y^{t,rndm}|#otimes m^{t#bar{t}}";
    if(variableName.Contains("ttbar_pt_vs_ttbar_mass")) variable="p_{T}^{t#bar{t}}#otimes m^{t#bar{t}}";
    if(variableName.Contains("delta_eta_vs_ttbar_mass")) variable="|#Delta#eta(t_{1}, t_{2})|#otimes m^{t#bar{t}}";
    if(variableName.Contains("delta_y_vs_ttbar_mass")) variable="|#Deltay(t_{1}, t_{2})|#otimes m^{t#bar{t}}";
    if(variableName.Contains("delta_phi_vs_ttbar_mass")) variable="|#Delta#phi(t_{1}, t_{2})|#otimes m^{t#bar{t}}";
    if(variableName.Contains("t1_y_vs_t2_y")) variable="|y^{t,1}|#otimes |y^{t,2}|";
    if(variableName.Contains("delta_y_vs_t1_y")) variable="|#Deltay(t_{1}, t_{2})|#otimes |y^{t,1}|";
    if(variableName.Contains("t1_phi_vs_t1_eta")) variable="#phi^{t,1}#otimes #eta^{t,1}";
    if(variableName.Contains("t2_phi_vs_t2_eta")) variable="#phi^{t,2}#otimes #eta^{t,2}";
    // 3D spectra
    if(variableName.Contains("ttbar_y_vs_ttbar_mass_vs_t1_pt")) variable="|y^{t#bar{t}}|#otimes m^{t#bar{t}}#otimes p_{T}^{t,1}";
    if(variableName.Contains("ttbar_y_vs_ttbar_pt_vs_ttbar_mass")) variable="|y^{t#bar{t}}|#otimes p_{T}^{t#bar{t}} #otimes m^{t#bar{t}}";
    
    return variable;
    
  }

//----------------------------------------------------------------------

  TString convertPDFset(const TString& name){
    TString converted="not_known";
    if (name=="MMHT") converted="MMHT2014nlo68clas118_LHgridweight";
    if (name=="CT14") converted="CT14nlo_as_0118_LHgridweight";
    if (name=="NNPDF") converted="NNPDF30_nlo_as_0118_LHgridweight";
    return converted;
  }
  
// ---------------------------------------------------------------------
  
  TString convertPDFsetForLHAPDF(const TString& name){
    TString converted="not_known";
    if (name=="MMHT") converted="MMHT2014nlo68clas118";
    if (name=="CT14") converted="CT14nlo_as_0118";
    if (name=="NNPDF") converted="NNPDF30_nlo_as_0118";
    return converted;
  }
  
  //----------------------------------------------------------------------

  std::vector<TString> getXtitles(const TString& variableName) {
    
    
    
    if (variableName.Contains("ttbar_y_vs_ttbar_mass_vs_t1_pt")) return {"|y^{t#bar{t}}|","m^{t#bar{t}} [TeV]","p_{T}^{t,1} [TeV]"};
    if (variableName.Contains("ttbar_y_vs_ttbar_pt_vs_ttbar_mass")) return {"|y^{t#bar{t}}|","p_{T}^{t#bar{t}} [TeV]","m^{t#bar{t}} [TeV]"};
    if (variableName.Contains("t1_pt_vs_t2_pt")) return { "p_{T}^{t,1} [TeV]","p_{T}^{t,2} [TeV]"};
    if (variableName.Contains("t1_pt_vs_delta_pt")) return { "p_{T}^{t,1} [TeV]","#Deltap_{T}(t_{1}, t_{2}) [TeV]"};
    if (variableName.Contains("t1_pt_vs_ttbar_pt")) return { "p_{T}^{t,1} [TeV]","p_{T}^{t#bar{t}} [TeV]"};
    if (variableName.Contains("t2_pt_vs_ttbar_pt")) return { "p_{T}^{t,2} [TeV]","p_{T}^{t#bar{t}} [TeV]"};
    if (variableName.Contains("top_pt_vs_ttbar_pt")) return { "p_{T}^{t,rndm} [TeV]","p_{T}^{t#bar{t}} [TeV]"};
    if (variableName.Contains("t1_pt_vs_ttbar_mass")) return {"p_{T}^{t,1} [TeV]","m^{t#bar{t}} [TeV]"};
    if (variableName.Contains("t2_pt_vs_ttbar_mass")) return {"p_{T}^{t,2} [TeV]","m^{t#bar{t}} [TeV]"};
    if (variableName.Contains("top_pt_vs_ttbar_mass")) return {"p_{T}^{t,rndm} [TeV]","m^{t#bar{t}} [TeV]"};
    if (variableName.Contains("ttbar_y_vs_t1_pt")) return {"|y^{t#bar{t}}|","p_{T}^{t,1} [TeV]"};
    if (variableName.Contains("ttbar_y_vs_t2_pt")) return {"|y^{t#bar{t}}|","p_{T}^{t,2} [TeV]"};
    if (variableName.Contains("t1_y_vs_t1_pt")) return {"|y^{t,1}|","p_{T}^{t,1} [TeV]"};
    if (variableName.Contains("t2_y_vs_t2_pt")) return {"|y^{t,2}|","p_{T}^{t,2} [TeV]"};
    if (variableName.Contains("ttbar_y_vs_top_pt")) return {"|y^{t#bar{t}}|","p_{T}^{t,rndm} [TeV]"};
    if (variableName.Contains("ttbar_y_vs_ttbar_mass")) return {"|y^{t#bar{t}}|","m^{t#bar{t}} [TeV]"};
    if (variableName.Contains("ttbar_y_vs_ttbar_pt")) return {"|y^{t#bar{t}}|","p_{T}^{t#bar{t}} [TeV]"};
    if(variableName.Contains("top_y_vs_top_pt")) return {"|y^{t,rndm}|","p_{T}^{t,rndm} [TeV]"};
    if(variableName.Contains("ttbar_y_vs_top_y")) return {"|y^{t#bar{t}}|","|y^{t,rndm}|"};
    if(variableName.Contains("ttbar_y_vs_t1_y")) return {"|y^{t#bar{t}}|","|y^{t,1}|"};
    if(variableName.Contains("ttbar_y_vs_t2_y")) return {"|y^{t#bar{t}}|","|y^{t,2}|"};
    if(variableName.Contains("t1_y_vs_ttbar_mass")) return {"|y^{t,1}|","m^{t#bar{t}} [TeV]"};
    if(variableName.Contains("t2_y_vs_ttbar_mass")) return {"|y^{t,2}|","m^{t#bar{t}} [TeV]"};
    if(variableName.Contains("top_y_vs_ttbar_mass")) return {"|y^{t,rndm}|","m^{t#bar{t}} [TeV]"};
    if(variableName.Contains("ttbar_pt_vs_ttbar_mass")) return {"p_{T}^{t#bar{t}} [TeV]","m^{t#bar{t}} [TeV]"};
    if(variableName.Contains("delta_eta_vs_ttbar_mass")) return {"|#Delta#eta(t_{1}, t_{2})|","m^{t#bar{t}} [TeV]"};
    if(variableName.Contains("delta_y_vs_ttbar_mass")) return {"|#Deltay(t_{1}, t_{2})|","m^{t#bar{t}} [TeV]"};
    if(variableName.Contains("delta_phi_vs_ttbar_mass")) return {"|#Delta#phi(t_{1}, t_{2})|","m^{t#bar{t}} [TeV]"};
    if(variableName.Contains("t1_y_vs_t2_y")) return {"|y^{t,1}|","|y^{t,2}|"};
    if(variableName.Contains("delta_y_vs_t1_y")) return {"|#Deltay(t_{1}, t_{2})|","|y^{t,1}|"};
    if(variableName.Contains("t1_phi_vs_t1_eta")) return {"#phi^{t,1}","#eta^{t,1}"};
    if(variableName.Contains("t2_phi_vs_t2_eta")) return {"#phi^{t,2}","#eta^{t,2}"};
    
    
    std::cout << "Error: functions::getXtitles: " << "Unknown variable name: " << variableName << std::endl;
    std::cout << "Returning empty vector" << std::endl;
    
    return {};
  }

//----------------------------------------------------------------------

  TString getXtitle(const TString& variableName) {
    
    TString xtitle{""};
    TString unit = getDifferentialVariableUnit(variableName);
    TString var = getVariableLatex(variableName);
    xtitle = var;
    if(unit!="") xtitle+=" [" + unit + "]";
    return xtitle;
  }

//----------------------------------------------------------------------

  void setLabelForNormalized(TH1* hist, const TString& label, const TString& unit){
    if(unit!=""){
      hist->SetYTitle("1/#sigma d#sigma/d"+label+" [1/"+unit+"]");
      //hist->SetYTitle("#frac{1}{#sigma}#frac{d#sigma}{d"+label+"} [#frac{1}{"+unit+"}]");
      hist->SetXTitle(label+" ["+unit+"]");
    }
    else{
      //cout << "Setting titles without unit" << endl;
      hist->SetYTitle("1/#sigma d#sigma/d"+label+" [1]");
      //hist->SetYTitle("#frac{1}{#sigma}#frac{d#sigma}{d"+label+"} [1]");
      hist->SetXTitle(label);
    }
        
  }
  
}
