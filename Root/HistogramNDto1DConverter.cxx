#include "TTbarHelperFunctions/HistogramNDto1DConverter.h"
#include "TTbarHelperFunctions/functions.h"
#include "TTbarHelperFunctions/stringFunctions.h"
#include "TTbarHelperFunctions/histManipFunctions.h"
#include "TTbarHelperFunctions/namesAndTitles.h"
#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"

#include <numeric>

ClassImp(HistogramNDto1DConverter)

HistogramNDto1DConverter::HistogramNDto1DConverter(const std::string& optBinningConfigFile ,int debugLevel) : m_dim(0), m_config(optBinningConfigFile.c_str()), m_configFile(optBinningConfigFile.c_str()), m_debug(debugLevel) {
  if(m_debug > 0) {
    std::cout << "Creating HistogramNDto1DConverter with config file name: " << optBinningConfigFile << std::endl;
  }
  
}

//----------------------------------------------------------------------

void HistogramNDto1DConverter::initializeOptBinning(const std::string& histNamePattern) {

  m_axisIDs.clear();
  m_internalAxisIDs.clear();
  m_optBinning.clear();
  
  if(m_debug > 1) {
    std::cout << "Initialize optimized binning with name pattern: " << histNamePattern << std::endl;
    
  } 
  
  while ( m_configFile.good() )
  {
    std::string line;
    getline (m_configFile,line);
    if(m_debug > 5) std::cout << line << std::endl;
    
    if(line.find("#")==0) continue;
    if(line.find(":")==std::string::npos) continue;
    
    if(line.find(histNamePattern) != std::string::npos) {
      std::size_t found = line.find(":");
      
      std::string axisID = line.substr(0,found);
      
      m_axisIDs.push_back(axisID);
      if(m_optBinning.count(axisID) > 0) {
        std::cout << "Error: Found two axes with the same IDs." << std::endl; 
        exit(-1);
      }
      m_optBinningTString[axisID]=functions::MakeVectorTString(m_config.GetValue(axisID.c_str(),""));
      m_optBinning[axisID]=functions::MakeVectorDouble(m_config.GetValue(axisID.c_str(),""));
    }
  }
  
  functions::sortVectorOfStrings(m_axisIDs);
  
  m_dim = getDimension(m_axisIDs); // Extracting dimension from the list of axis IDs
  m_internalBinPattern = getInternalBinPattern(m_axisIDs); // Extracting internal bin substring from the list of axis IDs

  makeListOfInternalAxisIDs(m_axisIDs); // This creates a smaller list of axis IDs corresponding to internal axes only
  makeAxisIndexes();
  
  
  m_xtitles = NamesAndTitles::getXtitles(histNamePattern);
  
}

//----------------------------------------------------------------------

std::vector<TString> HistogramNDto1DConverter::getExternalBinsInfo(int iproj, const TString& option) const {
  std::vector<TString> info(m_dim-1);
  std::string id = m_internalAxisIDs[iproj];
  int bin=-1;
  for(int i=m_dim-2;i>=0;i--) {
    id = findParrentAxisID(id,bin);
    const std::vector<TString>& binning = m_optBinningTString.at(id);
    if(option=="binning") info[i] = "[" + binning[bin-1] + "," + binning[bin] + "]";
    else if(option=="variable") info[i] = m_xtitles[i];
    else info[i] = binning[bin-1] + " < " + m_xtitles[i] + " < " + binning[bin];
  }
  return info;
}

//----------------------------------------------------------------------

void HistogramNDto1DConverter::makeAxisIndexes() {
  m_axisIndexes.resize(m_dim);
  std::string id = m_internalAxisIDs[0];
  const std::string delimiter="_binning_";
  std::size_t found = id.find(delimiter);
  std::string s = id.substr(found+delimiter.size());
  std::vector<std::string> vecstr = functions::splitString(s,"_");
  
  for(int i=0;i<m_dim-1;i++) {
    std::string subId = vecstr[i+2];
    functions::replaceAll(subId, "Ax", "");
    std::size_t found = subId.find("bin");
    m_axisIndexes[i]=subId.substr(0,found);
  }
  for(int i=0;i<m_dim;i++) {
    if(std::find(m_axisIndexes.begin(),m_axisIndexes.end(),std::to_string(i)) == m_axisIndexes.end()) {
      m_axisIndexes[m_dim-1] = std::to_string(i);
      break;
    }
  }
  if (m_debug > 5) {
    std::cout << "HistogramNDto1DConverter::makeAxisIndexes(): Found axis indexes: ";
    for(int i=0;i<m_dim;i++) {
      std::cout << m_axisIndexes[i] << " ";
    }
    std::cout << std::endl;
  }
}

//----------------------------------------------------------------------

int HistogramNDto1DConverter::getDimension(const vector<std::string>& axisIDs) const {
  
  int dim=0;
  for(const std::string& id : axisIDs) {
    const std::string delimiter="_binning_";
    std::size_t found = id.find(delimiter);
    std::string s = id.substr(found + delimiter.size());
    std::vector<std::string> vecstr = functions::splitString(s,"_");
    int newdim = vecstr.size() - 1;
    if( newdim>dim ) dim = newdim;
    if (m_debug > 10 ) std::cout << "HistogramNDto1DConverter::getDimension: " << "id: " << id << " substring: " << s << " vecstr.size(): " << vecstr.size() << " dim: " << dim << std::endl;
  }
  return dim;
}

//----------------------------------------------------------------------

std::string HistogramNDto1DConverter::getInternalBinPattern(const vector<std::string>& axisIDs) const {
  std::string internalBinPattern("");
  for(const std::string& id : axisIDs) {
    const std::string delimiter="_binning_";
    std::size_t found = id.find(delimiter);
    std::string s = id.substr(found+delimiter.size());
    std::vector<std::string> vecstr = functions::splitString(s,"_");
    int dim = vecstr.size() - 1;
    if (m_debug > 10 ) std::cout << "HistogramNDto1DConverter::getInternalBinPattern: " << "id: " << id << " substring: " << s << " vecstr.size(): " << vecstr.size() << " dim: " << dim << " m_dim: " << m_dim << std::endl;
    if( dim == m_dim ) {
      internalBinPattern = "_" + vecstr[0] + "_";
      if (m_debug > 4 ) std::cout << "HistogramNDto1DConverter::getInternalBinPattern: " << "Found internal bin pattern: " << internalBinPattern << std::endl;
      break;
    }
  }
  return internalBinPattern;
}

//----------------------------------------------------------------------

void HistogramNDto1DConverter::makeListOfInternalAxisIDs(const vector<std::string>& axisIDs) {
  m_internalAxisIDs.clear();
  for(const std::string& id : axisIDs) {
    if(id.find(m_internalBinPattern) != std::string::npos) {
      m_internalAxisIDs.push_back(id);
    }
  }
  functions::sortVectorOfStrings(m_internalAxisIDs);
}


//----------------------------------------------------------------------

void HistogramNDto1DConverter::initialize1DBinning(const binningConvention binSizeConvention) {
  m_binning_1D = get1DBinning(binSizeConvention);
}


//----------------------------------------------------------------------

void HistogramNDto1DConverter::print() const{
  std::cout << std::endl << "HistogramNDto1DConverter: Printing binning info." << std::endl << std::endl;
  
  std::cout << "Dimension: " << m_dim << " internal axis pattern: " << m_internalBinPattern << std::endl;
  
  
  for (const std::string& axisID : m_axisIDs){
    std::cout << axisID << ": ";
    
    vector<double> binning = m_optBinning.at(axisID);
    
    for (unsigned int i=0;i<binning.size()-1;i++) {
      std::cout << binning[i] <<";";
    }
    std::cout << binning.back() << std::endl;
    
  }
  std::cout << std::endl;
}

//----------------------------------------------------------------------

std::string HistogramNDto1DConverter::findParrentAxisID(const std::string& axisID,int& iParrentBin) const {
  /*
  Example:
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax0         // This is grandparrent
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax1_in_Ax0bin1         
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax1_in_Ax0bin2         // This is parrent: Function should return this value
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax1_in_Ax0bin3         
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax1_in_Ax0bin4         
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin1_Ax1bin1
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin1_Ax1bin2
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin1_Ax1bin3
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin1_Ax1bin4
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin2_Ax1bin1
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin2_Ax1bin2
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin2_Ax1bin3
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin2_Ax1bin4  // This is axisID: This enters the function. 
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin3_Ax1bin1
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin3_Ax1bin2
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin3_Ax1bin3
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin3_Ax1bin4
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin4_Ax1bin1
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin4_Ax1bin2
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin4_Ax1bin3
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin4_Ax1bin4
  */
  
  iParrentBin=-1;
  
  const std::string splitPattern="_binning_ax";
  std::vector<std::string> vecstr = functions::splitString(axisID,splitPattern);
  if(vecstr.size()!=2){
    std::cout << "Error in HistogramNDto1DConverter::findParrentAxisID: Unable to read axisID" << std::endl;
    exit(-1);
  }
  const std::string prefix = vecstr[0] + splitPattern;
  const std::string sufix = vecstr[1];
  
  vecstr = functions::splitString(sufix,"_");
  
  // Protection for incorrect settings
  if( (sufix == "") || (vecstr.size() == 1 && vecstr[0]!=m_axisIndexes[0]) || (vecstr.size() == 2)) {
    std::cout << "Error in HistogramNDto1DConverter::findParrentAxisID: Unable to read axisID" << std::endl;
    exit(-1);
  }
  
  if (vecstr.size() == 1 && vecstr[0]==m_axisIndexes[0]) {
    if(m_debug>0) std::cout << "HistogramNDto1DConverter::findParrentAxisID: axisID " << axisID << " has no parrent." << std::endl;
    return "";
  }
  
  int currentAxisPos = std::distance(m_axisIndexes.begin(),std::find(m_axisIndexes.begin(),m_axisIndexes.end(),vecstr[0]));
  int parrentAxisPos = currentAxisPos-1;
  
  // Making suffix for the parrent axis id
  std::string newsufix = m_axisIndexes[parrentAxisPos];
  if(vecstr.size() > 3) {
    newsufix+="_in";
  }
  for(unsigned int i=2;i<vecstr.size()-1;i++){
    newsufix+="_" + vecstr[i];
  }
  
  const std::string parrentAxisID = prefix + newsufix; 
  if(m_debug > 5) std::cout << "HistogramNDto1DConverter::findParrentAxisID: New parrent ID: " << parrentAxisID << std::endl;
  
  // Extracting parrent bin index
  std::string parrentBin = vecstr.back().substr(("Ax" + m_axisIndexes[parrentAxisPos] + "bin").length());
  iParrentBin = std::stoi(parrentBin);
  
  if( std::find(m_axisIDs.begin(),m_axisIDs.end(),parrentAxisID) == m_axisIDs.end() ){
    std::cout << "Error in HistogramNDto1DConverter::findParrentAxisID: Parrent axis ID is not on the list" << std::endl;
    exit(-1);
  }
  
  if(m_debug>5) {
    std::cout << "Found parrentAxisID: " << parrentAxisID  << " corresponding to parrent bin index: " << iParrentBin << std::endl;
  }
  return parrentAxisID;
}

//----------------------------------------------------------------------

std::vector<double> HistogramNDto1DConverter::getBinLowEdges(const std::string& axisID,int ibin) const {
  
  vector<double> binEdges(m_dim);
  
  std::string id=axisID;
  int bin=ibin;
  for(int k=m_dim-1;k>-1;k--){
    binEdges[k] = m_optBinning.at(id)[bin-1];
    if(k>0) {
      id = findParrentAxisID(id,bin);
      if(m_debug > 5) std::cout << "HistogramNDto1DConverter::getBinLowEdges: Axis id: " << id << " bin: " << bin << " index: " << k << std::endl; 
    }
  }
  
  return binEdges;
}

//----------------------------------------------------------------------

std::vector<double> HistogramNDto1DConverter::getBinUpEdges(const std::string& axisID,int ibin) const {
  
  vector<double> binEdges(m_dim);
  
  std::string id=axisID;
  int bin=ibin;
  for(int k=m_dim-1;k>-1;k--){
    binEdges[k] = m_optBinning.at(id)[bin];
    if(k>0) {
      id = findParrentAxisID(id,bin);
      if(m_debug > 5) std::cout << "HistogramNDto1DConverter::getBinUpEdges: Axis id: " << id << " bin: " << bin << " index: " << k << std::endl; 
    }
  }
  
  return binEdges;
}

//----------------------------------------------------------------------

double HistogramNDto1DConverter::getBinVolume(const std::string& axisID,int ibin,const binningConvention binSizeConvention) const {
  
  if(m_debug > 5) std::cout << "HistogramNDto1DConverter::getBinVolume: Input parameters: " << axisID << " " << ibin << std::endl;
  
  if (binSizeConvention==kVolume) {
    double volume=1.;
    std::string id=axisID;
    int bin=ibin;
    for(int k=m_dim-1;k>-1;k--){
      volume*= getBinWidth(id,bin);
      if(m_debug > 4) std::cout << "HistogramNDto1DConverter::getBinVolume: Axis id: " << id << " bin: " << bin << " index: " << k << " volume: " << volume << std::endl; 
      if (k > 0) {
        id = findParrentAxisID(id,bin);
      }
    }
    return volume;
  }
  else if (binSizeConvention==kInternalWidth) {
    return getBinWidth(axisID,ibin);
  }
  else if (binSizeConvention==kUnit) {
    return 1.;
  }
  return 0;
}

//----------------------------------------------------------------------

std::vector<double> HistogramNDto1DConverter::get1DBinning(const binningConvention binSizeConvention) const {
  
  std::vector<double> binning;
  //binning.push_back(m_optBinning.at(m_axisIDs.at(0))[0]);
  binning.push_back(0.);
  
  
  for(unsigned int iaxis = 0;iaxis < m_internalAxisIDs.size();iaxis++) {
    const std::string& axisID = m_internalAxisIDs[iaxis];
    const vector<double>& internalBinning = m_optBinning.at(axisID);
    
    //const int max = iaxis < (m_internalAxisIDs.size() - 1) ? internalBinning.size()-1 : internalBinning.size();
    //const int max = iaxis < (m_internalAxisIDs.size() - 1) ? internalBinning.size()-1 : internalBinning.size();
    const int max = internalBinning.size()-1;
    
    for(int bin = 1 ; bin <= max;bin++) {
      double binWidth(0.);
      
      //if(iaxis==0 && bin==1) binning.push_back(500.);
      
      
      if(binSizeConvention==kVolume) {
        binWidth=getBinVolume(axisID,bin);
      }
      else if (binSizeConvention==kInternalWidth) {
        binWidth=getBinWidth(axisID,bin);
      }
      else if (binSizeConvention==kUnit) {
        binWidth=1.;
      }
      binning.push_back(binning.back() + binWidth);
    }
  }
  
  if(m_debug > 5) for(int i=0;i<(int)binning.size();i++) {
    std::cout << " HistogramNDto1DConverter::get1DBinning:  index: " << i << " bin edge: " << binning[i] << std::endl;
  }
  
  
  return binning;
}

//----------------------------------------------------------------------

std::string HistogramNDto1DConverter::getAxisID(const int index,int& bin) const{
  if(m_debug > 4) std::cout << "Running HistogramNDto1DConverter::getAxisID" << std::endl;
  const int size = m_internalAxisIDs.size();
  std::cout << "size: " << size << " " << index << " " << bin << std::endl;
  int bincount(0);
  std::string id("");
  for(int i=0;i<size;i++) {
    id = m_internalAxisIDs[i];
    const int nbins = m_optBinning.at(id).size()-1;
    std::cout << "id: " << id << " " << nbins << " " << bincount << " " << index << std::endl;
    if (bincount + nbins < index ) {
      bincount+=nbins;
    }
    else {
      bin = index - bincount;
      break;
    }
  }
  return id;
}

//----------------------------------------------------------------------

std::string HistogramNDto1DConverter::getAxisID(const std::vector<double>& x,int& bin) const {
  /*
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax0:         0.0;0.3;0.7;1.2;2.0            // 
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax1_in_Ax0bin1:         1.;1.15;1.3;1.6;3.
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax1_in_Ax0bin2:         1.;1.15;1.3;1.6;3.
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax1_in_Ax0bin3:         1.;1.15;1.3;1.6;3.
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax1_in_Ax0bin4:         1.;1.15;1.3;1.6;3.
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin1_Ax1bin1:         500;550;600;700;1200
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin1_Ax1bin2:         500;550;600;700;1200
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin1_Ax1bin3:         500;550;600;700;1200
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin1_Ax1bin4:         500;550;600;700;1200
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin2_Ax1bin1:         500;550;600;700;1200
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin2_Ax1bin2:         500;550;600;700;1200
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin2_Ax1bin3:         500;550;600;700;1200
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin2_Ax1bin4:         500;550;600;700;1200
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin3_Ax1bin1:         500;550;600;700;1200
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin3_Ax1bin2:         500;550;600;700;1200
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin3_Ax1bin3:         500;550;600;700;1200
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin3_Ax1bin4:         500;550;600;700;1200
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin4_Ax1bin1:         500;550;600;700;1200
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin4_Ax1bin2:         500;550;600;700;1200
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin4_Ax1bin3:         500;550;600;700;1200
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin4_Ax1bin4:         500;550;600;700;1200  
  * 
  * Example: 
  * Get id for {0.4,1.4,720} 
  * Starting with id=ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax0
  * 0.4 is in the 2nd bin
  * id modified to id=tbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax0_in_Ax0bin2
  * 1.4 is in the 3rd bin
  * id modified to id=tbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax0_in_Ax0bin2_Ax1bin3
  * 720 is in the 4th bin
  * Finally, index is determined from getBinIndex(id,4);
  */
  
  if(m_debug > 4) std::cout << "Running HistogramNDto1DConverter::getAxisID" << std::endl;
  
  std::string id = m_axisIDs[0];
  std::string id_base = id.substr(0,id.length()-1);
  std::string id_suffix="";
  
  int ibin(-1);
  for(int i=0;i<m_dim;i++) {
    
    if(m_debug > 5) std::cout << "id: " << id << std::endl;
    
    const vector<double>& binning = m_optBinning.at(id);
    
    auto it = std::upper_bound(binning.begin(),binning.end(),x[i]);
    ibin = std::distance(binning.begin(),it);
    
    if(ibin <= 0 || (ibin > (int)binning.size()-1)) {
      if(m_debug > 5){
        std::cout << "Values are out of range of new histogram" << std::endl;
        std::cout << "Axis: " << i << " Low edge: " << x[i] << std::endl;
      }
      return "";
    }
    if(m_debug > 5) {
      std::cout << "Low edge: " << x[i] << " found bin: " << binning[ibin] << " ibin: " << ibin << std::endl;
    }
    
    if ( i==(m_dim-1)) continue;
    
    id_suffix+="_Ax"+std::to_string(i)+"bin"+std::to_string(ibin);
    
    id=id_base + std::to_string(i+1) + "_in" + id_suffix;
    
  }
  bin = ibin;
  return id;
}

//----------------------------------------------------------------------

int HistogramNDto1DConverter::getBinIndex(const vector<double>& x) const{
  int ibin(-1);
  std::string id = getAxisID(x,ibin);
  if(id=="") return -1;
  return getBinIndex(id,ibin);
}

//----------------------------------------------------------------------

int HistogramNDto1DConverter::getBinIndex(const std::string& axisID,const int ibin) const{
  /*
  Example:
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax0         
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax1_in_Ax0bin1         
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax1_in_Ax0bin2         
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax1_in_Ax0bin3         
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax1_in_Ax0bin4          
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin1_Ax1bin1  // index: ibin
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin1_Ax1bin2  // index: nbins1 + ibin
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin1_Ax1bin3  // index: nbins1 + nbins2 + ibin
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin1_Ax1bin4  // index: ...
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin2_Ax1bin1  // index: ...
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin2_Ax1bin2  // index: ...
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin2_Ax1bin3  // index: ...
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin2_Ax1bin4  // index: ...
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin3_Ax1bin1  // index: ...
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin3_Ax1bin2  // index: ...
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin3_Ax1bin3  // index: ...
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin3_Ax1bin4  // index: ...
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin4_Ax1bin1  // index: ...
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin4_Ax1bin2  // index: ...
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin4_Ax1bin3  // index: ...
  ttbar_y_vs_ttbar_mass_vs_t1_pt_Particle_reco_binning_ax2_in_Ax0bin4_Ax1bin4  // index: ...
  */
  
  // Get bin index of concatenated histogram
  // Index is assigned only to internal bin. It returns -1 otherwise.
  // Algorithm assumes that m_internalAxisIDs vector is sorted alphabetically
  
  std::vector<std::string>::const_iterator it = std::find(m_internalAxisIDs.begin(),m_internalAxisIDs.end(),axisID);
  if(it==m_internalAxisIDs.end()) {
    std::cout << "Error: Index does not correspond to any bin of the concatenated histogram" << std::endl;
    return -1;
  }
  int axisIndex = std::distance(m_internalAxisIDs.begin(),it); 
  
  int binIndex=0;
  
  for(int iaxis=0;iaxis<axisIndex;iaxis++) binIndex+=m_optBinning.at(m_internalAxisIDs[iaxis]).size()-1;
  
  binIndex+=ibin;
  
  return binIndex;
}

//----------------------------------------------------------------------

int HistogramNDto1DConverter::getBinIndex(const vector<int>& multiIndex) const {
  
  // Determining axis ID corresponding to internal bins
  std::string id = m_axisIDs[0] + "_in";
  for(int i=1;i<m_dim;i++) {
    id+="_Ax"+m_axisIndexes[i]+"bin"+std::to_string(multiIndex[i]);
  }
  
  return getBinIndex(id,multiIndex.back());
  
}

//----------------------------------------------------------------------

TH1D* HistogramNDto1DConverter::makeConcatenatedHistogram(const THnSparseD* input,const char* name) const {
  
  if(m_debug > 3) {
    std::cout << "Running HistogramNDto1DConverter::makeConcatenatedHistogram." << std::endl;
  }
  
  TH1D* output = new TH1D(name,"",m_binning_1D.size()-1,&m_binning_1D[0]);
  output->Sumw2();
  
  bool respectAxisRange=true;
  THnIter iterator(input,respectAxisRange); //
  std::vector<int> binId(m_dim);
  
  while (iterator.Next(&binId[0])>=0) {
    const double bincontent = input->GetBinContent(&binId[0]);    
    double binerror = input->GetBinError(&binId[0]);
    if(!std::isfinite(binerror)){
      std::cout << "Error in HistogramNDto1DConverter::makeConcatenatedHistogram: binerror is not finite." << std::endl;
      //std::cout << "Bin index: " << ibin << " binId:";
      for( int id : binId ) std::cout << " " << id;
      std::cout << std::endl;
      binerror = std::abs(bincontent); // This is probably due to missing Sumw2 when histograms were filled
    }
    
    // Getting low bin edges from original histogram
    vector<double> binCenters(m_dim);
    for(int i=0;i<m_dim;i++){
      binCenters[i] = input->GetAxis(i)->GetBinCenter(binId[i]);
    }
    
    if(m_debug > 4) {
      //std::cout << "Bin index: " << ibin << " binId:";
      for( int id : binId ) std::cout << " " << id;
      std::cout << " Bin content: " << bincontent << " Bin error: " << binerror <<  std::endl;
      std::cout << "Bin centers:";
      for( double center : binCenters ) std::cout << " " << center;
      std::cout << std::endl;
    }
    
    const int index = getBinIndex(binCenters);
    output->SetBinContent(index,output->GetBinContent(index) + bincontent);
    output->SetBinError(index,sqrt(pow(output->GetBinError(index),2) + pow(binerror,2)));
  
  }

  return output; 
}

//----------------------------------------------------------------------

TH2D* HistogramNDto1DConverter::make2DConcatenatedHistogram(const THnSparseD* input,const HistogramNDto1DConverter* truthBinningConverter,const char* name) const {
  
  if(m_debug > 3) {
    std::cout << "Running HistogramNDto1DConverter::make2DConcatenatedHistogram." << std::endl;
  }
  
  TH2D* output = new TH2D(name,"",m_binning_1D.size()-1,&m_binning_1D[0],truthBinningConverter->get1DBinningFast().size()-1,&truthBinningConverter->get1DBinningFast()[0]);
  output->Sumw2();
 
  const int dimReco = m_dim;
  const int dimTruth = truthBinningConverter->getDimension();
  const int dim = dimReco + dimTruth;
  
  if(dim!=input->GetNdimensions()){
    std::cout << "Error: input histogram is not consistent with HistogramNDto1DConverter setup." << std::endl;
    exit(-1);
  }
  
  bool respectAxisRange=true;
  THnIter iterator(input,respectAxisRange); //
  std::vector<int> binId(m_dim);
  
  while (iterator.Next(&binId[0]) >= 0) {  
    const double bincontent = input->GetBinContent(&binId[0]);    
    double binerror = input->GetBinError(&binId[0]);
    
    if(!std::isfinite(binerror)){
      std::cout << "Error in HistogramNDto1DConverter::makeConcatenatedHistogram: binerror is not finite." << std::endl;
      for( int id : binId ) std::cout << " " << id;
      std::cout << std::endl;
      binerror = std::abs(bincontent); // This is probably due to missing Sumw2 when histograms were filled
    }

    // Getting low bin edges from original histogram
    vector<double> binCentersReco(m_dim);
    vector<double> binCentersTruth(dimTruth);
    for(int i=0;i<dimReco;i++){
      binCentersReco[i] = input->GetAxis(i)->GetBinCenter(binId[i]);
    }
    for(int i=dimReco;i<dim;i++){
      binCentersTruth[i-dimReco] = input->GetAxis(i)->GetBinCenter(binId[i]); 
    }
    
    if(m_debug > 4) {
      for( int id : binId ) std::cout << " " << id;
      std::cout << " Bin content: " << bincontent << " Bin error: " << binerror <<  std::endl;
      std::cout << "Bin centers Reco:";
      for( double edge : binCentersReco ) std::cout << " " << edge;
      std::cout << std::endl;
      std::cout << "Bin centers Truth:";
      for( double edge : binCentersReco ) std::cout << " " << edge;
      std::cout << std::endl;
    }
    
    const int indexReco = getBinIndex(binCentersReco);
    const int indexTruth = truthBinningConverter->getBinIndex(binCentersTruth);
    
    output->SetBinContent(indexReco,indexTruth,output->GetBinContent(indexReco,indexTruth) + bincontent);
    output->SetBinError(indexReco,indexTruth,sqrt(pow(output->GetBinError(indexReco,indexTruth),2) + pow(binerror,2)));
  }
  
  return output; 
  
}

//----------------------------------------------------------------------

std::string HistogramNDto1DConverter::getProjectionName(int i, const char* name) const {
  return getProjectionName(m_internalAxisIDs.at(i),name);
}

//----------------------------------------------------------------------

std::string HistogramNDto1DConverter::getProjectionName(const std::string& id, const char* name) const {

  std::string proj_name = id;
  if (name) { 
    vector<string> helpvec = functions::splitString(proj_name,"_binning");
    if (helpvec.size()>=2) proj_name = name + helpvec[1];
    else proj_name = (std::string)name + "_" + id;
  } 
  else {
    functions::replaceAll(proj_name, "_binning", "");
  }
  return proj_name;
}

//----------------------------------------------------------------------

std::vector<TH1D*> HistogramNDto1DConverter::makeEmptyProjections(const char* name) const {
  const int size = m_internalAxisIDs.size();
  vector<TH1D*> output(size);
  // Making empty projection histograms
  for(int i=0;i<size;i++) {
    const std::string& id = m_internalAxisIDs[i];
    const std::string proj_name = getProjectionName(id,name);
    const std::vector<double>& binning = m_optBinning.at(id);
    output[i] = new TH1D(proj_name.c_str(),"",binning.size()-1,&binning[0]);
    output[i]->Sumw2();
  }
  
  return output;
}

//----------------------------------------------------------------------

std::vector<TH1D*> HistogramNDto1DConverter::makeProjections(const THnSparseD* input,const char* name) const {
  
  vector<TH1D*> output = makeEmptyProjections(name); // Empty projection histograms are made
  
  // Filling bin contents into projections histograms
  const long nbins_orig =  input->GetNbins();
  for(long ibin=1;ibin<=nbins_orig;ibin++) {
    std::vector<int> binId(m_dim);
    const double bincontent = input->GetBinContent(ibin,&binId[0]);
    const double binerror = input->GetBinContent(&binId[0]);
    vector<double> lowEdges(m_dim);
    for(int i=0;i<m_dim;i++){
      lowEdges[i] = input->GetAxis(i)->GetBinLowEdge(binId[i]);
    }
    
    int bin(-1);
    const string id = getAxisID(lowEdges,bin);
    
    auto it = std::find(m_internalAxisIDs.begin(),m_internalAxisIDs.end(),id);
    if(it==m_internalAxisIDs.end()){
      std::cout << "Error in HistogramNDto1DConverter::makeProjections: " << "Unknown bin ID: " << id << std::endl;
      exit(-1);
    }
    
    const int axis_index = std::distance(m_internalAxisIDs.begin(),it);
    output[axis_index]->SetBinContent(bin,output[axis_index]->GetBinContent(bin) + bincontent);
    output[axis_index]->SetBinError(bin, sqrt(pow(output[axis_index]->GetBinError(bin),2) + pow(binerror,2)));
  }
  
  return output;
}

//----------------------------------------------------------------------

std::vector<TH1D*> HistogramNDto1DConverter::makeProjections(const TH1D* input,const char* name) const {
  
  vector<TH1D*> output = makeEmptyProjections(name); // Empty projection histograms are made
  const int nproj = output.size();
  
  const long nbins_orig = input->GetNbinsX();
  long ibin_long=0;;
  
  for(int iproj=0;iproj<nproj;iproj++) {
    const int nbins = output[iproj]->GetNbinsX();
    for(int ibin=1;ibin<=nbins;ibin++) {
      ibin_long++;
      output[iproj]->SetBinContent(ibin, input->GetBinContent(ibin_long));
      output[iproj]->SetBinError(ibin, input->GetBinError(ibin_long));
    }
    
    output[iproj]->SetLineColor(input->GetLineColor());
    output[iproj]->SetLineStyle(input->GetLineStyle());
    output[iproj]->SetLineWidth(input->GetLineWidth());
    output[iproj]->SetMarkerColor(input->GetMarkerColor());
    output[iproj]->SetMarkerStyle(input->GetMarkerStyle());
    output[iproj]->SetMarkerSize(input->GetMarkerSize());
    
  }
  
  if(ibin_long!=nbins_orig) {
    std::cout << "Error in HistogramNDto1DConverter::makeProjections: No match in number of bins" << std::endl;
    exit(-1);
  }
  
  return output;
}

//----------------------------------------------------------------------

std::vector<TGraphAsymmErrors*> HistogramNDto1DConverter::makeProjections(const TGraphAsymmErrors* input,const char* name) const{ // Make projections from concatenated graph
  std::vector<TH1D*> histProj = makeEmptyProjections(name); // Empty projection histograms are made
  const int nproj = histProj.size();
  std::vector<TGraphAsymmErrors*> output(nproj);
  
  int binindex=0;
  for(int i=0;i<nproj;i++) {
    
    const int nbins= histProj[i]->GetNbinsX();
    vector<double> x(nbins);
    vector<double> y(nbins);
    vector<double> ex(nbins);
    vector<double> eyh(nbins);
    vector<double> eyl(nbins);
    TAxis* xaxis = histProj[i]->GetXaxis();
    for (int ibin=1;ibin<=nbins;ibin++) {
      int j=ibin-1;
      double xx(0.),yy(0.);
      input->GetPoint(binindex,xx,yy);
      x[j] = xaxis->GetBinCenter(ibin);
      ex[j] = xaxis->GetBinWidth(ibin)/2;
      y[j] = yy;
      eyh[j] = input->GetErrorYhigh(binindex);
      eyl[j] = input->GetErrorYlow(binindex);
      binindex++;
    }
    
    output[i] = new TGraphAsymmErrors(nbins,&x[0],&y[0],&ex[0],&ex[0],&eyl[0],&eyh[0]);
    
    output[i]->SetFillStyle(input->GetFillStyle());
    output[i]->SetFillColor(input->GetFillColor());
    output[i]->SetLineColor(input->GetLineColor());

    delete histProj[i];
  }
  
  return output;
}
//----------------------------------------------------------------------

void HistogramNDto1DConverter::divideByBinVolume(std::vector<TH1D*>& projections,const binningConvention binSizeConvention) const {
  // Projections are slices of N-dimensional differential cross-section. Bin contents should be divided by bin volume.
  const int nproj = projections.size();
  
  for(int iproj=0;iproj<nproj;iproj++) {
    const int nbins = projections[iproj]->GetNbinsX();
    const std::string& id = m_internalAxisIDs[iproj];
    for(int ibin=1;ibin<=nbins;ibin++) {
      const double binVolume = getBinVolume(id,ibin,binSizeConvention);
      projections[iproj]->SetBinContent(ibin, projections[iproj]->GetBinContent(ibin)/binVolume);
      projections[iproj]->SetBinError(ibin, projections[iproj]->GetBinError(ibin)/binVolume);
    }
  }
  
}

//----------------------------------------------------------------------

void HistogramNDto1DConverter::scaleProjections(std::vector<TH1D*>& projections,const double sf) const {
  const int nproj = projections.size();
  for(int iproj=0;iproj<nproj;iproj++) {
    projections[iproj]->Scale(sf);
  }
}

//----------------------------------------------------------------------

bool HistogramNDto1DConverter::checkConsistency(const THnSparseD* input) const {
  
  
  if (input->GetNdimensions () != m_dim) {
    std::cout << "Error in HistogramNDto1DConverter: Histogram " << input->GetName() << " did not passed a binning consitency test." << std::endl;
    return false;
  }
  
  for(const string& id : m_internalAxisIDs) {
    const int nInternalBins = m_optBinning.at(id).size()-1;
    for(int iInternalBin=1;iInternalBin<=nInternalBins;iInternalBin++) {
      
      std::vector<double> lowEdgesOpt = getBinLowEdges(id,iInternalBin);
      std::vector<double> upEdgesOpt = getBinUpEdges(id,iInternalBin);
      
      for(int i=0;i<m_dim;i++) {
        if(!functions::checkBinEdgeConsistency(input->GetAxis(i), lowEdgesOpt[i]) || !functions::checkBinEdgeConsistency(input->GetAxis(i), upEdgesOpt[i])) {
          std::cout << "Error in HistogramNDto1DConverter: Histogram " << input->GetName() << " did not passed a binning consitency test." << std::endl;
          std::cout << "Axis: " << i << " low edge: " << lowEdgesOpt[i] << " up edge: " << upEdgesOpt[i] << std::endl;
          return false;
        }
      } // Loop over axes
    } // Loop over bins of internal distributions
  } // Loop over internal distributions
  
  return true;
  
}

//----------------------------------------------------------------------

bool HistogramNDto1DConverter::checkConsistency(const TH1D* input) const {
  if ( input->GetNbinsX() != (int)(m_binning_1D.size() - 1) ) {
    std::cout << "Error in HistogramNDto1DConverter: Histogram " << input->GetName() << " did not passed a binning consitency test." << std::endl;
    return false;
  }
  return true;
}

