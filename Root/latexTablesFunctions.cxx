// Peter Berta, 15.10.2015

#include "TTbarHelperFunctions/functions.h"
#include "TTbarHelperFunctions/namesAndTitles.h"
#include "TTbarHelperFunctions/latexTablesFunctions.h"
#include "TTbarHelperFunctions/HistogramNDto1DConverter.h"

#include <algorithm>
#include <sstream>
#include <iostream>
#include <fstream>
#include <string>

#include "TLine.h"
#include "TPaveText.h"

using namespace std;

namespace latexTablesFunctions {

  void printCovarianceLatex(const TMatrixT<double> &matrix, const TString& variable,  const TString& fileName) {
    ofstream tex(fileName.Data(), std::ofstream::out);
    const int nbins = matrix.GetNcols();
    const TString nameLatex = NamesAndTitles::getVariableLatex(variable);
    
    tex << "\\begin{tabular}{|r|";
    for (int x=1;x<nbins;++x){
      tex << "r|";
    }
    tex << "}" << endl << "\\hline" << endl;
    tex << nameLatex;
    tex << fixed << setprecision(2);
    for (int x=1;x<=nbins;++x){
      tex << " & bin " << x;
    }
    tex << "  \\\\ \\hline" << endl;
    for (int x=1;x<=nbins;++x){
      tex << "bin " << x;
      for (int y=1;y<=nbins;++y){
        tex << " & " << matrix[x-1][y-1];
      }
      tex << " \\\\  \\hline" << endl;
    }
  
    tex << "\\end{tabular}" << endl;
    tex.close();
  }
  
//---------------------------------------------------------------------- 

  void printTableWithDiffCrossSection(const TString& fileName, const TString& variable, TH1D* centralValues, TGraphAsymmErrors* stat, TGraphAsymmErrors* allunc, const TString& normalization) {
    ofstream texfile(fileName.Data(), std::ofstream::out);
    const int nbins = centralValues->GetXaxis()->GetNbins();
    TString variableLatex = NamesAndTitles::getVariableLatex(variable);
    variableLatex.ReplaceAll("#","\\");
    
    TString unitX = NamesAndTitles::getDifferentialVariableUnit(variable);
    if(unitX!="") unitX = "$[\\rm " + unitX + "]$";
    
    const TString unitY = latexTablesFunctions::getDifferentialCrossSectionLatex(variable,normalization);
    
    texfile << setprecision(3);
    texfile << "\\begin{tabular}{|r|r|r|r|}" << endl;
    texfile << "\\hline" << endl;
    texfile << (TString)"Range "+unitX+" & " + unitY + " & Stat. unc. [\\%] & Total unc.  [\\%] \\\\ \\hline " << endl;
    
    for(int i=0;i<nbins;i++){
      const double centralValue=centralValues->GetBinContent(i+1);
      TString s = (TString)"$" + functions::printNumber(centralValue,allunc->GetErrorYhigh(i)) +"$";
      
      
      
      texfile << setprecision(5) <<  "$" << variableLatex <<  " \\in [" << centralValues->GetBinLowEdge(i+1) << "," << centralValues->GetBinLowEdge(i+2) << "]$";
      texfile << setprecision(3);
      texfile << " & " << s;
      texfile << " & " << Form("%2.1f",stat->GetErrorYhigh(i)/centralValue*100);
      texfile << " & " << Form("%2.1f",allunc->GetErrorYhigh(i)/centralValue*100);
      texfile << "\\\\ \\hline" << endl;
    }
 
    texfile << "\\end{tabular}";
    texfile.close();
  }

  void printTableWithDiffCrossSectionND(const TString& fileName, const TString& variable, TH1D* centralValues, TGraphAsymmErrors* stat, TGraphAsymmErrors* allunc, HistogramNDto1DConverter* histogramConverter, const TString& normalization) {
    ofstream texfile(fileName.Data(), std::ofstream::out);
    
    TString variableLatex = NamesAndTitles::getVariableLatex(variable);
    variableLatex.ReplaceAll("#","\\");
    
    TString unitX = NamesAndTitles::getDifferentialVariableUnit(variable);
    if(unitX!="") unitX = "$[\\rm " + unitX + "]$";
    
    const TString unitY = latexTablesFunctions::getDifferentialCrossSectionLatex(variable,normalization);
    
    vector<TH1D*> projections = histogramConverter->makeProjections(centralValues,"centralValues"); // Make projections from concatenated histogram
    vector<TGraphAsymmErrors*> statProjections = histogramConverter->makeProjections(stat,"stat"); // Make projections from concatenated histogram
    vector<TGraphAsymmErrors*> alluncProjections = histogramConverter->makeProjections(allunc,"allunc"); // Make projections from concatenated histogram
    const int nprojections = projections.size();
    
    
    
    texfile << setprecision(3);
    texfile << "\\begin{tabular}{|r|r|r|r|}" << endl;
    texfile << "\\hline" << endl;
    texfile << (TString)"Range "+unitX+" & " + unitY + " & Stat. unc. [\\%] & Total unc.  [\\%] \\\\ \\hline " << endl;
    
    for(int iproj=0;iproj<nprojections;iproj++) {
      
      const int nbins = projections[iproj]->GetXaxis()->GetNbins();
      
      const vector<TString>& externalBinsInfo = histogramConverter->getExternalBinsInfo(iproj, "binning");
      const int nExternalAxes=externalBinsInfo.size();
      
      for(int i=1;i<=nbins;i++){
        const double centralValue=projections[iproj]->GetBinContent(i);
        const double alluncValue = alluncProjections[iproj]->GetErrorYhigh(i-1);
        const double statValue = statProjections[iproj]->GetErrorYhigh(i-1);
        TString s = (TString)"$" + functions::printNumber(centralValue,alluncValue) +"$";
        
        stringstream internalBinInfo;
        internalBinInfo << "[" << projections[iproj]->GetBinLowEdge(i) << "," << projections[iproj]->GetBinLowEdge(i+1) << "]";
        
        
        texfile << setprecision(5) <<  "$" << variableLatex << " \\in " ;
        for(int iaxis=0;iaxis<nExternalAxes;iaxis++) texfile << externalBinsInfo[iaxis] << " \\otimes ";
        texfile << internalBinInfo.str() <<  "$";

        texfile << setprecision(3);
        texfile << " & " << s;
        texfile << " & " << Form("%2.1f",statValue/centralValue*100);
        texfile << " & " << Form("%2.1f",alluncValue/centralValue*100);
        texfile << "\\\\ \\hline" << endl;
      }
      
    }
    
    texfile << "\\end{tabular}";
    texfile.close();
    for(TH1D* proj : projections) delete proj;
    for(TGraphAsymmErrors* proj : statProjections) delete proj;
    for(TGraphAsymmErrors* proj : alluncProjections) delete proj;
    
  }
  
//----------------------------------------------------------------------

  TString getDifferentialCrossSectionLatex(const TString& variable, const TString& normalization) {
    
    const TString inverseUnit = NamesAndTitles::getInverseUnit(variable);
    TString diffVariable = NamesAndTitles::getDifferentialVariableLatex(variable);
    diffVariable.ReplaceAll("#","\\");
    TString diffXsecLatex = "";
    
    if(normalization=="absolute") {
      diffXsecLatex="$\\dfrac{d\\sigma}{" + diffVariable +"}";
      if(inverseUnit!="") diffXsecLatex += " [\\rm fb\\cdot\\rm " + inverseUnit+"]$";
      else diffXsecLatex  += " [\\rm fb]$";
    }
    else {
      diffXsecLatex="$\\dfrac{1}{\\sigma}\\dfrac{d\\sigma}{" + diffVariable +"}";
      if(inverseUnit!="") diffXsecLatex += "[\\rm " + inverseUnit+"]$";
      else diffXsecLatex  += "$";
    }
    
    return diffXsecLatex;
    
  }

} // namespace functions




