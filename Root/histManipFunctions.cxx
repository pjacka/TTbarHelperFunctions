#include "TTbarHelperFunctions/histManipFunctions.h"
#include "TTbarHelperFunctions/functions.h"
#include <algorithm>
#include "TLine.h"
#include "TPaveText.h"

namespace functions{

//----------------------------------------------------------------------

  void smearHistogram(TH1D* nominal,TH1D* smeared){
    const int n=nominal->GetNbinsX();
    for(int i=1;i<=n;i++){
      double x(-1);
      while(x<0)x=gRandom->Gaus(nominal->GetBinContent(i),nominal->GetBinError(i));
      smeared->SetBinContent(i,x);
    }
  }

//----------------------------------------------------------------------

  void coherentSmearing(const TH1D* reco, const TH2D* migra, const TH1D* truth, TH1D* recoSmeared, TH2D* migraSmeared, TH1D* truthSmeared) {
    
    const int nbinsReco=reco->GetNbinsX();
    const int nbinsTruth=truth->GetNbinsX();
    
    double mu,sigma;
    for(int i=1;i<=nbinsReco;i++) for(int j=1;j<=nbinsTruth;j++){
      mu=migra->GetBinContent(i,j);
      if(mu > 0){
        sigma=migra->GetBinError(i,j);
        migraSmeared->SetBinContent(i,j,functions::getRandomContent(mu,sigma));
        migraSmeared->SetBinError(i,j,sigma);
      }
      else {
        migraSmeared->SetBinContent(i,j,0);
        migraSmeared->SetBinError(i,j,0);
      }
    }
    
    double error;
    double integralSmeared;
    double integralNominal;
    
    for(int j=1;j<=nbinsTruth;j++) {
      mu = truth->GetBinContent(j);
      sigma = truth->GetBinError(j);
      integralNominal = migra->IntegralAndError(1,nbinsReco,j,j,error);
      integralSmeared = migraSmeared->Integral(1,nbinsReco,j,j);
      truthSmeared->SetBinContent(j,functions::getRandomContent(mu - integralNominal,sqrt(sigma*sigma - error*error)) + integralSmeared);
    }
    
    for(int j=1;j<=nbinsReco;j++) {
      mu = reco->GetBinContent(j);
      sigma = reco->GetBinError(j);
      integralNominal = migra->IntegralAndError(j,j,1,nbinsTruth,error);
      integralSmeared = migraSmeared->Integral(j,j,1,nbinsTruth);
      recoSmeared->SetBinContent(j,functions::getRandomContent(mu - integralNominal,sqrt(sigma*sigma - error*error)) + integralSmeared);
    }
  }


//----------------------------------------------------------------------

  void getAccNumerator(const TH2D* migra,TH1D* accNumerator) {
    const int nbinsx = migra->GetXaxis()->GetNbins();
    const int nbinsy = migra->GetYaxis()->GetNbins();
    
    if(nbinsx!=accNumerator->GetXaxis()->GetNbins()) {
      cout << "Error in getAccNumerator: Inconsistent binning" << endl;
      return;
    }
    
    double error;
    for(int i=1;i<=nbinsx;i++) {
      accNumerator->SetBinContent(i,migra->IntegralAndError(i,i,1,nbinsy,error));
      accNumerator->SetBinError(i,error);
    }
  }
  
//----------------------------------------------------------------------
  
  void getEffNumerator(const TH2D* migra,TH1D* effNumerator) {
    const int nbinsx = migra->GetXaxis()->GetNbins();
    const int nbinsy = migra->GetYaxis()->GetNbins();
    
    if(nbinsy!=effNumerator->GetXaxis()->GetNbins()) {
      cout << "Error in getEffNumerator: Inconsistent binning" << endl;
      return;
    }
    
    double error;
    for(int i=1;i<=nbinsy;i++) {
      effNumerator->SetBinContent(i,migra->IntegralAndError(1,nbinsx,i,i,error));
      effNumerator->SetBinError(i,error);
    }
    
  }

//----------------------------------------------------------------------

  TH1D* getEfficiency(TH1* passed, TH1* total){
    TH1D* eff=(TH1D*)passed->Clone();
    eff->Divide(passed,total,1,1,"b");   // the new way
    return eff;
  }

//----------------------------------------------------------------------

  THStack* makeTHStack(const TString& name,const std::vector<TH1D*> histos) {
    THStack* stack = new THStack(name,"");
    for(TH1* h : histos) {
      stack->Add(h);
    }
    return stack;
  }


//----------------------------------------------------------------------

  std::pair<double,double> getYRange(const vector<TH1D*>& hist,bool useLogScale,const double& spaceBelow,const double& spaceAbove) {
    
    const double& yminThreshold = useLogScale ? 0. : -FLT_MAX;
    double ymax=yminThreshold;
    double ymin=+FLT_MAX;
    
    const int nhistos=hist.size();
    for(int i=0;i<nhistos;i++){
      const int nbins=hist[i]->GetNbinsX();
      for(int ibin=1;ibin<=nbins;ibin++){
        const double& bincontent = hist[i]->GetBinContent(ibin);
        if(bincontent>yminThreshold) ymin=std::min(ymin,bincontent);
        ymax=std::max(ymax,bincontent);
      }
    }
    
    std::pair<double,double> yRange;
    if(useLogScale){
      yRange.first = ymin/pow(1.+ymax/ymin,spaceBelow);
      yRange.second = ymax*pow(1.+ymax/ymin,spaceAbove);
    }
    else{
      yRange.first = ymin - spaceBelow*(ymax - ymin);
      yRange.second = ymax + spaceAbove*(ymax - ymin);
    }
    return yRange;
  }

//----------------------------------------------------------------------

  void ChangeYRange(TH1* hist, float &yaxis_min, float &yaxis_max){
    for (int i=1;i<hist->GetNbinsX()+1;++i){
      float ibincontent=hist->GetBinContent(i);
      //cout << i << " binContent   "  << ibincontent  << "yaxis_min " << yaxis_min <<  "  yaxis_max " << yaxis_max<< endl;
      if (yaxis_max<ibincontent) yaxis_max=ibincontent;
      if (fabs(ibincontent)>1e-5 && yaxis_min>ibincontent) yaxis_min=ibincontent;
    }
  }

//----------------------------------------------------------------------

  double getMaximum(const TH1& hist,const double maxval) {
    
    double m = -FLT_MAX;
    const int n=hist.GetXaxis()->GetNbins();
    for(int i=1;i<=n;i++) {
      const double x = hist.GetBinContent(i);
      if(x<maxval) m = std::max(m,x);
    }
    return m;
    
  }
  
//----------------------------------------------------------------------
  
  double getMinimum(const TH1& hist,const double minval) {
    double m = FLT_MAX;
    const int n=hist.GetXaxis()->GetNbins();
    for(int i=1;i<=n;i++) {
      const double x = hist.GetBinContent(i);
      if(x>minval) m = std::min(m,x);
    }
    return m;
    
  }

//----------------------------------------------------------------------
  
  double getMaximum(const TH1* hist,const double maxval) {
    return getMaximum(*hist,maxval);
  }
  
//----------------------------------------------------------------------

  double getMinimum(const TH1* hist,const double minval) {
    return getMinimum(*hist,minval);
  }

//----------------------------------------------------------------------

  double getMaximum(const vector<TH1D*>& hist,const double maxval){
    double m = -FLT_MAX;
    for(const TH1D* h : hist) {
      m = std::max(functions::getMaximum(h,maxval),m);
    }
    return m;
  }
  
 //----------------------------------------------------------------------
 
  
  double getMinimum(const vector<TH1D*>& hist,const double minval) {
    double m = FLT_MAX;
    for(const TH1D* h : hist) {
      m = std::min(m,functions::getMinimum(h,minval));
    }
    return m;
  }

//----------------------------------------------------------------------

  double getMaximum(const TGraph* graph,const double maxval) {
    double m = -FLT_MAX;
    double x(0.),y(0.);
    const int n=graph->GetN();
    for(int i=0;i<n;i++) {
      graph->GetPoint(i,x,y);
      y+=graph->GetErrorYhigh(i);
      const double n = std::max(m,y);
      if(n<maxval) m = n;
    }
    return m;
  }
  
//----------------------------------------------------------------------

  double getMinimum(const TGraph* graph,const double minval) {
    double m = FLT_MAX;
    double x(0.),y(0.);
    const int n=graph->GetN();
    for(int i=0;i<n;i++) {
      graph->GetPoint(i,x,y);
      y-=graph->GetErrorYlow(i);
      const double n = std::min(m,y);
      if(n>minval) m = n;
    }
    return m;
  }

//----------------------------------------------------------------------
  
  double getMaximum(const vector<TGraphAsymmErrors*>& graphs,const double maxval) {
    double m = -FLT_MAX;
    for(const TGraph* graph : graphs) {
      m=std::max(m,getMaximum(graph,maxval));
    }
    return m;
  }
  
//----------------------------------------------------------------------

  double getMinimum(const vector<TGraphAsymmErrors*>& graphs,const double minval) {
    double m = FLT_MAX;
    for(const TGraph* graph : graphs) {
      m=std::min(m,getMinimum(graph,minval));
    }
    return m;
  }

//----------------------------------------------------------------------

  std::pair<double,double> getRange(const TAxis* axis) {
    const int nbins = axis->GetNbins();
    const double min = axis->GetBinLowEdge(1);
    const double max = axis->GetBinUpEdge(nbins);
    return {min,max};
  }

//----------------------------------------------------------------------

  std::pair<double,double> getXRange(const vector<TH1D*>& histos) {
    double xmin = FLT_MAX;
    double xmax = -FLT_MAX;
    for(TH1D* h : histos) {
      std::pair<double,double> range = getRange(h->GetXaxis());
      xmin = std::min(range.first,xmin);
      xmax = std::max(range.second,xmax);
    }
    return {xmin,xmax};
  }

//----------------------------------------------------------------------
  
  void RemoveNegatives(TH1* hist){
    for (int i=1;i<hist->GetNbinsX()+1;++i){
      if (hist->GetBinContent(i)<0) hist->SetBinContent(i,1e-100);
    }
  }

//----------------------------------------------------------------------

  void RemoveNegatives(TH2* hist){
    for (int i=1;i<hist->GetNbinsX()+1;++i){
      for (int j=1;j<hist->GetNbinsY()+1;++j){
        if (hist->GetBinContent(i,j)<0){
          hist->SetBinContent(i,j,1e-100);
          hist->SetBinError(i,j,1e-200);
        }
      }
    }
  }
  
//----------------------------------------------------------------------  
  
  void SetOverflowBinsToZero(TH2D* hist){
    for (int i=0;i<hist->GetNbinsX()+2;++i){
      hist->SetBinContent(i,0,0);
      hist->SetBinContent(i,hist->GetNbinsY()+1,0);
    }
    for (int i=0;i<hist->GetNbinsY()+2;++i){
      hist->SetBinContent(0,i,0);
      hist->SetBinContent(hist->GetNbinsX()+1,i,0);
    }
  }

//----------------------------------------------------------------------

  void SetErrorsToZero(TH1* hist){
    for (int i=1;i<hist->GetNbinsX()+1;++i){
      hist->SetBinError(i,0.000000001);
    }
  }

//----------------------------------------------------------------------

  void setBinErrors(TH1* hist, const TMatrixT<double>* covariance)	{
    const int nbins = hist->GetXaxis()->GetNbins();
    for(int i=0;i<nbins;i++) {
      hist->SetBinError(i+1,sqrt((*covariance)[i][i]));
    }
  }

//----------------------------------------------------------------------
  
  double getMean(TH1* h){
    return h->Integral()/(h->GetXaxis()->GetXmax() - h->GetXaxis()->GetXmin());
  }
  
//----------------------------------------------------------------------  
  
  double getMeanFromInterpolateFunction(TH1* h){
    int nbins = h->GetNbinsX();
    double res = h->GetBinContent(1)*(h->GetBinCenter(1) - h->GetXaxis()->GetXmin());
    // cout << res/(h->GetBinCenter(1) - h->GetXaxis()->GetXmin()) << endl;
    res+= h->GetBinContent(nbins)*(-h->GetBinCenter(nbins) + h->GetXaxis()->GetXmax());
    for(int i=2;i<=nbins;i++) res += (h->GetBinCenter(i) - h->GetBinCenter(i-1))*(h->GetBinContent(i) + h->GetBinContent(i-1))/2;
    return res/(h->GetXaxis()->GetXmax() - h->GetXaxis()->GetXmin());
  }

//----------------------------------------------------------------------

  vector<double> getArrayOfBins(int nBins,Double_t low, Double_t up){
    vector<double> array(nBins+1);
    Double_t width=(up-low)/(Double_t)nBins;
    for (int i=0;i<nBins+1;++i){
      array[i]=low+i*width;
    }
    return(array);
  }

//----------------------------------------------------------------------

  vector<double> getArrayOfBins(const string& s) {
    vector<double> binning = functions::MakeVectorDouble(s);
    if((binning.size() == 3)) {
      int nbins = binning[0]+0.5;
      if( nbins>0 && TMath::AreEqualRel(binning[0],nbins,1.E-12)) {
	binning = functions::getArrayOfBins(nbins,binning[1],binning[2]);
      }
    }
    return binning;
  }

//----------------------------------------------------------------------
  
  double* getArrayOfBins(vector<double>& vec){
    const int nBins=vec.size()-1;
    double *array=new double[vec.size()];
    for (int i=0;i<=nBins;++i){
      array[i]=vec[i];
    }
    return(array);
  }

//----------------------------------------------------------------------

  int getVectorOfBinEdges(TAxis* axis,vector<double>& vec){
    const int nbins=axis->GetNbins();
    vec.resize(nbins+1);
    vec[0]=axis->GetBinLowEdge(1);
    for(int i=1;i<=nbins;i++)vec[i]=axis->GetBinUpEdge(i);
    return nbins;
  }

//----------------------------------------------------------------------

  void getVectorOfBinContents(TH1D* h,vector<double>& vec){
    const int nbins=h->GetNbinsX();
    vec.resize(nbins);
    for(int i=0;i<nbins;i++)vec[i]=h->GetBinContent(i+1);
  }

//----------------------------------------------------------------------

  vector<double> binContentsFromHistogram(TH1* h) { // getting vector with bin contents
    const int nbins =h->GetNbinsX();
    vector<double> x(nbins);
    for(int i=0;i<nbins;i++)x[i]=h->GetBinContent(i+1);
    return x;
  }

//----------------------------------------------------------------------

  vector<double> binErrorsFromHistogram(TH1* h) { // getting vector with bin errors
    const int nbins =h->GetNbinsX();
    vector<double> x(nbins);
    for(int i=0;i<nbins;i++)x[i]=h->GetBinContent(i+1);
    return x;
  }

//----------------------------------------------------------------------

  TMatrixT<double> tMatrixFromHistogram(const TH1* h) {
    const int nbins = h->GetXaxis()->GetNbins();
    TMatrixT<double> m(1,nbins);
    for(int i=0;i<nbins;i++) m[0][i] = h->GetBinContent(i+1);
    return m;
  }
  
//----------------------------------------------------------------------
  
  TMatrixT<double> tMatrixFromVector(const vector<double>& vec) {
    const int nbins = vec.size();
    TMatrixT<double> m(1,nbins);
    for(int i=0;i<nbins;i++) m[0][i] = vec[i];
    return m;
  }
  
//----------------------------------------------------------------------
  
  TMatrixT<double> tMatrixFromVector(const TVectorD& vec) {
    const int nbins = vec.GetNrows();
    TMatrixT<double> m(1,nbins);
    for(int i=0;i<nbins;i++) m[0][i] = vec[i];
    return m;
  }

//----------------------------------------------------------------------

  void DivideByBinWidth(TH1* hist){
    for (int i=1;i<hist->GetNbinsX()+1;++i){
      hist->SetBinContent(i,hist->GetBinContent(i)/hist->GetBinWidth(i));
      hist->SetBinError(i,hist->GetBinError(i)/hist->GetBinWidth(i));
    }
  }

//----------------------------------------------------------------------

  void MultiplyByBinWidth(TH1* hist){
    for (int i=1;i<hist->GetNbinsX()+1;++i){
      hist->SetBinContent(i,hist->GetBinContent(i)*hist->GetBinWidth(i));
      hist->SetBinError(i,hist->GetBinError(i)*hist->GetBinWidth(i));
    }
  }  

//----------------------------------------------------------------------

  void ScaleInRange(TH1* hist,double sf, int imin,int imax){
    for(int i=imin;i<=imax;i++){
      hist->SetBinContent(i,hist->GetBinContent(i)*sf);
      hist->SetBinError(i,hist->GetBinError(i)*TMath::Abs(sf));
    }
  }

//----------------------------------------------------------------------

  void NormalizeConcatenatedHistogram(TH1* hist,vector<int>& nbins){
    int imin=1,imax=0;
    const int size=nbins.size();
    for(int i=0;i<size;i++){
      imax+=nbins[i];
      ScaleInRange(hist,1./hist->Integral(imin,imax),imin,imax);
      imin+=nbins[i];
    }
  }

//----------------------------------------------------------------------

  void ConvertToCrossSection(TH1* hist,float lumi, TString label, TString unit){
    hist->Scale(1./lumi);
    for (int ix=1;ix<hist->GetXaxis()->GetNbins()+1;++ix){
      hist->SetBinContent(ix, hist->GetBinContent(ix)/hist->GetBinWidth(ix));
      hist->SetBinError(ix, hist->GetBinError(ix)/hist->GetBinWidth(ix));
    }
    if(unit!=""){
      hist->GetYaxis()->SetTitle("d#sigma/d"+label+" [fb/"+unit+"]");
      hist->GetXaxis()->SetTitle(label+" ["+unit+"]");
    }
    else{
      //cout << "Setting titles without unit" << endl;
      hist->GetYaxis()->SetTitle("d#sigma/d"+label+" [fb]");
      hist->GetXaxis()->SetTitle(label);
    }
  }

//---------------------------------------------------------------------------------
  
  void NormalizeRows(TH2 *hist){
    for (int irow=0;irow<hist->GetNbinsY()+2;++irow){
      float sum=0;
      for (int icolumn=0;icolumn<hist->GetNbinsX()+2;++icolumn){
        if (hist->GetBinContent(icolumn,irow) < 0)  hist->SetBinContent(icolumn,irow,0);  // setting to 0 the negative bins!
        sum+=hist->GetBinContent(icolumn,irow);
      }
      if (sum < 1e-5) continue;
      for (int icolumn=0;icolumn<hist->GetNbinsX()+2;++icolumn){
        hist->SetBinContent(icolumn,irow,hist->GetBinContent(icolumn,irow)/sum);
        hist->SetBinError(icolumn,irow,hist->GetBinError(icolumn,irow)/sum);
      }
    }
    hist->GetZaxis()->SetTitle("normalized number of entries in rows");
  }
    
//----------------------------------------------------------------------

  void NormalizeColumns(TH2D *hist, bool use_overlow_underflow){
    int startingBin=0;
    int additionalBins=2;
    if (!use_overlow_underflow){
      startingBin=1;
      additionalBins=1;
    }
    
    TH2D* total=(TH2D*)hist->Clone();
    total->GetZaxis()->SetTitle("normalized number of entries in columns");
    for (int i=startingBin;i<hist->GetNbinsX()+additionalBins;++i){
      double sum=0;
      double squaredSum=0;
      for (int j=startingBin;j<hist->GetNbinsY()+additionalBins;++j){
        if (hist->GetBinContent(i,j) < 0)  hist->SetBinContent(i,j,0);  // setting to 0 the negative bins!
        sum+=hist->GetBinContent(i,j);
        squaredSum+=pow(hist->GetBinError(i,j),2.);
      }
      //if (sum < 1e-5) continue;
      for (int j=startingBin;j<hist->GetNbinsY()+additionalBins;++j){
        total->SetBinContent(i,j,sum);
        total->SetBinError(i,j,sqrt(squaredSum));
        //hist->SetBinContent(i,j,hist->GetBinContent(i,j)/sum);
        //hist->SetBinError(i,j,hist->GetBinError(i,j)/sum);
      }
    }
    
    hist->GetZaxis()->SetTitle("normalized number of entries in columns");
    hist->Divide(hist,total,1,1,"b");
  
  }
      
//----------------------------------------------------------------------

THnSparseD* makeTHnSparseDHistogram(const TString& name, const vector<TString>& axes_titles,const vector<vector<double> >& binning){
  const int dim = axes_titles.size();
  if(dim!=(int)binning.size()) {
    std::cout << "Error in functions::makeTHnSparseDHistogram: Name: " << name << " Found inconsistency in dimensions." << std::endl;
    return 0;
  }
  
  vector<int> nbins(dim);
  vector<double> minbin(dim);
  vector<double> maxbin(dim);
  for(int i=0;i<dim;i++){
    nbins[i]=binning[i].size()-1;
    maxbin[i]= *std::max_element(binning[i].begin(),binning[i].end());
    minbin[i]= *std::min_element(binning[i].begin(),binning[i].end());
    if (!functions::checkBinning(nbins[i],&binning[i][0])) {
      std::cout << "Error in functions::makeTHnSparseDHistogram: Name: " << name << " Found incorrect binning setup. Problem with " << i << "th axis." << std::endl;
    }
  }
  
  THnSparseD* h=new THnSparseD(name,"",dim,&nbins[0],&minbin[0],&maxbin[0]); // Making histogram with equidistant binning but with correct axes range
  for(int i=0;i<dim;i++){
    h->GetAxis(i)->Set(nbins[i],&binning[i][0]); // Redefinition of bin edges to get non-equidistant binning
    h->GetAxis(i)->SetTitle(axes_titles[i]); // Setting axes titles
  }
  h->Sumw2();
  return h;
}


//----------------------------------------------------------------------

  TH2D* Rebin2D(TH2* hist, int nbinsx, double* x, int nbinsy, double* y, TString name){
    TString changedName=hist->GetName()+name;
    TH2D* rebined=new TH2D(changedName+"_rebined",";"+(TString)(hist->GetXaxis()->GetTitle())+";"+(TString)(hist->GetYaxis()->GetTitle()),nbinsx,x,nbinsy,y);
    TH2D* rebined_content=new TH2D(changedName+"_rebined_content","",nbinsx,x,nbinsy,y);
    TH2D* rebined_squaredError=new TH2D(changedName+"_rebined_squaredError","",nbinsx,x,nbinsy,y);
    for (int i=1;i<hist->GetNbinsX()+1;++i){
      for (int j=1;j<hist->GetNbinsY()+1;++j){
        rebined_content->Fill(hist->GetXaxis()->GetBinCenter(i),hist->GetYaxis()->GetBinCenter(j),hist->GetBinContent(i,j));
        rebined_squaredError->Fill(hist->GetXaxis()->GetBinCenter(i),hist->GetYaxis()->GetBinCenter(j),hist->GetBinError(i,j)*hist->GetBinError(i,j));
      }
    }
    for (int i=1;i<rebined->GetNbinsX()+1;++i){
      for (int j=1;j<hist->GetNbinsY()+1;++j){
        rebined->SetBinContent(i,j,rebined_content->GetBinContent(i,j));
        rebined->SetBinError(i,j,sqrt(rebined_squaredError->GetBinContent(i,j)));
      }
    }
    
    /*    for (int i=1;i<hist->GetNbinsX()+1;++i){
          for (int j=1;j<hist->GetNbinsY()+1;++j){
          //cout << "i: " << i << " j: " << j << " bin content: " << hist->GetBinContent(i,j) << endl;
          rebined->Fill(hist->GetXaxis()->GetBinCenter(i),hist->GetYaxis()->GetBinCenter(j),hist->GetBinContent(i,j));
          }
          }*/  // the old way
    //rebined->Sumw2();
    delete rebined_content;
    delete rebined_squaredError;
    return rebined;
  }

//----------------------------------------------------------------------  

  THnSparseD* RebinND(THnSparseD* hist,vector<vector<double> >& bins,TString newname){
    // Warning: This function had never been tested!
    const int dimension=bins.size();
    if(dimension != 4) {
      throw TtbarDiffCrossSection_exception("Error in functions::Rebin4D: bins size is not 4!");
      return 0;
    }
    
    vector<int> nbins(dimension);
    vector<double> minbin(dimension),maxbin(dimension);
    
    vector<vector<double> > orig_bins(dimension);
    vector<int> orig_nbins(dimension);
    for(int i=0;i<dimension;i++) {
      orig_nbins[i]=getVectorOfBinEdges(hist->GetAxis(i),orig_bins[i]);
      nbins[i]=bins[i].size();
      if(orig_nbins[i]<nbins[i])throw TtbarDiffCrossSection_exception("Error in functions::RebinND: New binning has more bins than original binning!");
      for(int j=0;j<nbins[i]+1;j++){
        if(std::find(orig_bins[i].begin(), orig_bins[i].end(), bins[i][j]) == orig_bins[i].end())throw TtbarDiffCrossSection_exception("Error in functions::Rebin4D: New binning is not consistent with original binning (check bin edges)!");
      }
      minbin[i]=bins[i][0];
      maxbin[i]=bins[i].back();
    }
    
    THnSparseD* rebined = new THnSparseD(newname,"",dimension,&nbins[0],&minbin[0],&maxbin[0]);
    rebined->Sumw2();
    for(int i=0;i<dimension;i++){
      rebined->GetAxis(i)->Set(nbins[i],&bins[i][0]);
      rebined->GetAxis(i)->SetTitle(hist->GetAxis(i)->GetTitle());
    }
    rebined->SetTitle(hist->GetTitle());
    THnSparseD* binerrors = (THnSparseD*)rebined->Clone();
    const long nbinslong_orig=hist->GetNbins();
    vector<int> id(dimension);
    vector<int> orig_id(dimension);
    vector<double> bin_center(dimension);
    for(long i=1;i<=nbinslong_orig;i++){
      getBinID(i,dimension,nbins,orig_id);
      for(int d=0;d<dimension;d++){
        bin_center[d]=hist->GetAxis(d)->GetBinCenter(orig_id[d]);
      }
      cout << "functions::Rebin4D: BinNumber " << i << " " << hist->GetBin(&orig_id[0]) << " BinContent: " << hist->GetBinContent(i) << " " << hist->GetBinContent(&orig_id[0]) 
          << " BinError: " <<  hist->GetBinError(i) << " " << hist->GetBinError(&orig_id[0]) << endl;
      
      rebined->Fill(&bin_center[0],hist->GetBinContent(&orig_id[0]));
      binerrors->Fill(&bin_center[0],pow(hist->GetBinError(&orig_id[0]),2));
    }
    const long nbinslong=rebined->GetNbins();
    for(long i=1;i<=nbinslong;i++){
      rebined->SetBinError(i,sqrt(binerrors->GetBinContent(i)));
    }
    if(newname==""){
      TString name=hist->GetName();
      delete hist;
      rebined->SetName(name);
    }
    delete binerrors;
    return rebined;
  }

//----------------------------------------------------------------------

  void getBinID(long bin,int dimension,const vector<int>& nbins,vector<int>& binID){
    bin--;
    for(int d=0;d<dimension;d++){
      binID[d]=bin % nbins[d];
      bin=(bin-binID[d])/nbins[d];
      binID[d]++;
    }
  }

//----------------------------------------------------------------------

  void load2DBinning(TFile* f, TString variable_name,vector<double>& binning_x,vector<vector<double> >& binning_y,vector<double>& binning_x_truth,vector<vector<double> >& binning_y_truth){
    
    TVectorD* helpvec = (TVectorD*)f->Get("nominal/histInfo/"+variable_name+"_binning_x");
    const int nbinsx=helpvec->GetNrows()-1;
    binning_x.resize(nbinsx+1);
    for(int i=0;i<=nbinsx;i++)binning_x[i]=(*helpvec)[i];
    delete helpvec;
    binning_y.resize(nbinsx);
    for(int i=0;i<nbinsx;i++){
      TString name= "nominal/histInfo/"+variable_name+"_binning_y_inXbin" + Form("%i",i+1);
      helpvec = (TVectorD*)f->Get(name);
      const int nbinsy=helpvec->GetNrows()-1;
      binning_y[i].resize(nbinsy+1);
      for(int j=0;j<=nbinsy;j++)binning_y[i][j]=(*helpvec)[j];
      delete helpvec;
    }
    
    helpvec = (TVectorD*)f->Get("nominal/histInfo/"+variable_name+"_binning_truth_x");
    const int nbinsx_truth=helpvec->GetNrows()-1;
    binning_x_truth.resize(nbinsx_truth+1);
    for(int i=0;i<=nbinsx_truth;i++)binning_x_truth[i]=(*helpvec)[i];
    delete helpvec;
    binning_y_truth.resize(nbinsx_truth);
    for(int i=0;i<nbinsx_truth;i++){
      TString name= "nominal/histInfo/"+variable_name+"_binning_truth_y_inXbin" + Form("%i",i+1);
      helpvec = (TVectorD*)f->Get(name);
      const int nbinsy_truth=helpvec->GetNrows()-1;
      binning_y_truth[i].resize(nbinsy_truth+1);
      for(int j=0;j<=nbinsy_truth;j++)binning_y_truth[i][j]=(*helpvec)[j];
      delete helpvec;
    }
    
    
  }
  
//----------------------------------------------------------------------

  void makeConcatenatedHistogram(TH1D* input,TH1D*& output,vector<double>& binning_x,vector<vector<double> >& binning_y){
    
    int nbins=0;
    const int nbinsx=binning_x.size()-1;
    for(int i=0;i<nbinsx;i++)nbins+=binning_y[i].size()-1;
    
    if(nbins!=input->GetNbinsX() ) cout << "Error in functions::makeConcatenatedHistogram(): Different number in input histogram and new binning!" << endl;
    
    vector<double> binning(nbins+1);
    binning[0] = binning_y[0][0];
    int index=0;
    for(int i=0;i<nbinsx;i++){
      for(unsigned int j=1;j<binning_y[i].size();j++){
        index++;
        binning[index]=binning[index-1] + (binning_y[i][j]-binning_y[i][j-1]);
      }
    }
    
    //for(int i=0;i<=nbins;i++)cout << "Concatenated binning: " << i+1 << " " << binning[i] << endl;
    
    TString name = (TString)input->GetName()+"_concatenated";
    output = new TH1D(name,"",nbins,&binning[0]);
    output->Sumw2();
    for(int i=1;i<=nbins;i++){
      output->SetBinContent(i,input->GetBinContent(i));
      output->SetBinError(i,input->GetBinError(i));
      
    }
    
    copyHistStyle(input,output);
    output->GetXaxis()->SetTitle(input->GetXaxis()->GetTitle());
    output->GetYaxis()->SetTitle(input->GetYaxis()->GetTitle());

  }

//----------------------------------------------------------------------

  void copyHistStyle(const TH1* input,TH1* output) {
    output->SetLineStyle(input->GetLineStyle());
    output->SetLineWidth(input->GetLineWidth());
    output->SetLineColor(input->GetLineColor());
    output->SetFillColor(input->GetFillColor());
    output->SetMarkerColor(input->GetMarkerColor());
    output->SetMarkerSize(input->GetMarkerSize());
    output->SetMarkerStyle(input->GetMarkerStyle());
  }

//---------------------------------------------------------------------- 

  bool checkBinning(const int& nbins,const double* binning){
    for(int i=1;i<=nbins;i++){
      //cout << binning[i-1] << " " << binning[i] << " " << TMath::AreEqualRel(binning[i],binning[i-1],1.E-12) << endl;
      if(binning[i] <= binning[i-1] || TMath::AreEqualRel(binning[i],binning[i-1],1.E-12)){
        cout << "ERROR in functions::checkBinning: Binning is not in increasing order!"<< endl;
        return false;
      }
    }
    return true;
    
  }

//----------------------------------------------------------------------

  bool checkEquidistantBinning(const double& nbins,const double& xmin,const double& xmax){
    const int nBins = (int)(nbins+0.5);
    //cout << nbins << " " << nBins << " " << TMath::AreEqualRel(nbins,nBins,1.E-12) << " " << TMath::AreEqualRel(nbins,nBins,1.E-6) <<endl;
    if(!TMath::AreEqualRel(nbins,nBins,1.E-12)) {
      cout << "ERROR in functions::checkEquidistantBinning: nbins is not integer!"<< endl;
      return false; // Checking if nbins is integer
    }
    if(nBins < 1) {
      cout << "ERROR in functions::checkEquidistantBinning: nbins is not positive number!"<< endl;
      return false; // Checking if nbins is positive
    }
    if(xmax < xmin || TMath::AreEqualRel(xmin,xmax,1.E-12)){
      cout << "ERROR in functions::checkEquidistantBinning: xmax <= xmin!" << endl;
      return false; // checking if xmax > xmin
    }
    return true;
  }

//----------------------------------------------------------------------
  
  bool checkNewBinning1D(TAxis* axis, vector<double>& binning,const double max_dev){
    /// Checking if new binning is consistent with the original one (before rebinning).
    const int nbins_orig=axis->GetNbins();
    const int nbins_new=binning.size()-1;
    
    for(int j=0;j<=nbins_new;j++) {
      
      bool consistent = false;
      for(int i=0;i<=nbins_orig;i++) if( TMath::AreEqualRel( binning[j], axis->GetBinUpEdge(i), max_dev ) ) {
        consistent = true;
        break;
      }
      if ( !consistent ) return false;
    }
    return true;
  }

//----------------------------------------------------------------------

  bool checkNewBinning2D(TAxis* xaxis, TAxis* yaxis, vector<double>& binning_x, vector<vector<double> >& binning_y){
    
    if ( !checkNewBinning1D(xaxis,binning_x) ) return false;
    const int size=binning_x.size()-1;
    for(int i=0;i<size;i++) if ( !checkNewBinning1D(yaxis,binning_y[i]) ) return false;
    
    return true;
  }

//----------------------------------------------------------------------  
  
  bool checkForEmptyBins(TH2D* migration,TH1D* effOfTruthLevelCutsNominator,TH1D* signal_reco,TH1D* effOfRecoLevelCutsNominator,TH1D* signal_truth){
    bool reco_zero=checkForEmptyBins(effOfTruthLevelCutsNominator,signal_reco);
    bool truth_zero=checkForEmptyBins(effOfRecoLevelCutsNominator,signal_truth);
    bool migration_EmptyRow = checkForEmptyRow(migration);
    return reco_zero || truth_zero || migration_EmptyRow;
  }

//----------------------------------------------------------------------
  
  bool checkForEmptyBins(TH1D* nominator,TH1D* denominator){
    const int nbins=nominator->GetNbinsX();
    bool foundzero=false;
    for(int i=1;i<=nbins;i++){
      if(denominator->GetBinContent(i) <= 0. && nominator->GetBinContent(i) <= 0.){
        foundzero=true;
        nominator->SetBinContent(i,1);
        nominator->SetBinError(i,0);
        denominator->SetBinContent(i,1);
        denominator->SetBinError(i,0);
      }
    }
    
    return foundzero;
  }
  
//----------------------------------------------------------------------
  
  bool checkForEmptyRow(TH2D* migration){
    const int nbinsx=migration->GetNbinsX();
    const int nbinsy=migration->GetNbinsY();
    bool hasEmptyRow=false;
    bool emptyrow=true;
    for(int j=1;j<=nbinsy;j++){
      emptyrow=true;
      for(int i=1;i<=nbinsx;i++){
        if(migration->GetBinContent(i,j)>0){
          emptyrow=false;
          continue;
        }
      }
      if(emptyrow){
        migration->SetBinContent(j,std::min(j,nbinsy),1);
        migration->SetBinError(j,std::min(j,nbinsy),0);
        hasEmptyRow=true;
      }
    }
    return hasEmptyRow;
  }

//----------------------------------------------------------------------

  bool checkBinEdgeConsistency(const TAxis* ax, const double edge, const double relPrec) {
    
    const int nbins = ax->GetNbins();
    
    for(int i=0;i<=nbins;i++) {
      if(TMath::AreEqualRel (ax->GetBinUpEdge(i),edge,relPrec)) {
        return true;
      }
    }
    return false;
  }

//----------------------------------------------------------------------
  
  void create1DBinningFrom2DHistogram(TAxis* xaxis,TAxis* yaxis,vector<double>& binning){
    const int nbinsx=xaxis->GetNbins();
    const int nbinsy=yaxis->GetNbins();
    const int nbins=nbinsx*nbinsy;
    binning.resize(nbins+1);
    binning[0]=xaxis->GetBinLowEdge(1)*yaxis->GetBinLowEdge(1);
    for(int x=1;x<=nbinsx;x++)for(int y=1;y<=nbinsy;y++){
      int bin=(x-1)*nbinsy + y;
      binning[bin]=binning[bin-1]+xaxis->GetBinWidth(x)*yaxis->GetBinWidth(y); 
    }
    
    
  }
  
//----------------------------------------------------------------------  

  void create1DBinningFrom2DHistogram(vector<double>& binning, TVectorD& binning_x, vector<TVectorD>& binning_y){
    const int xsize=binning_x.GetNrows()-1;
    int nbins=0;
    for(int i=0;i<xsize;i++){
      const int ysize=binning_y[i].GetNrows()-1;
      for(int j=0;j<ysize;j++) nbins++;
    }
    binning.resize(nbins+1);
    int index=0;
    
    binning[0]=binning_x[0]*binning_y[0][0];
    for(int i=0;i<xsize;i++){
      const int ysize=binning_y[i].GetNrows()-1;
      for(int j=0;j<ysize;j++) {
        
        if(index==0)continue;
        binning[index] =  binning[index-1] + (binning_x[i+1] - binning_x[i])*(binning_y[i][j+1] - binning_y[i][j]);
        
        index++;
      }
    }
    int i=xsize-1;
    int j=binning_y[i].GetNrows()-1;
    binning[index] = binning[index-1] + (binning_x[i+1] - binning_x[i])*(binning_y[i][j+1] - binning_y[i][j]);
    
    
    
  }
  
//----------------------------------------------------------------------
  
  TH1D* convert2Dto1D(TH2D* input,TVectorD& binning_x, vector<TVectorD>& binning_y){
    vector<double> binning1D;
    create1DBinningFrom2DHistogram(binning1D,binning_x,binning_y);
    const int nbins1D=binning1D.size()-1;
    
    TH1D* output = new TH1D((TString)input->GetName()+"_convertedTo1D","",nbins1D,&binning1D[0]);
    
    
    return output;
  }

//----------------------------------------------------------------------
  
  TH1D* convert2Dto1D(TH2D* input,TH1D* htemplate){
    TH1D* output=(TH1D*)htemplate->Clone();
    const int nbinsx=input->GetNbinsX();
    const int nbinsy=input->GetNbinsY();
    for(int x=1;x<=nbinsx;x++)for(int y=1;y<=nbinsy;y++) {
      const int bin=(x-1)*nbinsy + y;
      output->SetBinContent(bin,input->GetBinContent(x,y));
      output->SetBinError(bin,input->GetBinError(x,y));
    }
    return output;
  }

//----------------------------------------------------------------------

  TH2D* convert1Dto2D(TH1D* input,TH2D* htemplate){
    TH2D* output=(TH2D*)htemplate->Clone();
    const int nbinsx=htemplate->GetNbinsX();
    const int nbinsy=htemplate->GetNbinsY();
    for(int x=1;x<=nbinsx;x++)for(int y=1;y<=nbinsy;y++){
      const int bin=(x-1)*nbinsy + y;
      output->SetBinContent(x,y,input->GetBinContent(bin));
      output->SetBinError(x,y,input->GetBinError(bin));
    }
    return output;
  }

//----------------------------------------------------------------------
  
  TH2D* convert4Dto2D(THnSparseD* input,TH2D* htemplate){
    TH2D* output=(TH2D*)htemplate->Clone();
    const int nbinsx_reco=input->GetAxis(0)->GetNbins();
    const int nbinsy_reco=input->GetAxis(1)->GetNbins();
    const int nbinsx_truth=input->GetAxis(2)->GetNbins();
    const int nbinsy_truth=input->GetAxis(3)->GetNbins();
    for(int xreco=1;xreco<=nbinsx_reco;xreco++)for(int yreco=1;yreco<=nbinsy_reco;yreco++){ 
      int bin_reco=(xreco-1)*nbinsy_reco+yreco;
      for(int xtruth=1;xtruth<=nbinsx_truth;xtruth++)for(int ytruth=1;ytruth<=nbinsy_truth;ytruth++){
        int bin_truth=(xtruth-1)*nbinsy_truth+ytruth;
        int bin4D[4]{xreco,yreco,xtruth,ytruth};
        output->SetBinContent(bin_reco,bin_truth,input->GetBinContent(bin4D));
        output->SetBinError(bin_reco,bin_truth,input->GetBinError(bin4D));
      }
    }
    return output;
  }

//----------------------------------------------------------------------

  THnSparseD* convert2Dto4D(TH2D* input,THnSparseD* htemplate){
    THnSparseD* output=(THnSparseD*)htemplate->Clone();
    const int nbinsx_reco=htemplate->GetAxis(0)->GetNbins();
    const int nbinsy_reco=htemplate->GetAxis(1)->GetNbins();
    const int nbinsx_truth=htemplate->GetAxis(2)->GetNbins();
    const int nbinsy_truth=htemplate->GetAxis(3)->GetNbins();
    for(int xreco=1;xreco<=nbinsx_reco;xreco++)for(int yreco=1;yreco<=nbinsy_reco;yreco++){ 
      int bin_reco=(xreco-1)*nbinsy_reco+yreco;
      for(int xtruth=1;xtruth<=nbinsx_truth;xtruth++)for(int ytruth=1;ytruth<=nbinsy_truth;ytruth++){
        int bin_truth=(xtruth-1)*nbinsy_truth+ytruth;
        int bin4D[4]{xreco,yreco,xtruth,ytruth};
        output->SetBinContent(bin4D,input->GetBinContent(bin_reco,bin_truth));
        output->SetBinError(bin4D,input->GetBinError(bin_reco,bin_truth));
      }
    }
    return output;
  }

//----------------------------------------------------------------------
  
  TH1D* createEmptyConcatenatedHistogram(vector<TH1D*>& histos){
    const int nhistos=histos.size();
    int nbins_all(0);
    for(int i=0;i<nhistos;i++)nbins_all+=histos[i]->GetNbinsX();
    vector<double> binning(nbins_all+1);
    binning[0]=histos[0]->GetXaxis()->GetBinLowEdge(1);
    int k=0;
    for(int i=0;i<nhistos;i++){
      const int nbins=histos[i]->GetNbinsX();
      for(int j=1;j<=nbins;j++){
        k++;
        binning[k]=binning[k-1] + histos[i]->GetBinWidth(j);
      }
    }
    TH1D* h=new TH1D("","",nbins_all,&binning[0]);
    
    
    return h;
  }

//----------------------------------------------------------------------

  void FillConcatenatedHistogram(TH1D* h,vector<TH1D*>& histos){
    const int nhistos=histos.size();
    int k=0;
    for(int i=0;i<nhistos;i++){
      const int nbins=histos[i]->GetNbinsX();
      for(int j=1;j<=nbins;j++){
        k++;
        h->SetBinContent(k,histos[i]->GetBinContent(j));
        h->SetBinError(k,histos[i]->GetBinError(j));
      }
    }
    
  }
  
//----------------------------------------------------------------------

  TH2D* createEmptyConcatenatedHistogram(vector<TH2D*>& histos){
    const int nhistos=histos.size();
    int nbinsx_all(0);
    int nbinsy_all(0);
    for(int i=0;i<nhistos;i++){
      nbinsx_all+=histos[i]->GetNbinsX();
      nbinsy_all+=histos[i]->GetNbinsY();
    }
    vector<double> binningx(nbinsx_all+1);
    vector<double> binningy(nbinsy_all+1);
    binningx[0]=histos[0]->GetXaxis()->GetBinLowEdge(1);
    binningy[0]=histos[0]->GetYaxis()->GetBinLowEdge(1);
    int k=0;
    int l=0;
    for(int i=0;i<nhistos;i++){
      const int nbinsx=histos[i]->GetNbinsX();
      const int nbinsy=histos[i]->GetNbinsY();
      for(int j=1;j<=nbinsx;j++){
        k++;
        binningx[k]=binningx[k-1] + histos[i]->GetXaxis()->GetBinWidth(j);
      }
      for(int j=1;j<=nbinsy;j++){
        l++;
        binningy[l]=binningx[l-1] + histos[i]->GetYaxis()->GetBinWidth(j);
      }
    }
    TH2D* h=new TH2D("","",nbinsx_all,&binningx[0],nbinsy_all,&binningy[0]);
    return h;
  }
  
//----------------------------------------------------------------------

  void FillConcatenatedHistogram(TH2D* h,vector<TH2D*>& histos){
    const int nhistos=histos.size();
    int k=0,l=0,ll=0;
    for(int i=0;i<nhistos;i++){
      const int nbinsx=histos[i]->GetNbinsX();
      const int nbinsy=histos[i]->GetNbinsY();
      for(int x=1;x<=nbinsx;x++){
        k++;
        l=ll;
        for(int y=1;y<=nbinsy;y++){
          l++;
          //cout << x << " " << y << " " << k << " " << l << endl;
          h->SetBinContent(k,l,histos[i]->GetBinContent(x,y));
          h->SetBinError(k,l,histos[i]->GetBinError(x,y));
        }
      }
      ll+=nbinsy;
    }
  } 

//----------------------------------------------------------------------  
  
  TH1DBootstrap* createEmptyConcatenatedHistogram(vector<TH1DBootstrap*>& histos){
    const int nhistos=histos.size();
    int nbins_all(0);
    for(int i=0;i<nhistos;i++)nbins_all+=histos[i]->GetNominal()->GetNbinsX();
    vector<double> binning(nbins_all+1);
    binning[0]=histos[0]->GetNominal()->GetXaxis()->GetBinLowEdge(1);
    int k=0;
    for(int i=0;i<nhistos;i++){
      const int nbins=histos[i]->GetNominal()->GetNbinsX();
      for(int j=1;j<=nbins;j++){
        k++;
        binning[k]=binning[k-1] + histos[i]->GetNominal()->GetBinWidth(j);
      }
    }
    TH1DBootstrap* h=new TH1DBootstrap("","",nbins_all,&binning[0],histos[0]->GetNReplica());
    return h;
  }

//----------------------------------------------------------------------

  void FillConcatenatedHistogram(TH1DBootstrap* h,vector<TH1DBootstrap*>& histos){
    const int nhistos=histos.size();
    const int nReplica=histos[0]->GetNReplica();
    vector<TH1D*> helpvec(nhistos);
    for(int i=0;i<nhistos;i++)helpvec[i]=(TH1D*)histos[i]->GetNominal();
    FillConcatenatedHistogram((TH1D*)h->GetNominal(),helpvec);
    
    for(int iReplica=0;iReplica<nReplica;iReplica++){
      for(int i=0;i<nhistos;i++)helpvec[i]=(TH1D*)histos[i]->GetReplica(iReplica);
      FillConcatenatedHistogram((TH1D*)h->GetReplica(iReplica),helpvec);
    }
  }


//----------------------------------------------------------------------

  void copyBinContents(TH1* hInput,TH1* hOutput){
    const int nbins=hInput->GetNbinsX();
    for(int i=1;i<=nbins;i++)hOutput->SetBinContent(i,hInput->GetBinContent(i));
  }

//----------------------------------------------------------------------

  void copyBinContents(TH2* hInput,TH2* hOutput){
    const int nbinsx=hInput->GetNbinsX();
    const int nbinsy=hInput->GetNbinsY();
    for(int i=1;i<=nbinsx;i++)for(int j=1;j<=nbinsy;j++)hOutput->SetBinContent(i,j,hInput->GetBinContent(i,j));
  }

//----------------------------------------------------------------------

  void copyBinContentsAndErrors(TH1* hInput,TH1* hOutput){
    const int nbins=hInput->GetNbinsX();
    for(int i=1;i<=nbins;i++){
      hOutput->SetBinContent(i,hInput->GetBinContent(i));
      hOutput->SetBinError(i,hInput->GetBinError(i));
    }
  }

//----------------------------------------------------------------------

  void copyBinContentsAndErrors(TH2* hInput,TH2* hOutput){
    const int nbinsx=hInput->GetNbinsX();
    const int nbinsy=hInput->GetNbinsY();
    for(int i=1;i<=nbinsx;i++)for(int j=1;j<=nbinsy;j++){
      hOutput->SetBinContent(i,j,hInput->GetBinContent(i,j));
      hOutput->SetBinError(i,j,hInput->GetBinError(i,j));
    }
  }

//----------------------------------------------------------------------

  void copyBinContentsAndErrors(TH1* hMeas,TH1* hTruth,TH2* hMig,RooUnfoldResponse* response){
    copyBinContentsAndErrors(hMeas,response->Hmeasured());
    copyBinContentsAndErrors(hTruth,response->Htruth());
    copyBinContentsAndErrors(hMig,response->Hresponse());
  }

//----------------------------------------------------------------------

  TH2D* ProduceHistogramWithSwitchedAxis(const TH2* hist){
    TString name=hist->GetName();
    TH2D* hist_switchedAxis=new TH2D(name+"_switchedAxis","",hist->GetYaxis()->GetNbins(),hist->GetYaxis()->GetXbins()->GetArray(),hist->GetXaxis()->GetNbins(),hist->GetXaxis()->GetXbins()->GetArray());
    hist_switchedAxis->GetXaxis()->SetTitle(hist->GetYaxis()->GetTitle());
    hist_switchedAxis->GetYaxis()->SetTitle(hist->GetXaxis()->GetTitle());
    for (int ix=0;ix<hist->GetXaxis()->GetNbins()+2;++ix){
      for (int iy=0;iy<hist->GetYaxis()->GetNbins()+2;++iy){
        double content=hist->GetBinContent(ix,iy);
        double error=hist->GetBinError(ix,iy);
        hist_switchedAxis->SetBinContent(iy,ix,content);
        hist_switchedAxis->SetBinError(iy,ix,error);
      }
    }
    return hist_switchedAxis;
  }

//----------------------------------------------------------------------

  TGraphAsymmErrors* graphFromHist(const TH1* h) {
    const int n = h->GetXaxis()->GetNbins();
    TGraphAsymmErrors* graph = new TGraphAsymmErrors(h);
    for (int i=0;i<n;++i){
      graph->SetPointEXlow(i,h->GetXaxis()->GetBinWidth(i+1)/2);
      graph->SetPointEXhigh(i,h->GetXaxis()->GetBinWidth(i+1)/2);
    }
    return graph;
  }


//----------------------------------------------------------------------

  TH1D* histFromGraph(const TGraphAsymmErrors& gr) {
    std::vector<double> binEdges;
    
    binEdges.push_back(gr.GetPointX(0) - gr.GetErrorXlow(0));
    const int nbins = gr.GetN();
  
    for(int i = 0;i<nbins;i++) {
      binEdges.push_back(gr.GetPointX(i) + gr.GetErrorXhigh(i));
    }
    
    auto h = new TH1D((TString)gr.GetName() + "_h","",nbins,&binEdges[0]);
    h->GetXaxis()->SetTitle(gr.GetXaxis()->GetTitle());
    h->GetYaxis()->SetTitle(gr.GetYaxis()->GetTitle());
    h->Sumw2();
    
  
    for(int i = 0;i<nbins;i++) {
      const double bincontent = gr.GetPointY(i);
      const double binerror = 0.5*(gr.GetErrorYlow(i) + gr.GetErrorYhigh(i));
      h->SetBinContent(i+1,bincontent);
      h->SetBinError(i+1,binerror);
    }
    
    return h;
  }


//----------------------------------------------------------------------

  TGraphAsymmErrors* getUnitGraph(TGraphAsymmErrors* graph){
    TGraphAsymmErrors* unit=(TGraphAsymmErrors*)graph->Clone();
    for (int i=0;i<graph->GetN();++i){
      double x,y;
      graph->GetPoint(i,x,y);
      unit->SetPoint(i,x,1);
      double low=unit->GetErrorYlow(i);
      double high=unit->GetErrorYhigh(i);
      //    cout << "i: " << i << "  x: " << x << "  y: " << y << "  ey_low: " << low << "  ey_high: " << high << endl;
      double rel_low=low/y;
      double rel_high=high/y;
      if (low < 0 || y<1e-5) rel_low=0;
      if (high < 0 || y<1e-5) rel_high=0;
      // use this for ratio Data/Pred. in the ratio plots:
      unit->SetPointEYlow(i,rel_low);
      unit->SetPointEYhigh(i,rel_high);
      // use this for ratio Pred./Data in the ratio plot:
      //unit->SetPointEYlow(i,rel_high);
      //unit->SetPointEYhigh(i,rel_low);
    }
    return unit;
  }

//----------------------------------------------------------------------

  void scaleGraphY(TGraphAsymmErrors* graph,const double sf) {
    const int n=graph->GetN();
    for (int i=0;i<n;i++) {
      graph->GetY()[i] *= sf;
      graph->SetPointEYlow(i,graph->GetErrorYlow(i)*sf);
      graph->SetPointEYhigh(i,graph->GetErrorYhigh(i)*sf);
    }
    
  }

//----------------------------------------------------------------------

  void scaleGraphErrors(TGraphAsymmErrors* graph,const double sf) {
    const int n=graph->GetN();
    for (int i=0;i<n;i++) {
      graph->SetPointEYlow(i,graph->GetErrorYlow(i)*sf);
      graph->SetPointEYhigh(i,graph->GetErrorYhigh(i)*sf);
    }
  }
  
//----------------------------------------------------------------------

  void changeUnits(TH1& h,const double sf,const TString& option) {
    
    TAxis* xaxis = h.GetXaxis();
    const int nbins=xaxis->GetNbins();
    std::vector<double> edges(nbins+1);
    edges[0]=xaxis->GetBinLowEdge(1)*sf;
    for(int i=1;i<=nbins;i++) edges[i] = xaxis->GetBinUpEdge(i)*sf;

    if(option=="width") {
      for(int i=1;i<=nbins;i++) {
	h.SetBinContent(i,h.GetBinContent(i)/sf);
	h.SetBinError(i,h.GetBinError(i)/sf);
      }
    }

    xaxis->Set(nbins,&edges[0]); // Redefinition of bin edges
    
  }

//----------------------------------------------------------------------

  void changeUnits(TGraphAsymmErrors* graph,const double sf) {
    const int n=graph->GetN();
    double x(0),y(0);
    for (int i=0;i<n;i++) {
      graph->GetPoint(i,x,y);
      graph->SetPoint(i,x*sf,y/sf);
      graph->SetPointEYlow(i,graph->GetErrorYlow(i)/sf);
      graph->SetPointEYhigh(i,graph->GetErrorYhigh(i)/sf);
      graph->SetPointEXlow(i,graph->GetErrorXlow(i)*sf);
      graph->SetPointEXhigh(i,graph->GetErrorXhigh(i)*sf);
    }
  }

//----------------------------------------------------------------------

//----------------------------------------------------------------------

  void divideByBinWidth(TGraphAsymmErrors* graph) {
    const int n=graph->GetN();
    double x(0),y(0);
    for (int i=0;i<n;i++) {
      graph->GetPoint(i,x,y);
      const double sf = 1./(graph->GetErrorXlow(i)+graph->GetErrorXhigh(i));
      graph->SetPoint(i,x,y*sf);
      graph->SetPointEYlow(i,graph->GetErrorYlow(i)*sf);
      graph->SetPointEYhigh(i,graph->GetErrorYhigh(i)*sf);
    }
    
  }

//----------------------------------------------------------------------

  void normalizeGraph(TGraphAsymmErrors* graph) {
    const int n=graph->GetN();
    double x(0),y(0);
    double integral(0.);
    for (int i=0;i<n;i++) {
      graph->GetPoint(i,x,y);
      integral+=y;
    }
    const double sf=1./integral;
    
    for (int i=0;i<n;i++) {
      graph->GetPoint(i,x,y);
      graph->SetPoint(i,x,y*sf);
      graph->SetPointEYlow(i,graph->GetErrorYlow(i)*sf);
      graph->SetPointEYhigh(i,graph->GetErrorYhigh(i)*sf);
    }
    
  }

//----------------------------------------------------------------------

  void divideGraphByHistogram(TGraphAsymmErrors* graph,const TH1* h) {
    const int n=graph->GetN();
    double x(0),y(0);
    for (int i=0;i<n;i++) {
      const double bincontent = h->GetBinContent(i+1);
      graph->SetPointEYlow(i,graph->GetErrorYlow(i)/bincontent);
      graph->SetPointEYhigh(i,graph->GetErrorYhigh(i)/bincontent);
      graph->GetPoint(i,x,y);
      graph->SetPoint(i,x,y/bincontent);
    } 
  }

//----------------------------------------------------------------------

  void divideHistogramByGraph(TH1& h,const TGraphAsymmErrors& graph) {
    const int n=graph.GetN();
    for (int i=0;i<n;i++) {
      const int ibin=i+1;
      const double y = graph.GetPointY(i);
      h.SetBinContent(ibin,h.GetBinContent(ibin)/y);
      h.SetBinError(ibin,h.GetBinError(ibin)/y);
    }
    
  }
  
//----------------------------------------------------------------------

  void setErrors(TGraphAsymmErrors& graph_unc,const TMatrixT<double>& cov) {
	  
    for (int ibin=0;ibin<graph_unc.GetN();++ibin){
      graph_unc.SetPointEYlow(ibin,sqrt(std::abs(cov[ibin][ibin])));
      graph_unc.SetPointEYhigh(ibin,sqrt(std::abs(cov[ibin][ibin])));
    }
  }

} // end functions namespace



