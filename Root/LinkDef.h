#ifdef __CINT__

#include "TTbarHelperFunctions/TTBarConfig.h"
#include "TTbarHelperFunctions/MultiDimensionalPlotsSettings.h"
#include "TTbarHelperFunctions/HistogramNDto1DConverter.h"
#include "TTbarHelperFunctions/Predictions.h"
#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"


#include "TTbarHelperFunctions/latexTablesFunctions.h"
#include "TTbarHelperFunctions/functions.h"
#include "TTbarHelperFunctions/histManipFunctions.h"
#include "TTbarHelperFunctions/plottingFunctions.h"
#include "TTbarHelperFunctions/namesAndTitles.h"




#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
//#pragma link C++ nestedclass;

#pragma link C++ namespace functions+;
#pragma link C++ namespace NamesAndTitles+;
#pragma link C++ namespace latexTablesFunctions+;

#pragma link C++ class TTBarConfig+;
#pragma link C++ enum LargeRJetType+;
#pragma link C++ class MultiDimensionalPlotsSettings+;
#pragma link C++ class HistogramNDto1DConverter+;
#pragma link C++ class Predictions+;
#pragma link C++ class TtbarDiffCrossSection_exception+;

#endif // __CINT__
