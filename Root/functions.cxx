// Peter Berta, 15.10.2015

#include "TTbarHelperFunctions/functions.h"
#include "TTbarHelperFunctions/histManipFunctions.h"

#include <algorithm>
#include "TLine.h"
#include "TPaveText.h"
#include "TSystem.h"

namespace functions{

  const int g_iRegionA=0;
  const int g_iRegionB=1;
  const int g_iRegionC=2;
  const int g_iRegionD=3;
  const int g_iRegionE=4;
  const int g_iRegionF=5;
  const int g_iRegionG=6;
  const int g_iRegionH=7;
  const int g_iRegionI=8;
  const int g_iRegionJ=9;
  const int g_iRegionK=10;
  const int g_iRegionL=11;
  const int g_iRegionM=12;
  const int g_iRegionN=13;
  const int g_iRegionO=14;
  const int g_iRegionP=15;


//----------------------------------------------------------------------

  void writeObject(const TObject* obj, const TString& objName, const TString& fileName, const TString& fileOption) {
    auto f = std::make_unique<TFile>(fileName,fileOption);
    if(!f.get()) {
      throw TtbarDiffCrossSection_exception("Error: Unable to create a TFile with name: " + fileName);
    }
    obj->Write(objName);
    f->Close();
  }

//----------------------------------------------------------------------

  std::vector<TString> getVectorOfKeys(TFile* f, const TString& filter) {
    std::vector<TString> output;
    TIter nextkey(f->GetListOfKeys());
    TKey* key=0;
    while( (key = (TKey*)nextkey())) {
      const TString& name = key->GetName();
      if(name.Contains(filter)) output.push_back(name);
    }
    return output;
  }


//----------------------------------------------------------------------

  TString printNumber(const double value,double uncert) {
    
    int nValidDigits = 1;
    if(uncert < 0.00000001){
      nValidDigits=8;
    }
    else {
      while(uncert<value) {
        uncert*=10;
        nValidDigits++;
      }
    }
    double x=std::abs(value);
    int n=0;
    while (x>1) {
      x/=10.;
      n++;
    } 
    while (x<1) {
      x*=10.;
      n--;
    }
    
    TString prec = (TString)"%1." + Form("%i",nValidDigits-1) + "f";
    
    TString res = Form(prec.Data(),x);
    if(n!=0) res += (TString)"\\cdot 10^{" + Form("%i",n) + "}";
    
    return res;
  }

//----------------------------------------------------------------------

  double deltaPhi(double phi1, double phi2){
    double delta=phi1-phi2;
    if (delta<0) delta=delta+TMath::Pi();
    if (delta<0) delta=delta+TMath::Pi();
    if (delta>TMath::Pi()) delta=delta-TMath::Pi();
    if (delta>TMath::Pi()) delta=delta-TMath::Pi();
    return delta;
  }

//----------------------------------------------------------------------

  double getLumi(const TEnv& configLumi,const TString& mcProduction) {
    double lumi(-1000.);
    if(mcProduction=="MC16a")lumi=configLumi.GetValue("lumi_data1516",0.)/1000.;
    else if (mcProduction=="MC16c" || mcProduction=="MC16d") lumi=configLumi.GetValue("lumi_data17",0.)/1000.;
    else if (mcProduction=="MC16e" || mcProduction=="MC16f") lumi=configLumi.GetValue("lumi_data18",0.)/1000.;
    else if (mcProduction=="All") lumi = configLumi.GetValue("lumi_dataAllYears",0.)/1000.;
    else cout << "Error: Unknown MC production" << endl;
    
    return lumi;
  }

//----------------------------------------------------------------------
  
  double getLumi(const TTBarConfig& config){
    TString mcProduction = config.mc_samples_production();
    return getLumi(config.getTEnv(),mcProduction);
  }

//----------------------------------------------------------------------


  int getDSID(const std::string& filename) {
    if(filename.find("data") != std::string::npos) return 0;
    if(filename.find("Data") != std::string::npos) return 0;
    if(filename.find("DATA") != std::string::npos) return 0;
    
    
    std::string mc = "MC16";
    size_t pos = filename.find(mc);
    if(pos==std::string::npos) return 0;
    std::string substr = filename.substr(pos+mc.length()+2);
    
    size_t pos2 = substr.find(".");
    substr= substr.substr(0,pos2);
    return std::stoi(substr);
  }

//----------------------------------------------------------------------

  vector<TString> MakeVector(string text){
    vector<TString> dividedText;
    string::size_type end;
    if (text.size() > 0) {
      do {
        end = text.find(";");
        dividedText.push_back(text.substr(0,end));
        if (end != string::npos) text = text.substr(end+1);
      } while (end != string::npos);
    }
    return dividedText;
  }

//----------------------------------------------------------------------

  vector<int> MakeVectorInt(string text){
    vector<int> dividedText;
    string::size_type end;
    if (text.size() > 0) {
      do {
        end = text.find(";");
        dividedText.push_back(atoi(text.substr(0,end).c_str()));
        if (end != string::npos) text = text.substr(end+1);
      } while (end != string::npos);
    }
    return dividedText;
  }

//----------------------------------------------------------------------

  vector<unsigned int> MakeVectorUnsignedInt(string text){
    vector<unsigned int> dividedText;
    string::size_type end;
    if (text.size() > 0) {
      do {
        end = text.find(";");
        dividedText.push_back(stoul(text.substr(0,end) ) );
        if (end != string::npos) text = text.substr(end+1);
      } while (end != string::npos);
    }
    return dividedText;
  }
  
//----------------------------------------------------------------------

  vector<Float_t> MakeVectorFloat(string text){
    vector<Float_t> dividedText;
    string::size_type end;
    if (text.size() > 0) {
      do {
        end = text.find(";");
        dividedText.push_back(atof(text.substr(0,end).c_str()));
        if (end != string::npos) text = text.substr(end+1);
      } while (end != string::npos);
    }
    return dividedText;
  }
  
//----------------------------------------------------------------------

  vector<Double_t> MakeVectorDouble(string text){
    vector<Double_t> dividedText;
    string::size_type end;
    if (text.size() > 0) {
      do {
        end = text.find(";");
        dividedText.push_back(atof(text.substr(0,end).c_str()));
        if (end != string::npos) text = text.substr(end+1);
      } while (end != string::npos);
    }
    return dividedText;
  }
  
//----------------------------------------------------------------------

  vector<TString> MakeVectorTString(string text){
    vector<TString> dividedText;
    string::size_type end;
    if (text.size() > 0) {
      do {
        end = text.find(";");
        dividedText.push_back(text.substr(0,end).c_str() );
        if (end != string::npos) text = text.substr(end+1);
      } while (end != string::npos);
    }
    return dividedText;
  }

//----------------------------------------------------------------------

  vector<TString> GetSubdirectories(int debug){
    vector<TString> subdirs;
    TDirectory *current_dir = gDirectory;
    TIter nextkey(current_dir->GetListOfKeys());
    TKey *key, *oldkey=0;
    while (( key = (TKey*)nextkey())){
      if (oldkey && !strcmp(oldkey->GetName(),key->GetName())) continue;
      TObject *obj=key->ReadObj();
      if (debug > 1) cout <<  "found object: " << obj->GetName() << endl;
      if (obj->IsA()->InheritsFrom(TDirectory::Class())){
        if (debug > 1) cout << "it is a subdirectory"  << endl;
        subdirs.push_back(obj->GetName());
      }
    }
    return(subdirs);
  }

//----------------------------------------------------------------------
  
  void GetListOfTH1DNames(vector<TString>& names){
    TIter nextkey( gDirectory->GetListOfKeys()); 
    TKey* key=0;
    while( (key = (TKey*)nextkey())){
      
      TObject *obj=key->ReadObj();
      if (obj->IsA()->InheritsFrom(TH1D::Class())){
          // cout << key->GetName() << endl;
           names.push_back(key->GetName());
      }
    }
  }

//----------------------------------------------------------------------

  vector<TString> GetTTrees(int debug){
    vector<TString> ttrees;
    TDirectory *current_dir = gDirectory;
    TIter nextkey(current_dir->GetListOfKeys());
    TKey *key, *oldkey=0;
    while (( key = (TKey*)nextkey())){
      if (oldkey && !strcmp(oldkey->GetName(),key->GetName())) continue;
      TObject *obj=key->ReadObj();
      if (debug > 1) cout <<  "found object: " << obj->GetName() << endl;
      if (obj->IsA()->InheritsFrom(TTree::Class())){
        if (debug > 1) cout << "it is a TTree"  << endl;
        ttrees.push_back(obj->GetName());
      }
      delete obj;
    }
    return(ttrees);
  }

//----------------------------------------------------------------------

  Double_t invert(Double_t *x, Double_t /**/){
    if (fabs(x[1])<1e-8) return(1e8);
    else return(1/x[1]);
  }

//----------------------------------------------------------------------

  float getFloor(float number, int nDigits){
    if (nDigits>=0){
      if (number < 1e-6) return(0.);
      int number_of_digits_before_dot=0;
      if (fabs(number)>=1){
        float result=number;
        while (fabs(result)>=1){
          result=result / 10.;
          number_of_digits_before_dot++;
        }
      }
      else{
        float result=number*10;
        while (fabs(result)<1){
          result=result * 10;
          number_of_digits_before_dot--;
        }
      }
    int rounding=nDigits-number_of_digits_before_dot;
    number=floor(number*pow(10.,rounding))/pow(10.,rounding);
    }
    return(number);
  }

//----------------------------------------------------------------------

  void PrintPtEtaPhiE(TString info, TLorentzVector vec){
    cout << left << setw(20) << info << setw(3) << "pT: ";
    cout << right << setw(6) << fixed << setprecision(1) << vec.Pt();
    cout  << setprecision(3) <<  "     eta: " << setw(7) << vec.Eta();
    cout << "     phi: " << setw(7) << vec.Phi();
    cout << "     E: " << setprecision(1) << setw(6) << vec.E();
    cout << "     M: " << setprecision(3) << setw(7) << vec.M() << endl;
    cout << setprecision(7);
  }

//----------------------------------------------------------------------

  void CopyArray(Double_t* in,Double_t* out, int size){
    for (int i=0;i<size;++i){
      out[i]=in[i];
    }
  }
  
  float AngularDistance(TLorentzVector jet1, TLorentzVector jet2){
    float deltaPhi=fabs(jet1.Phi()-jet2.Phi());
    if (deltaPhi>TMath::Pi()) deltaPhi=2*TMath::Pi()-deltaPhi;
    float deltaRap=jet1.Rapidity()-jet2.Rapidity();
    float R=sqrt(pow(deltaPhi,2)+pow(deltaRap,2));
    return (R);
  }

//----------------------------------------------------------------------
  
  tuple<TString,TString> setPrecision(TString variableName){
    
    TString precision="4.f";
    TString precision2="4.f";
    
    if(variableName.Contains("ttbar_y_vs_ttbar_mass") || variableName.Contains("t1_pt_vs_ttbar_mass") ) precision="4.2f";
    if(variableName.Contains("ttbar_y_vs_ttbar_mass") || variableName.Contains("ttbar_y_vs_t1_pt")) precision2="4.1f";
    
    return {precision,precision2};
  }
  
// ---------------------------------------------------------------------

  bool isNominalTree(const TString& treename){
    if( treename=="nominal") return true;
    return false;
  }
  
//---------------------------------------------------------------------------------
  
  bool isSignalModelingSys(const TString& sysname){
    if(	sysname=="ME" || 
        sysname=="PS" || 
        sysname.Contains("Sherpa")  || 
        sysname.BeginsWith("ISR_") || 
        sysname.BeginsWith("FSR_") ||
        sysname.BeginsWith("Var3c") ||  
        sysname.BeginsWith("ALPHAS_FSR") ||
        sysname.BeginsWith("MMHT2014") ||
        sysname.BeginsWith("CT14") ||
        sysname.BeginsWith("NNPDF_") ||
	sysname=="PhH704" ||
	sysname=="PhH713" ||
	sysname=="PhPy8MECoff" ||
	sysname=="hdamp" ||
	sysname=="nnlo_rew"
        ) return true;
    return false;
  }
  
//---------------------------------------------------------------------------------
  
  bool isPDFSys(const TString& sysname) {
    if(sysname.BeginsWith("PDF4LHC15") ) return true;
    return false;
  }

//---------------------------------------------------------------------------------


  bool isLargeRJERSys(const TString& treename) {
    if(treename.Contains("JET_JER") && treename.Contains("_R10_")) return true;
    return false;
  }

//---------------------------------------------------------------------------------


  bool isDetectorSys(const TString& treename) {
    
    if(isPDFSys(treename) || isReweightedTree(treename) || isSignalModelingSys(treename) || isNominalTree(treename)) {
      return false;
    }
    
    return true;
  }

//---------------------------------------------------------------------------------
  
  bool isReweightedTree(const TString& treename) {
    if(treename.BeginsWith("reweighted") ) return true;
    return false;
  }
  
//---------------------------------------------------------------------------------
  
  vector<TString> getReweightedTrees(const vector<TString>& all){
    vector<TString> res;
    for(const TString& treeName : all){
      if(isReweightedTree(treeName)) res.push_back(treeName);
    }
    return res;
  }
     
//---------------------------------------------------------------------------------
  
  // Get Signal modeling systematic trees
  vector<TString> getSignalModelingSysNames(vector<TString>& all) {
    vector<TString> res;
    for(const TString& sysName : all){
      if( isSignalModelingSys(sysName)) {
        res.push_back(sysName);
      }
    }
    return res;
  }

//---------------------------------------------------------------------------------
  // Get PDF systematic trees
  vector<TString> getPDFSysNames(vector<TString>& all) {
    vector<TString> res;
    for(const TString& sysName : all){
      if( isPDFSys(sysName)) {
        res.push_back(sysName);
      }
    }
    return res;
  }

//---------------------------------------------------------------------------------
  
  bool isPairedSys(vector<TString>& all,const TString& left,const TString& right,int i,int& j){
    const int size = all.size();
    if(i>size-1){
      std::cout << "Error in functions::isPairedSys(): requested index out of range." << std::endl;
      exit(-1); 
    }
    
    const int leftLength = left.Length();
    const int rightLength = right.Length();
    const TString& sys = all[i];
    TString sys2 = sys;
    if(sys.EndsWith(left) ) {
      sys2.Replace(sys2.Length()-leftLength,leftLength,right);
    }
    else if (sys.EndsWith(right)){
      sys2.Replace(sys2.Length()-rightLength,rightLength,left);
    }	
    else {
      return false;
    }
    
    vector<TString>::iterator it = std::find(all.begin(),all.end(),sys2);
    if(it==all.end()) {
      return false;
    }
    
    j=std::distance(all.begin(),it); // j is an index of oposite systematic name
    return true;
  }
  
//---------------------------------------------------------------------------------
  
  vector<TString> getSingleSysNames(vector<TString>& all, const TString& left,const TString& right) // Detector systematic sources only with 1 shift available (up or down) 
  {
    vector<TString> res;
    const int size=all.size();
    int j=0; 
    for(int i=0;i<size;i++){
      const TString& sys = all[i];
      if( isReweightedTree(sys) || 
          isSignalModelingSys(sys) ||
          isPDFSys(sys) ||
          isNominalTree(sys)
          ) continue;
      if(!isPairedSys(all,left,right,i,j)){
        res.push_back(sys);
      }
    }
    return res;
  }
  
//---------------------------------------------------------------------------------

  
  vector<pair<TString,TString> > getPairedSysNames(vector<TString>& all, const TString& left,const TString& right) // Detector systematic sources with up and down shifts
  {
    vector<pair<TString,TString> > res;
    const int size=all.size();
    int j=0; 
    for(int i=0;i<size;i++){
      const TString& sys = all[i];
      if( isReweightedTree(sys) || 
          isSignalModelingSys(sys) ||
          isPDFSys(sys) ||
          isNominalTree(sys)
          ) continue;
      if(isPairedSys(all,left,right,i,j)){
        if(j<i) continue; // To avoid duplicate pairs
        
        if(all[i].EndsWith(left)) {
          res.push_back({all[i],all[j]});
        }
        else {
          res.push_back({all[j],all[i]});
        }
      }
    }
    
    return res;
  }
  
//---------------------------------------------------------------------------------
  
  void classifySysNames(vector<TString>& all,vector<pair<TString,TString> >& pairs, vector<TString>& single, vector<TString>& pdf, const TString& left,const TString& right){
    
    // This one is for interaction with root files from produce_hists jobs
    
    int size=all.size(); 
    int i=0,j;
    for(i=0;i<size;i++){

      if( isReweightedTree(all[i]) ||
          isSignalModelingSys(all[i]) ||
          isNominalTree(all[i])
          ) continue;
    
      if( isPDFSys(all[i])){	      
        pdf.push_back(all[i]);
        continue;
      }
      
      if(isPairedSys(all,left,right,i,j)){
        if(j<i) continue; // To avoid duplicate pairs
        
        if(all[i].EndsWith(left)) {
          pairs.push_back({all[i],all[j]});
        }
        else {
          pairs.push_back({all[j],all[i]});
        }
        
      }
      else { // Not paired sys
        single.push_back(all[i]);
      }
      
    } // End loop over sys
  }
  
//---------------------------------------------------------------------------------
  
  void classifySysNames(vector<TString>& all,vector<pair<TString,TString> >& pairs, vector<TString>& single, vector<TString>& pdf, vector<TString>& signalModeling ,const TString& left,const TString& right){
    // This one is for interaction with systematics histos
    const int size=all.size(); 
    int i=0,j;
        
    for(i=0;i<size;i++){

      if( isReweightedTree(all[i]) ||
          isNominalTree(all[i])
          ) continue;
      if( isPDFSys(all[i])){	      
        pdf.push_back(all[i]);
        continue;
      }
      if( isSignalModelingSys(all[i])) {
        signalModeling.push_back(all[i]);
        continue;
      }
      
      if(isPairedSys(all,left,right,i,j)){
        if(j<i) continue; // To avoid duplicate pairs
        
        if(all[i].EndsWith(left)) {
          pairs.push_back({all[i],all[j]});
        }
        else {
          pairs.push_back({all[j],all[i]});
        }
      }
      else { // Not paired sys
        single.push_back(all[i]);
      }
      
    } // End loop over sys
  }

//---------------------------------------------------------------------------------
        
        
  void splitDouble(double x,double &z, int& k){
  
    double y=1.;
    if(x<y){
      while(x<y){
        y/=10;
        k--;
      }
      z=x/y;
    }
    else{
      while(x>y){
        y*=10;
        k++;
      }
      k--;
      y/=10;
      z=x/y;
    }
  }
  
//----------------------------------------------------------------------  
  
  void Calculate_ABCD16_estimate(const vector<TH1D*>& data,const vector<TH1D*>& MC,TH1D* estimate,bool calculate_errors){
    const int nbins=data[0]->GetNbinsX();
    
    double nominator;
    double denominator1;
    //double denominator2;
    static vector<double> vec(15);
    for(int ibin=1;ibin<=nbins;ibin++){
      
      
      vec[g_iRegionA]=data[g_iRegionA]->GetBinContent(ibin) - MC[g_iRegionA]->GetBinContent(ibin);
      vec[g_iRegionB]=data[g_iRegionB]->GetBinContent(ibin) - MC[g_iRegionB]->GetBinContent(ibin);
      vec[g_iRegionC]=data[g_iRegionC]->GetBinContent(ibin) - MC[g_iRegionC]->GetBinContent(ibin);
      vec[g_iRegionD]=data[g_iRegionD]->GetBinContent(ibin) - MC[g_iRegionD]->GetBinContent(ibin);
      vec[g_iRegionE]=data[g_iRegionE]->GetBinContent(ibin) - MC[g_iRegionE]->GetBinContent(ibin);
      vec[g_iRegionF]=data[g_iRegionF]->GetBinContent(ibin) - MC[g_iRegionF]->GetBinContent(ibin);
      vec[g_iRegionG]=data[g_iRegionG]->GetBinContent(ibin) - MC[g_iRegionG]->GetBinContent(ibin);
      vec[g_iRegionH]=data[g_iRegionH]->GetBinContent(ibin) - MC[g_iRegionH]->GetBinContent(ibin);
      vec[g_iRegionI]=data[g_iRegionI]->GetBinContent(ibin) - MC[g_iRegionI]->GetBinContent(ibin);
      vec[g_iRegionJ]=data[g_iRegionJ]->GetBinContent(ibin) - MC[g_iRegionJ]->GetBinContent(ibin);
      vec[g_iRegionO]=data[g_iRegionO]->GetBinContent(ibin) - MC[g_iRegionO]->GetBinContent(ibin);
      
      // (S2 + S3)/2
      //nominator=vec[g_iRegionJ]*vec[g_iRegionO]*vec[g_iRegionF]*vec[g_iRegionH];
      //denominator1=vec[g_iRegionE]*vec[g_iRegionD]*vec[g_iRegionI];
      //denominator2=vec[g_iRegionB]*vec[g_iRegionC]*vec[g_iRegionG];
      //if(denominator1>0.){
              //if(denominator2>0.) estimate->SetBinContent(ibin,nominator*(1./denominator1 + 1./denominator2)/2);
              //else estimate->SetBinContent(ibin,nominator/denominator1);
      //}
      //else {
              //if(denominator2>0.) estimate->SetBinContent(ibin,nominator/denominator2);
              //else estimate->SetBinContent(ibin,0.);
      //}
      
      // S9
      nominator=vec[g_iRegionJ]*vec[g_iRegionO]*vec[g_iRegionH]*vec[g_iRegionF]*vec[g_iRegionD]*vec[g_iRegionG]*pow(vec[g_iRegionA],3);
      denominator1=pow(vec[g_iRegionB]*vec[g_iRegionE]*vec[g_iRegionC]*vec[g_iRegionI],2);
      
      estimate->SetBinContent(ibin,denominator1 > 0 ? nominator/denominator1 : 0. );
      estimate->SetBinError(ibin,0.);
    }
    if(!calculate_errors)return;
    const int npseudoexperiments=10000;
    vector<double> means(nbins),meansquares(nbins);
    
    for(int i=0;i<npseudoexperiments;i++){
      
      for(int ibin=1;ibin<=nbins;ibin++){
        vec[g_iRegionA]=(data[g_iRegionA]->GetBinContent(ibin) > 100) ? gRandom->Gaus(data[g_iRegionA]->GetBinContent(ibin),sqrt(data[g_iRegionA]->GetBinContent(ibin))) : gRandom->Poisson(data[g_iRegionA]->GetBinContent(ibin));
        vec[g_iRegionB]=(data[g_iRegionB]->GetBinContent(ibin) > 100) ? gRandom->Gaus(data[g_iRegionB]->GetBinContent(ibin),sqrt(data[g_iRegionB]->GetBinContent(ibin))) : gRandom->Poisson(data[g_iRegionB]->GetBinContent(ibin));
        vec[g_iRegionC]=(data[g_iRegionC]->GetBinContent(ibin) > 100) ? gRandom->Gaus(data[g_iRegionC]->GetBinContent(ibin),sqrt(data[g_iRegionC]->GetBinContent(ibin))) : gRandom->Poisson(data[g_iRegionC]->GetBinContent(ibin));
        vec[g_iRegionD]=(data[g_iRegionD]->GetBinContent(ibin) > 100) ? gRandom->Gaus(data[g_iRegionD]->GetBinContent(ibin),sqrt(data[g_iRegionD]->GetBinContent(ibin))) : gRandom->Poisson(data[g_iRegionD]->GetBinContent(ibin));
        vec[g_iRegionE]=(data[g_iRegionE]->GetBinContent(ibin) > 100) ? gRandom->Gaus(data[g_iRegionE]->GetBinContent(ibin),sqrt(data[g_iRegionE]->GetBinContent(ibin))) : gRandom->Poisson(data[g_iRegionE]->GetBinContent(ibin));
        vec[g_iRegionF]=(data[g_iRegionF]->GetBinContent(ibin) > 100) ? gRandom->Gaus(data[g_iRegionF]->GetBinContent(ibin),sqrt(data[g_iRegionF]->GetBinContent(ibin))) : gRandom->Poisson(data[g_iRegionF]->GetBinContent(ibin));
        vec[g_iRegionG]=(data[g_iRegionG]->GetBinContent(ibin) > 100) ? gRandom->Gaus(data[g_iRegionG]->GetBinContent(ibin),sqrt(data[g_iRegionG]->GetBinContent(ibin))) : gRandom->Poisson(data[g_iRegionG]->GetBinContent(ibin));
        vec[g_iRegionH]=(data[g_iRegionH]->GetBinContent(ibin) > 100) ? gRandom->Gaus(data[g_iRegionH]->GetBinContent(ibin),sqrt(data[g_iRegionH]->GetBinContent(ibin))) : gRandom->Poisson(data[g_iRegionH]->GetBinContent(ibin));
        vec[g_iRegionI]=(data[g_iRegionI]->GetBinContent(ibin) > 100) ? gRandom->Gaus(data[g_iRegionI]->GetBinContent(ibin),sqrt(data[g_iRegionI]->GetBinContent(ibin))) : gRandom->Poisson(data[g_iRegionI]->GetBinContent(ibin));
        vec[g_iRegionJ]=(data[g_iRegionJ]->GetBinContent(ibin) > 100) ? gRandom->Gaus(data[g_iRegionJ]->GetBinContent(ibin),sqrt(data[g_iRegionJ]->GetBinContent(ibin))) : gRandom->Poisson(data[g_iRegionJ]->GetBinContent(ibin));
        vec[g_iRegionO]=(data[g_iRegionO]->GetBinContent(ibin) > 100) ? gRandom->Gaus(data[g_iRegionO]->GetBinContent(ibin),sqrt(data[g_iRegionO]->GetBinContent(ibin))) : gRandom->Poisson(data[g_iRegionO]->GetBinContent(ibin));
        
        vec[g_iRegionA]-= gRandom->Gaus(MC[g_iRegionA]->GetBinContent(ibin),MC[g_iRegionA]->GetBinError(ibin));
        vec[g_iRegionB]-= gRandom->Gaus(MC[g_iRegionB]->GetBinContent(ibin),MC[g_iRegionB]->GetBinError(ibin));
        vec[g_iRegionC]-= gRandom->Gaus(MC[g_iRegionC]->GetBinContent(ibin),MC[g_iRegionC]->GetBinError(ibin));
        vec[g_iRegionD]-= gRandom->Gaus(MC[g_iRegionD]->GetBinContent(ibin),MC[g_iRegionD]->GetBinError(ibin));
        vec[g_iRegionE]-= gRandom->Gaus(MC[g_iRegionE]->GetBinContent(ibin),MC[g_iRegionE]->GetBinError(ibin));
        vec[g_iRegionF]-= gRandom->Gaus(MC[g_iRegionF]->GetBinContent(ibin),MC[g_iRegionF]->GetBinError(ibin));
        vec[g_iRegionG]-= gRandom->Gaus(MC[g_iRegionG]->GetBinContent(ibin),MC[g_iRegionG]->GetBinError(ibin));
        vec[g_iRegionH]-= gRandom->Gaus(MC[g_iRegionH]->GetBinContent(ibin),MC[g_iRegionH]->GetBinError(ibin));
        vec[g_iRegionI]-= gRandom->Gaus(MC[g_iRegionI]->GetBinContent(ibin),MC[g_iRegionI]->GetBinError(ibin));
        vec[g_iRegionJ]-= gRandom->Gaus(MC[g_iRegionJ]->GetBinContent(ibin),MC[g_iRegionJ]->GetBinError(ibin));
        vec[g_iRegionO]-= gRandom->Gaus(MC[g_iRegionO]->GetBinContent(ibin),MC[g_iRegionO]->GetBinError(ibin));
        
        
        nominator=vec[g_iRegionJ]*vec[g_iRegionO]*vec[g_iRegionH]*vec[g_iRegionF]*vec[g_iRegionD]*vec[g_iRegionG]*pow(vec[g_iRegionA],3);
        denominator1=pow(vec[g_iRegionB]*vec[g_iRegionE]*vec[g_iRegionC]*vec[g_iRegionI],2);
        double bincontent=denominator1 > 0 ? nominator/denominator1 : 0.;
        
        means[ibin-1]+=bincontent;
        meansquares[ibin-1]+=(bincontent*bincontent);
    
      }
    }
    for(int ibin=1;ibin<=nbins;ibin++){
      means[ibin-1]/=npseudoexperiments;
      meansquares[ibin-1]/=npseudoexperiments;
      estimate->SetBinError(ibin,sqrt(meansquares[ibin-1] - means[ibin-1]*means[ibin-1]));
    }
  }
  
//----------------------------------------------------------------------

  void calculateSystematicUncertainties(TGraphAsymmErrors* unc,TFile *f,TString variable_name){
  
    std::unique_ptr<TH1D> nominal {(TH1D*)f->Get("nominal/" + variable_name +"_pseudodata")};
    
    if(!unc) unc = new TGraphAsymmErrors(nominal.get());
    const int nbins=nominal->GetNbinsX();
    
    for(int i=0;i<nbins;i++){
      unc->SetPointEYhigh(i, 0.);
      unc->SetPointEYlow (i, 0.);
    }
    
    
    vector<TString> allSys;
    vector<pair<TString,TString> > pairs;
    vector<TString> single,pdf,signalModeling;
    
    
    vector<double> sigma_up(nbins),sigma_down(nbins);
    
    functions::GetListOfNames(allSys,f,TDirectory::Class());// Making list of systematic names
    functions::classifySysNames(allSys,pairs,single,signalModeling, pdf); // Classifying systematics to categories
    
    const int nsys_paired=pairs.size();
    const int nsys_single=single.size();
    
    for(int isys=0;isys<nsys_paired;isys++){
      getOneSysUncertainty(nominal.get(),f,pairs[isys],variable_name,sigma_up,sigma_down);
      for(int i=0;i<nbins;i++){
        unc->SetPointEYhigh(i, sqrt( pow(unc->GetErrorYhigh(i),2) + pow(  sigma_up[i],2) ) );
        unc->SetPointEYlow (i, sqrt( pow( unc->GetErrorYlow(i),2) + pow(sigma_down[i],2) ) );
      }
      
      
    }
    for(int isys=0;isys<nsys_single;isys++){
      
      getOneSysUncertainty(nominal.get(),f,single[isys],variable_name,sigma_up,sigma_down);
      for(int i=0;i<nbins;i++){
        unc->SetPointEYhigh(i, sqrt( pow(unc->GetErrorYhigh(i),2) + pow(  sigma_up[i],2) ) );
        unc->SetPointEYlow (i, sqrt( pow( unc->GetErrorYlow(i),2) + pow(sigma_down[i],2) ) );
      }
    }
    
  }
  
//----------------------------------------------------------------------
  
  void getOneSysUncertainty(TH1D* nominal,TFile *f,const pair<TString,TString>& sysName,const TString& variable_name,vector<double>& sigma_up,vector<double>& sigma_down){
    const int nbins=nominal->GetNbinsX();
    
    std::unique_ptr<TH1D> h_shift_up {(TH1D*)f->Get(sysName.first +"/" + variable_name +"_pseudodata")};
    std::unique_ptr<TH1D> h_shift_down {(TH1D*)f->Get(sysName.second +"/" + variable_name +"_pseudodata")};
    
    double diff1,diff2;
    
    for(int i=0;i<nbins;i++){
      int ibin=i+1;
      
      double bincontent=nominal->GetBinContent(ibin);
      
      diff1=0.;
      diff2=0.;
      
      if(bincontent>0.){
        diff1=(h_shift_up->GetBinContent(ibin) - bincontent)/bincontent;
        diff2=(h_shift_down->GetBinContent(ibin) - bincontent)/bincontent;
      }
      
      sigma_up[i]=0.;
      sigma_down[i]=0.;
      
      if (diff1>diff2 && diff1>0) sigma_up[i]=diff1;
      if (diff2>diff1 && diff2>0) sigma_up[i]=diff2; 
      if (diff1<diff2 && diff1<0) sigma_down[i]=diff1;
      if (diff2<diff1 && diff2<0) sigma_down[i]=diff2; 
      
    }
  }

//----------------------------------------------------------------------

  void getOneSysUncertainty(TH1D* nominal,TFile *f,const TString& sysName,const TString& variable_name,vector<double>& sigma_up,vector<double>& sigma_down){
    
    const int nbins=nominal->GetNbinsX();
    std::unique_ptr<TH1D> h_shift {(TH1D*)f->Get(sysName +"/" + variable_name +"_pseudodata")};
    double diff;
    for(int i=0;i<nbins;i++){
      int ibin=i+1;
      double bincontent=nominal->GetBinContent(ibin);
      diff=0.;
      
      if(bincontent>0.) diff = fabs( (h_shift->GetBinContent(ibin) - bincontent)/bincontent );
      sigma_up[i]=diff;
      sigma_down[i]=diff;
      
    }

  }
  
//----------------------------------------------------------------------

  double calculateFSRsys(const double up,const double down){
    double res=0.;
    if( (up*down)<0. ) res = 0.5*(up - down); // Shifts has opposite sign
    else if ( (up + down) > 0.) res = std::max(up,down); // Shifts are positive
    else res = std::min(up,down); // Shifts are negative
    return res;
  }

//----------------------------------------------------------------------
  
  map<TString, TH1D*> GetTH1Ds(int debug){
    map<TString, TH1D*> objects;
    TDirectory *current_dir = gDirectory;
    TIter nextkey(current_dir->GetListOfKeys());
    TKey *key, *oldkey=0;
    while (( key = (TKey*)nextkey())){
      if (oldkey && !strcmp(oldkey->GetName(),key->GetName())) continue;
      TObject *obj=key->ReadObj();
      if (debug > 1) cout <<  "found object: " << obj->GetName() << endl;
      if (obj->IsA()->InheritsFrom(TH1D::Class())){
        if (debug > 1) cout << "it is a TH1D object"  << endl;
        objects[obj->GetName()]=dynamic_cast<TH1D*>(current_dir->Get(obj->GetName()));
        objects[obj->GetName()]->SetTitle("");
        //objects[obj->GetName()]->Sumw2();
      }
      delete obj;
    }
    return(objects);
  }
  
//----------------------------------------------------------------------
  
  map<TString, TH2F*> GetTH2Fs(int debug){
    map<TString, TH2F*> objects;
    TDirectory *current_dir = gDirectory;
    TIter nextkey(current_dir->GetListOfKeys());
    TKey *key, *oldkey=0;
    while (( key = (TKey*)nextkey())){
      if (oldkey && !strcmp(oldkey->GetName(),key->GetName())) continue;
      TObject *obj=key->ReadObj();
      if (debug > 1) cout <<  "found object: " << obj->GetName() << endl;
      if (obj->IsA()->InheritsFrom(TH2F::Class())){
        if (debug > 1) cout << "it is a TH1F object"  << endl;
        objects[obj->GetName()]=dynamic_cast<TH2F*>(current_dir->Get(obj->GetName()));
        objects[obj->GetName()]->SetTitle("");
        //objects[obj->GetName()]->Sumw2();
      }
      delete obj;
    }
    return(objects);
  }

//----------------------------------------------------------------------
  
  map<TString, TH2D*> GetTH2Ds(int debug){
    map<TString, TH2D*> objects;
    TDirectory *current_dir = gDirectory;
    TIter nextkey(current_dir->GetListOfKeys());
    TKey *key, *oldkey=0;
    while (( key = (TKey*)nextkey())){
      if (oldkey && !strcmp(oldkey->GetName(),key->GetName())) continue;
      TObject *obj=key->ReadObj();
      if (debug > 1) cout <<  "found object: " << obj->GetName() << endl;
      if (obj->IsA()->InheritsFrom(TH2D::Class())){
        if (debug > 1) cout << "it is a TH1F object"  << endl;
        objects[obj->GetName()]=dynamic_cast<TH2D*>(current_dir->Get(obj->GetName()));
        objects[obj->GetName()]->SetTitle("");
        //objects[obj->GetName()]->Sumw2();
      }
      delete obj;
    }
    return(objects);
  }

//----------------------------------------------------------------------

  map<TString, TProfile*> GetTProfiles(int debug){
    map<TString, TProfile*> objects;
    TDirectory *current_dir = gDirectory;
    TIter nextkey(current_dir->GetListOfKeys());
    TKey *key, *oldkey=0;
    while (( key = (TKey*)nextkey())){
      if (oldkey && !strcmp(oldkey->GetName(),key->GetName())) continue;
      TObject *obj=key->ReadObj();
      if (debug > 1) cout <<  "found object: " << obj->GetName() << endl;
      if (obj->IsA()->InheritsFrom(TProfile::Class())){
        if (debug > 1) cout << "it is a TH1F object"  << endl;
        objects[obj->GetName()]=dynamic_cast<TProfile*>(current_dir->Get(obj->GetName()));
        objects[obj->GetName()]->SetTitle("");
        //objects[obj->GetName()]->Sumw2();
      }
      delete obj;
    }
    return(objects);
  }

//----------------------------------------------------------------------

TMatrixT<double>* makeCorrelationMatrix(const TMatrixT<double>& covarianceMatrix){
  const int ncols = covarianceMatrix.GetNcols();
  const int nrows = covarianceMatrix.GetNrows();
  
  TMatrixT<double>* cor = new TMatrixT<double>(ncols,nrows);
  
  for(int i=0;i<ncols;i++) for(int j=0;j<nrows;j++) {
    const double correlation = covarianceMatrix[i][j]/sqrt(covarianceMatrix[i][i]*covarianceMatrix[j][j]);
    (*cor)[i][j]= std::isfinite(correlation) ? correlation : 0.;
  }
  return cor;
}

//----------------------------------------------------------------------
  
  double chi2TestNormalized(TMatrixT<double>& x1,TMatrixT<double>& x2, TMatrixT<double>& cov,double& chi2,int& ndf,TString diffXsec) {
    TVectorT<double> eigenValues;
    TMatrixT<double> eigenVectors = cov.EigenVectors(eigenValues); 
    TMatrixT<double> eigenVectorsTransposed=eigenVectors;
    eigenVectorsTransposed.Transpose(eigenVectorsTransposed);
    
    TMatrixT<double> covTransformed = eigenVectorsTransposed;
    covTransformed*=cov;
    covTransformed*=eigenVectors;
    //cout << "Printing transformed matrix:" <<endl;
    //covTransformed.Print();
    
    //eigenValues.Print();
    
    TMatrixT<double> x1Transformed=x1;
    TMatrixT<double> x2Transformed=x2;
    
    x1Transformed.Transpose(x1Transformed);
    x2Transformed.Transpose(x2Transformed);
    
    x1Transformed*=eigenVectors;
    x2Transformed*=eigenVectors;
    
    chi2=0;
    ndf=cov.GetNcols()-1;
    if(diffXsec=="absolute") ndf++;
    
    for(int i=0;i<ndf;i++) {
      chi2+=(pow(x1Transformed[0][i]-x2Transformed[0][i],2)/covTransformed[i][i]);
    }
      //cout << "chi2/ndf = " << chi2 << "/" << ndf << endl;
      //cout << "p-value = " << TMath::Prob(chi2,ndf) << endl;
    return TMath::Prob(chi2,ndf);
  }
  
//----------------------------------------------------------------------
  
  TMatrixT<double> covarianceFromPrediction(TH1D* h, const TString& option, const int niter){
    const int nbins=h->GetNbinsX();
    TMatrixT<double> cov(nbins,nbins);
    if(option=="absolute"){
      for(int i=0;i<nbins;i++) cov[i][i]=pow(h->GetBinError(i+1),2);
    }
    else if(option=="relative"){
      TH1D* h2=(TH1D*)h->Clone();
      vector<double> sum(nbins);
      for(int i=0;i<niter;i++){
              
        smearHistogram(h,h2);
        h2->Scale(1./h2->Integral("width")); // Assume that histogram h was already divided by bin width!!
        for(int i=0;i<nbins;i++){
          sum[i]+=h2->GetBinContent(i+1);
          for(int j=0;j<nbins;j++){
            cov[i][j]+=h2->GetBinContent(i+1)*h2->GetBinContent(j+1);
          }
        }
      }
      for(int i=0;i<nbins;i++)sum[i]/=niter;
      for(int i=0;i<nbins;i++)for(int j=0;j<nbins;j++)cov[i][j]=cov[i][j]/niter -sum[i]*sum[j];
      
    }
    else cout << "Error in TMatrixT<double> covarianceFromPrediction(TH1D* h,TString diffXsec): Unknown option " << option<< endl;
    return cov;
  }
  
//----------------------------------------------------------------------

// Calculates covarince matrix for normalized spectrum from known absolute spectrum and covariance of absolute spectrum
  TMatrixT<double> covarianceForNormalizedPrediction(const TH1D& hAbs,const TMatrixT<double>& covAbs,const int niter,bool divideByBinWidth) {
    std::cout << "covarianceForNormalizedPrediction: Start" << std::endl;
    const int nbins = hAbs.GetXaxis()->GetNbins();
    
    TMatrixT<double> covNorm(nbins,nbins);
    TVectorT<double> mean(nbins);
    
    std::vector<double> bincontents(nbins);
    for(int i=0;i<nbins;i++) bincontents[i] = hAbs.GetBinContent(i);
    
    TVectorT<double> eigenValues(nbins);
    TMatrixT<double> eigenVectors = covAbs.EigenVectors(eigenValues);
    TVectorT<double> eigenValuesSqrt=eigenValues.Sqrt();
    
    TVectorT<double> shifts(nbins);
    TVectorT<double> shiftedValues(nbins);
    
    for(int iter=0;iter<niter;iter++) {
    
    
      for(int i=0;i<nbins;i++) {
	shifts[i]=gRandom->Gaus(0.,eigenValuesSqrt[i]);
	shiftedValues[i]=0.;
      }
      
      for(int j=0;j<nbins;j++) for(int i=0;i<nbins;i++) shiftedValues[j] += eigenVectors[j][i]*shifts[i];
      
      double shiftedIntegral(0.);
      
      for(int i=0;i<nbins;i++) {
	shiftedValues[i] += bincontents[i];
	shiftedIntegral += shiftedValues[i];
      }
      
      for(int i=0;i<nbins;i++) {
	
	shiftedValues[i] /= shiftedIntegral;
	if(divideByBinWidth) shiftedValues[i] /= hAbs.GetXaxis()->GetBinWidth(i+1);
      }
      
      for(int i=0;i<nbins;i++) {
	mean[i]+=shiftedValues[i];
	for(int j=0;j<nbins;j++) covNorm[i][j]+=(shiftedValues[i]*shiftedValues[j]);
	
      }
      
    }

    for(int i=0;i<nbins;i++) mean[i]/=niter;
    
    for(int i=0;i<nbins;i++) for(int j=0;j<nbins;j++) covNorm[i][j] = covNorm[i][j]/niter - mean[i]*mean[j];
    
    
    std::cout << "covarianceForNormalizedPrediction: End" << std::endl;
    
    return covNorm;
  }
  
//----------------------------------------------------------------------  
  
  double chi2Statistics(const TMatrixT<double>& difference, const TMatrixT<double>& inverseCov) {
    double chi2=0.;
    const int n=inverseCov.GetNcols();
    for(int i=0;i<n;i++) for(int j=0;j<n;j++) chi2+=(difference[0][i]*difference[0][j]*inverseCov[i][j]);
    return chi2;
  }

//----------------------------------------------------------------------
  
  void scaleCovariance(TMatrixT<double>& cov, const double sf) {
    const int nrows=cov.GetNrows();
    const int ncols=cov.GetNcols();
    const double sf2=sf*sf;
    for(int i=0;i<nrows;i++) for(int j=0;j<ncols;j++) {
      cov[i][j]*=sf2;
    }
    
  }

} // namespace functions




