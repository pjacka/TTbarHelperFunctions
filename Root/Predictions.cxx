#include "TTbarHelperFunctions/Predictions.h"
#include "TTbarHelperFunctions/HistogramNDto1DConverter.h"
#include "TTbarHelperFunctions/functions.h"
#include "TTbarHelperFunctions/stringFunctions.h"
#include "TTbarHelperFunctions/histManipFunctions.h"
#include "TTbarHelperFunctions/namesAndTitles.h"
#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"

#include "TGraphAsymmErrors.h"
#include "TH1.h"
#include "TLegend.h"

ClassImp(Predictions)

void Predictions::addHistogram(TH1D* h,bool clone) {
  if(clone) m_histos.push_back(std::unique_ptr<TH1D>{(TH1D*)h->Clone()});
  else m_histos.push_back(std::unique_ptr<TH1D>{h});
  
}

//----------------------------------------------------------------------

void Predictions::addGraph(TGraphAsymmErrors* gr,bool clone) {
  if(clone) m_graphs.push_back(std::unique_ptr<TGraphAsymmErrors>{(TGraphAsymmErrors*)gr->Clone()});
  else m_graphs.push_back(std::unique_ptr<TGraphAsymmErrors>{gr});
}

//----------------------------------------------------------------------

void Predictions::addUnc(TGraphAsymmErrors* gr,bool clone) {
  if(clone) m_unc.push_back(std::unique_ptr<TGraphAsymmErrors>{(TGraphAsymmErrors*)gr->Clone()});
  else m_unc.push_back(std::unique_ptr<TGraphAsymmErrors>{gr});
}

//----------------------------------------------------------------------

void Predictions::setData(TH1D* h,bool clone) {
  if(clone) m_data = std::unique_ptr<TH1D>{(TH1D*)h->Clone()};
  else m_data = std::unique_ptr<TH1D>{h};
}

//----------------------------------------------------------------------

void Predictions::setHistograms(const std::vector<TH1D*>& histos,bool clone) {
  for(auto h : histos) addHistogram(h,clone);
}

//----------------------------------------------------------------------

void Predictions::setGraphs(const std::vector<TGraphAsymmErrors*>& graphs,bool clone) {
  for(auto gr : graphs) addGraph(gr,clone);
}

//----------------------------------------------------------------------

void Predictions::setUnc(const std::vector<TGraphAsymmErrors*>& graphs,bool clone) {
  for(auto gr : graphs) addUnc(gr,clone);
}

//----------------------------------------------------------------------

Predictions Predictions::clone() const {
  Predictions copy;
  copy.setData(m_data.get(),true);
  for(const auto& h : m_histos) copy.addHistogram(h.get(),true);
  for(const auto& gr : m_graphs) copy.addGraph(gr.get(),true);
  for(const auto& gr : m_unc) copy.addUnc(gr.get(),true);
 
  return copy;
  
}

//----------------------------------------------------------------------

Predictions Predictions::makeRatios(const TH1D* h) const {
  Predictions ratios = this->clone();
  
  if(ratios.m_data!=0) ratios.m_data->Divide(h);
  for(const auto& hist : ratios.m_histos) hist->Divide(h);
  for(const auto& gr : ratios.m_graphs) functions::divideGraphByHistogram(gr.get(),h);
  for(const auto& gr : ratios.m_unc) functions::divideGraphByHistogram(gr.get(),h);
  
  return ratios;
}

//----------------------------------------------------------------------
  
std::vector<Predictions> Predictions::makeProjections(const HistogramNDto1DConverter* histogramConverter) const {
    
  std::vector<TH1D*> dataProjections;
  if(m_data!=0) dataProjections = histogramConverter->makeProjections(m_data.get(),"data"); // Make projections from concatenated histogram  
  
  std::vector<std::vector<TH1D*>> histosProjections;
  std::vector<std::vector<TGraphAsymmErrors*>> graphsProjections;
  std::vector<std::vector<TGraphAsymmErrors*>> uncProjections;
  
  int i=0;
  for(const auto& h : m_histos) {
    histosProjections.push_back(histogramConverter->makeProjections(h.get(),Form("histos_%i",i)));
    i++;
  }
  
  i=0;
  for(const auto& gr : m_graphs) {
    graphsProjections.push_back(histogramConverter->makeProjections(gr.get(),Form("graphs_%i",i)));
    i++;
  }
  i=0;
  for(const auto& gr : m_unc) {
    uncProjections.push_back(histogramConverter->makeProjections(gr.get(),Form("unc_%i",i)));
    i++;
  }
  
  
  
  const size_t nhistos = m_histos.size();
  const size_t ngraphs = m_graphs.size();
  const size_t nunc = m_unc.size();
  
  size_t nprojections = dataProjections.size();
  if(nhistos>0) nprojections = histosProjections[0].size();
  else if(ngraphs>0) nprojections = graphsProjections[0].size();
  else if(nunc>0) nprojections = uncProjections[0].size();
  
  
  std::vector<Predictions> projections{nprojections};
  for(size_t iproj = 0 ; iproj < nprojections; iproj++) {
    if(m_data!=0) projections[iproj].setData(dataProjections[iproj],false);
    for(size_t j = 0 ; j < nhistos; j++) projections[iproj].addHistogram(histosProjections[j][iproj],false); 
    for(size_t j = 0 ; j < ngraphs; j++)projections[iproj].addGraph(graphsProjections[j][iproj],false);
    for(size_t j = 0 ; j < nunc; j++) projections[iproj].addUnc(uncProjections[j][iproj],false); 
  }
    
  return projections;
}

//----------------------------------------------------------------------


void Predictions::setRangeUser(const double ymin, const double ymax) {
  
  m_data->GetYaxis()->SetRangeUser(ymin,ymax);
  for(auto& h : m_histos) h->GetYaxis()->SetRangeUser(ymin,ymax);
  for(auto& gr : m_graphs) gr->GetYaxis()->SetRangeUser(ymin,ymax);
  
}


//----------------------------------------------------------------------


void Predictions::setRangeUser(const bool useLogScale, const double bottomSpace,const double upSpace) {
  
  double ymin = useLogScale ? getMinimum(0.) : getMinimum();
  double ymax = getMaximum();
  
  // This is hardcoded
  if(useLogScale){
    ymax = ymax*(1+pow(ymax/ymin,upSpace));
    ymin = ymin/(1+pow(ymax/ymin,bottomSpace));
    //ymin = ymin/2;
  }
  else{
    ymax= ymax + (ymax-ymin)*upSpace;
    ymin= ymin - (ymax-ymin)*bottomSpace;
  }
  
  setRangeUser(ymin,ymax);
}

//----------------------------------------------------------------------

void Predictions::setXtitle(const TString& title) {
  m_data->GetXaxis()->SetTitle(title);
  for(auto& h : m_histos) h->GetXaxis()->SetTitle(title);
  for(auto& gr : m_graphs) gr->GetXaxis()->SetTitle(title);
}

//----------------------------------------------------------------------

void Predictions::setYtitle(const TString& title) {
  m_data->GetYaxis()->SetTitle(title);
  for(auto& h : m_histos) h->GetYaxis()->SetTitle(title);
  for(auto& gr : m_graphs) gr->GetYaxis()->SetTitle(title);
}

//----------------------------------------------------------------------

double Predictions::getMinimum(const double minVal) const {
  
  // Getting minimum from histograms
  double histMin{FLT_MAX};
  if(!m_histos.empty()) {
    auto it = std::min_element(m_histos.begin(),m_histos.end(),[&minVal](const auto& a, const auto& b){
      return functions::getMinimum(a.get(),minVal) < functions::getMinimum(b.get(),minVal);
    });
    histMin = functions::getMinimum(it->get(),minVal);
  }
  // Getting minimum from graphs
  double graphMin{FLT_MAX};
  if(!m_graphs.empty()) {
    auto it = std::min_element(m_graphs.begin(),m_graphs.end(),[&minVal](const auto& a, const auto& b){
      return functions::getMinimum(a.get(),minVal) < functions::getMinimum(b.get(),minVal);
    });
    graphMin = functions::getMinimum(it->get(),minVal);
  }
  
  double uncMin{FLT_MAX};
  if(!m_unc.empty()) {
    auto it = std::min_element(m_unc.begin(),m_unc.end(),[&minVal](const auto& a, const auto& b){
      return functions::getMinimum(a.get(),minVal) < functions::getMinimum(b.get(),minVal);
    });
    uncMin = functions::getMinimum(it->get(),minVal);
  }
  
  
  double yMin = std::min(uncMin,std::min(graphMin,histMin));
  if(m_data!=0) yMin = std::min(functions::getMinimum(m_data.get(),minVal),yMin);
  
  return yMin;
}

//----------------------------------------------------------------------

double Predictions::getMaximum(const double maxVal) const {
  
  // Getting maximum from histograms
  double histMax{-FLT_MAX};
  if(!m_histos.empty()) {
    auto it = std::max_element(m_histos.begin(),m_histos.end(),[&maxVal](const auto& a, const auto& b){
      return functions::getMaximum(a.get(),maxVal) < functions::getMaximum(b.get(),maxVal);
    });
    histMax = functions::getMaximum(it->get(),maxVal);
  }
  // Getting maximum from graphs
  double graphMax{-FLT_MAX};
  if(!m_graphs.empty()) {
    auto it = std::max_element(m_graphs.begin(),m_graphs.end(),[&maxVal](const auto& a, const auto& b){
      return functions::getMaximum(a.get(),maxVal) < functions::getMaximum(b.get(),maxVal);
    });
    graphMax = functions::getMaximum(it->get(),maxVal);
  }
  
  double uncMax{-FLT_MAX};;
  if(!m_unc.empty()) {
    auto it = std::max_element(m_unc.begin(),m_unc.end(),[&maxVal](const auto& a, const auto& b){
      return functions::getMaximum(a.get(),maxVal) < functions::getMaximum(b.get(),maxVal);
    });
    uncMax = functions::getMaximum(it->get(),maxVal);
  }
  
  
  double yMax = std::max(uncMax,std::max(graphMax,histMax));
  if(m_data!=0) yMax = std::max(functions::getMaximum(m_data.get(),maxVal),yMax);
  
  return yMax;
}

//----------------------------------------------------------------------

void Predictions::fillLegend(TLegend* leg) const {
  
  auto itEnd = std::end(m_legendNames);
  
  if(m_legendNames.find("data")!=itEnd && !m_legendNames.at("data").empty()) {
    leg->AddEntry(m_data.get(),m_legendNames.at("data")[0],"p");
  }
  
  if(m_legendNames.find("histos")!=itEnd) {
    const auto& names = m_legendNames.at("histos");
    size_t m = std::max(names.size(),m_histos.size());
    for(size_t i=0;i<m;i++) {
      leg->AddEntry(m_histos[i].get(),names[i]);
    }
  }
  
  if(m_legendNames.find("graphs")!=itEnd) {
    const auto& names = m_legendNames.at("graphs");
    size_t m = std::max(names.size(),m_graphs.size());
    for(size_t i=0;i<m;i++) {
      leg->AddEntry(m_graphs[i].get(),names[i]);
    }
  }
  
  if(m_legendNames.find("unc")!=itEnd) {
    const auto& names = m_legendNames.at("unc");
    size_t m = std::max(names.size(),m_unc.size());
    for(size_t i=0;i<m;i++) {
      leg->AddEntry(m_unc[i].get(),names[i],"f");
    }
  }
  
}

//----------------------------------------------------------------------

