#include "TTbarHelperFunctions/histManipFunctions.h"
#include "TTbarHelperFunctions/stringFunctions.h"
#include "TTbarHelperFunctions/functions.h"
#include <regex>
#include "TLine.h"
#include "TPaveText.h"


namespace functions{
  
  std::vector<std::string> splitString(std::string input,const std::string& delimiter) {
    
    size_t pos = 0;
    std::vector<std::string> output;
    
    while ((pos = input.find(delimiter)) != std::string::npos) {
      output.push_back(input.substr(0, pos));
      input.erase(0, pos + delimiter.length());
    }
    if(input!="") output.push_back(input);
    
    return output;
    
  }

//----------------------------------------------------------------------

  std::vector<TString> splitTString(TString input,const TString& delimiter) {
  
    Ssiz_t pos = 0;
    std::vector<TString> output;
    
    while ((pos = input.First(delimiter)) != TString::kNPOS) {
      output.push_back(input(0, pos));
      input = input.Remove(0, pos + delimiter.Length());
    }
    output.push_back(input);

    return output;
  }

//----------------------------------------------------------------------
  
  std::string convertNumberToString(const int number){
    std::stringstream ss;//create a stringstream
    ss << number;//add number to the stream
    return ss.str();//return a string with the contents of the stream
  }
  
//----------------------------------------------------------------------

  std::string convertNumberToString(double number, const int nDigits){
    std::stringstream ss;//create a stringstream

    if (nDigits>=0) {
      if (number < 1e-6) return("0");
      int number_of_digits_before_dot=0;
      if (fabs(number)>=1){
        float result=number;
        while (fabs(result)>=1){
          result=result / 10.;
          number_of_digits_before_dot++;
        }
      }
      else{
        float result=number*10;
        while (fabs(result)<1){
          result=result * 10;
          number_of_digits_before_dot--;
        }
      }
      int rounding=nDigits-number_of_digits_before_dot;
      //cout << "rounding to " << rounding << " digits" << endl;
      number=floor(number*pow(10.,rounding)+0.5)/pow(10.,rounding);
      ss.precision(rounding);
    }

    ss << number;//add number to the stream
    return ss.str();//return a string with the contents of the stream
  }

//----------------------------------------------------------------------

  void replaceFirst(TString& str,const TString& from,const TString& to){
    std::string s=str.Data();
    size_t pos = s.find(from.Data());
    if(pos!=std::string::npos) str = str.Replace(pos,from.Length(),to);
  }

//----------------------------------------------------------------------

  void replaceLast(TString& str,const TString& from,const TString& to){
    std::string s=str.Data();
    size_t pos = s.rfind(from.Data());
    if(pos!=std::string::npos) str = str.Replace(pos,from.Length(),to);
  }
  
//----------------------------------------------------------------------

  bool compareWildcard(const std::string& input, const std::string& filter){
  
    std::string filter_regex = regex_replace(filter, std::regex("\\*"), ".*");
    std::smatch match;
    std::regex_search(input, match, std::regex("^"+filter_regex));
    if (!match.empty()) return false;
    return true;
  }

//----------------------------------------------------------------------

}


