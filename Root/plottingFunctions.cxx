#include "TTbarHelperFunctions/plottingFunctions.h"
#include "TTbarHelperFunctions/histManipFunctions.h"
#include "TTbarHelperFunctions/namesAndTitles.h"
#include "TTbarHelperFunctions/functions.h"
#include <algorithm>
#include <memory>
#include "TLine.h"
#include "TPaveText.h"
#include "THStack.h"

namespace functions{

  void PrintCanvas(TCanvas *can, const TString& sampleDir, const TString& mc_production, const TString& filename,Bool_t logy){
    if (logy){
      gPad->SetLogy();
      //can->SetLogy(); 
      can->SaveAs(sampleDir+"pdf."+ mc_production + "/"+filename+"_log.pdf");
      can->SaveAs(sampleDir+"png."+ mc_production + "/"+filename+"_log.png");
      //can->SaveAs(sampleDir+"C."+ mc_production + "/"+filename+"_log.C");
      //can->SaveAs(sampleDir+"root."+ mc_production + "/"+filename+"_log.root");
      //can->SetLogy(0); 
      gPad->SetLogy(0);
    }
    else{
      can->SaveAs(sampleDir+"pdf."+ mc_production + "/"+filename+".pdf");
      can->SaveAs(sampleDir+"png."+ mc_production + "/"+filename+".png");
      //can->SaveAs(sampleDir+"C."+ mc_production + "/"+filename+".C");
      //can->SaveAs(sampleDir+"root."+ mc_production + "/"+filename+".root");
    }
    
    
  }

//----------------------------------------------------------------------

  void setStyle() {
    gROOT->SetStyle("ATLAS");
    gStyle->SetErrorX();
    gStyle->SetLegendBorderSize(0);
    gStyle->SetLegendFillColor(0);
    gStyle->SetLegendFont(42);
    gStyle->SetLegendTextSize(0.05);
  }

//----------------------------------------------------------------------

  void drawStackHistos(const TString& figurename, TH1D* data, const vector<TH1D*>& mc, TGraphAsymmErrors* stat, TGraphAsymmErrors* unc, const std::vector<TString>& info, TLegend* leg, bool drawRatio, bool useLogScale) {
    
    const int nhistos = mc.size();
    if (nhistos==0) {
      std::cout << "Error: vector with MC is empty" << std::endl;
      return;
    }
    
    std::unique_ptr<TH1D> pseudodata{(TH1D*)mc.back()->Clone()};
    
    for(int i=0;i<nhistos-1;i++) {
      pseudodata->Add(mc[i]);
    }
    pseudodata->SetFillColor(0);
    
    std::unique_ptr<TCanvas> can;
    std::unique_ptr<TPad> pad1;
    std::unique_ptr<TPad> pad2;
    if(drawRatio) {
      can = std::make_unique<TCanvas>("can","canvas 2 pads",0,0,1000,600);
      pad1 = std::make_unique<TPad>("pad1","pad1",0,0.33,1,1,0,0,0);
      pad2 = std::make_unique<TPad>("pad2","pad2",0,0,1,0.33,0,0,0);
      
      pad2->SetBottomMargin(0.55);
      pad2->SetTopMargin(0);
      pad2->SetRightMargin(0.05);
      pad2->SetGridy(1);
      pad2->SetTicks();
      pad1->SetBottomMargin(0.);
      pad1->SetTopMargin(0.05);
      pad1->SetRightMargin(0.05);
      pad1->SetTicks();
      pad1->Draw();
      pad2->Draw();
      
      pad1->cd();
    }
    else {
      can = std::make_unique<TCanvas>("can","canvas 2 pads",0,0,1000,600);
      pad1 = std::make_unique<TPad>("pad1","pad1",0,0,1,1,0,0,0);
      pad1->SetTicks();
      pad1->SetTopMargin(0.05);
      pad1->SetRightMargin(0.05);
      pad1->Draw();
    }
    
    std::unique_ptr<THStack> stack{functions::makeTHStack("stack",mc)};
    
    double max= std::max(data->GetMaximum(),pseudodata->GetMaximum());    
    double min = mc[0]->GetMinimum(0.);
    
    double ymin=min/5;
    double ymax=max*1.3;
    
    if(useLogScale){
      ymin=min/5;
      ymax=max*200;
    }
    data->GetYaxis()->SetRangeUser(ymin,ymax);
    
    // Plotting into pad1
    functions::drawStackHistos(pad1.get(), data, pseudodata.get(), stack.get(), {unc,stat});
    leg->Draw();
    pad1->Update();
    pad1->RedrawAxis("g");
    
    std::unique_ptr<TH1D> ratio;
    std::unique_ptr<TGraphAsymmErrors> unit_graph;
    std::unique_ptr<TGraphAsymmErrors> unit_graph_stat;
    
    if (drawRatio) {
  
      ratio = std::unique_ptr<TH1D>{(TH1D*)data->Clone()};
        
      for(int ibin=1;ibin<=ratio->GetNbinsX();ibin++){
        double bincontent = pseudodata->GetBinContent(ibin)>0 ? data->GetBinContent(ibin)/pseudodata->GetBinContent(ibin) : 0.;
        ratio->SetBinContent(ibin,bincontent);
        double binerror = pseudodata->GetBinContent(ibin)>0 ? data->GetBinError(ibin)/pseudodata->GetBinContent(ibin) : 0.;
        ratio->SetBinError(ibin,binerror);
      }
      
      ratio->SetYTitle("Data / Pred.");
      
      functions::setAxisLabelStyle(ratio->GetXaxis(), 0.13, 1, 0.11, 0.01);
      functions::setAxisLabelStyle(ratio->GetYaxis(), 0.1, 0.6, 0.09, 0.01,10);
      
      ymin=0.41;
      ymax=1.59;
      ratio->GetYaxis()->SetRangeUser(ymin,ymax);
      ratio->SetLineColor(1);
      
      unit_graph = std::unique_ptr<TGraphAsymmErrors>{functions::getUnitGraph(unc)};
      unit_graph_stat= std::unique_ptr<TGraphAsymmErrors>{functions::getUnitGraph(stat)};
      
      functions::plotHistogram(pad2.get(), ratio.get(), {unit_graph.get(),unit_graph_stat.get()},"");
      
      //pad2->cd();
      
      //ratio->Draw();
      
      //unit_graph->Draw("same2");
      //unit_graph_stat->Draw("same2");
      
      //ratio->Draw("same");
        
      pad2->RedrawAxis("g"); 
      pad2->RedrawAxis(); 
      
    }
    
    pad1->cd();
        
    double size=0.049;
    double x=0.18;
    double y=0.92;
    double shift=0.055;
    
    const int nlines = info.size();
    
    TPaveText paveText (x,y-nlines*shift,0.3,y,"NDC"); 
    paveText.SetFillColor(0);
    paveText.SetBorderSize(0);
    paveText.SetTextSize(size);
    paveText.SetTextAlign(12);
    paveText.SetTextFont( 42 );
    
    for(const TString& line : info) {
      paveText.AddText(line);
    }
    paveText.Draw();
    
    pad1->SetLogy(useLogScale);
    pad1->RedrawAxis("g"); 
    pad1->RedrawAxis(); 
    
    if (figurename.EndsWith(".pdf") || figurename.EndsWith(".png") || figurename.EndsWith(".root") || figurename.EndsWith(".C")) {
      can->SaveAs(figurename);
    }
    else {
      can->SaveAs(figurename+".pdf");
    }  
    
    
  }


//----------------------------------------------------------------------

  void drawStackHistos(TPad* pad, TH1D* data, TH1D* pseudodata, THStack* stack, const vector<TGraphAsymmErrors*>& unc){
    pad->cd();
    data->Draw();
    for(TGraphAsymmErrors* gr : unc) {
      gr->Draw("same2");
    }
    stack->Draw("HISTsame"); 
    pseudodata->Draw("HISTsame");
    data->Draw("same");
  }

//----------------------------------------------------------------------

  void plotHistogram(TPad* pad, TH1D* h, const vector<TGraphAsymmErrors*>& unc, const TString& option) {
    pad->cd();
    h->Draw(option);
    for(TGraphAsymmErrors* gr : unc) {
      gr->Draw("same2");
    }
    h->Draw(option+" same");
  }

//----------------------------------------------------------------------

  void getGridSize(const int nProjections,int& nLines,int& nPerLine) {
    nPerLine = 0;
    
    if(nProjections < 4) nPerLine=nProjections;
    else if(nProjections ==4 ) nPerLine=2;
    else if (nProjections <= 9) nPerLine=3;
    else if (nProjections == 10 || nProjections == 15) nPerLine=5;
    else if (nProjections < 17) nPerLine=4;
    else nPerLine=5;
    
    nLines = std::ceil((double)nProjections / nPerLine);
    
  }

//----------------------------------------------------------------------
  
  void setAxisLabelStyle(TAxis* axis, const double titleSize, const double titleOffset, const double labelSize, const double labelOffset, const int nDivisions) {
    axis->SetTitleSize(titleSize);
    axis->SetTitleOffset(titleOffset);
    axis->SetLabelSize(labelSize);
    axis->SetLabelOffset(labelOffset);
    if(nDivisions>0) axis->SetNdivisions(nDivisions);
  }

//----------------------------------------------------------------------

  TPaveText* makePaveText(const std::vector<TString>& info, const double size, const double xmin, const double xmax, const double ymax) {

    double shift=size*1.14;
    const int nlines = info.size();
    
    TPaveText* paveText = new TPaveText(xmin,ymax-nlines*shift,xmax,ymax,"NDC"); 
    paveText->SetFillColor(0);
    paveText->SetBorderSize(0);
    paveText->SetTextSize(size);
    paveText->SetTextAlign(12);
    paveText->SetTextFont( 42 );
    
    for(const TString& line : info) {
      paveText->AddText(line);
    }
    
    return paveText;
  }

//----------------------------------------------------------------------

  vector<vector<shared_ptr<TPad>>> makeArrayOfPads(const int ntotal, int nPerLine,const double xmin,const double ymin,const double xmax,const double ymax,const double spacex, const double spacey, const double leftMargin,const double rightMargin,const double topMargin,const double bottomMargin) {
    
    if(nPerLine>ntotal) nPerLine=ntotal;
    
    const int nlines = std::ceil((double)ntotal/nPerLine);
    const double xsize = (xmax - xmin - (nPerLine-1)*spacex)/nPerLine;
    const double ysize = (ymax - ymin - (nlines-1)*spacey)/nlines;
    vector<vector<shared_ptr<TPad>>> pads(nlines);
    
    
    // We start at left top corner
    double xmin_pad = xmin;
    double xmax_pad = xmin_pad + xsize;
    double ymax_pad = ymax;
    double ymin_pad = ymax_pad - ysize;
        
    for(int i=0;i<ntotal;i++) {
      int iline = i / nPerLine;

      int ix = i % nPerLine;
      pads[iline].push_back(std::make_shared<TPad>(Form("Pad_%i",i),"",xmin_pad,ymin_pad,xmax_pad,ymax_pad,0,0,0));
      
      if(ix < nPerLine-1) {
        xmin_pad = xmax_pad+spacex;
        xmax_pad += xsize+spacex;
      }
      else {
        xmin_pad = xmin;
        xmax_pad = xmin_pad + xsize;
        ymax_pad = ymin_pad - spacey;
        ymin_pad -= (ysize+spacey);
      }
      
      ymin_pad=std::max(ymin,ymin_pad);  // This is protection for small negative values like -1e-16 which can occur due to limited precision
      ymax_pad=std::min(ymax,ymax_pad);
      xmin_pad=std::max(xmin,xmin_pad);
      xmax_pad=std::min(xmax,xmax_pad);
      
      pads[iline].back()->SetFillStyle(0);
      pads[iline].back()->SetTopMargin(topMargin);
      pads[iline].back()->SetBottomMargin(bottomMargin);
      pads[iline].back()->SetLeftMargin(leftMargin);
      pads[iline].back()->SetRightMargin(rightMargin);
      
      pads[iline].back()->Draw(); // Pads are drawn into current canvas
    }
    return pads;
    
  }


//----------------------------------------------------------------------


  void plotHistograms(vector<TH1D*>& hist,TLegend* leg,TString dirname,TString name,TH1D* hd, TString y2name, double y2min, double y2max, TString lumi,bool use_logscale){
          
    TH1D* hdenumerator = (TH1D*)hd->Clone("hdenumerator");
    vector<TH1D*> hist_ratio;
    
    //gStyle->SetEndErrorSize(5);
    int n=hist.size();
    //double legX1 = leg->GetX1();
    //cout << legX1 << endl;
    int imiddlebin=1;
    const int nbins=hist[0]->GetNbinsX();
    const double upper_edge = hist[0]->GetBinLowEdge(nbins-1) + hist[0]->GetBinWidth(nbins-1);
    const double low_edge = hist[0]->GetBinLowEdge(1);
    const double middle =(upper_edge + low_edge)*0.55;
    for(int i=1;i<nbins;i++){
      if(hist[0]->GetBinLowEdge(i+1) > middle){
        imiddlebin=i;
        break;
      }
      imiddlebin=nbins;
    }
    //cout << imiddlebin << endl;
    
    for(int i=0;i<n;i++) hist_ratio.push_back((TH1D*)hist[i]->Clone());
    
    
    for(int i=0;i<n;i++) hist[i]->SetMarkerSize(0.7);
    double max1(0.),max2(0.);
    double min1(FLT_MAX);
    double y,y2,z;
    for(int i=0;i<n;i++){
      y=hist[i]->GetMaximum();
      if(max1 < y) max1 = y;
      for(int j=imiddlebin;j<=nbins;j++){
        y2=hist[i]->GetBinContent(j);
        if(max2<y2)max2=y2;
              
      }
      z=hist[i]->GetMinimum(0.);
      if(min1 > z) min1=z;
    }
    
    
    double max_final(0.);
    double min_final(0.);
    
    if(use_logscale){
      
      max_final=max1*(1+pow(max1/min1,1./2.));
      min_final=min1/(1+pow(max1/min1,1./4.));
    }
    else{
      max_final=std::max(1.5*max1,2.5*max2);
      min_final=min1/100;
    }
    
    TH2D* h = new TH2D("","",hist[0]->GetXaxis()->GetNbins(),hist[0]->GetXaxis()->GetXmin(),hist[0]->GetXaxis()->GetXmax(),2.,min_final,max_final);
    h->SetStats(kFALSE);
    h->SetXTitle(hist[0]->GetXaxis()->GetTitle());
    //h->SetXTitle(xname);
    h->SetYTitle(hist[0]->GetYaxis()->GetTitle());
    //h->SetYTitle("N_{events} [1/GeV]");
    //h->SetYTitle(yname);
    h->GetYaxis()->SetTitleSize(0.075);
    h->GetYaxis()->SetTitleOffset(0.6);
    h->GetYaxis()->SetLabelSize(0.06);
    
    
    TCanvas* c = new TCanvas("c","");
    
    TPad *pad1 = new TPad("pad1","pad1",0,0.33,1,1,0,0,0);
    TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.33,0,0,0);
    pad2->SetBottomMargin(0.3);
    pad2->SetTopMargin(0);
    pad2->SetRightMargin(0.05);
    pad2->SetGridy(1);
    pad2->SetTicks();
    pad1->SetBottomMargin(0.);
    pad1->SetTopMargin(0.05);
    pad1->SetRightMargin(0.05);
    pad1->SetTicks();
    if(use_logscale){
      pad1->SetLogy();
      //pad2->SetLogy();
    }
    pad1->Draw();
    pad2->Draw();
    pad1->cd();
    pad2->cd();
    
    pad1->cd();
    h->Draw();
    WriteGeneralInfo("",lumi,0.06,0.2,0.85,0.05);
    for(int i=0;i<n;i++) hist[i]->DrawClone("same");
    leg->Draw();
  
    TH2D* h2 = new TH2D("","",hist[0]->GetXaxis()->GetNbins(),hist[0]->GetXaxis()->GetXmin(),hist[0]->GetXaxis()->GetXmax(),2.,y2min,y2max);
    h2->SetStats(kFALSE);
    //h2->SetXTitle(hist[0]->GetXaxis()->GetTitle());
    h2->SetXTitle(h->GetXaxis()->GetTitle());
    h2->SetYTitle(y2name);
    h2->GetXaxis()->SetTitleSize(0.15);
    h2->GetXaxis()->SetTitleOffset(0.85);
    h2->GetYaxis()->SetTitleSize(0.11);
    h2->GetYaxis()->SetTitleOffset(0.38);
    h2->GetYaxis()->SetNdivisions(10);
    h2->GetXaxis()->SetLabelSize(0.123);
    h2->GetYaxis()->SetLabelSize(0.123);
    
    pad2->cd();
    h2->Draw();
    bool ok=0;
    for(int i=0;i<n;i++) { 
    hist_ratio[i]->Divide(hdenumerator);
    if(hist[i] == hd) {
      for(int j=0;j<nbins;j++) hist_ratio[i]->SetBinError(j+1,hdenumerator->GetBinError(j+1)/hdenumerator->GetBinContent(j+1));
      ok=1;
    }
    else for(int j=0;j<nbins;j++) hist_ratio[i]->SetBinError(j+1,hist[i]->GetBinError(j+1)/hdenumerator->GetBinContent(j+1));
    hist_ratio[i]->Draw("same");
  
    }
    if(ok==0){
      for(int j=0;j<nbins;j++)hdenumerator->SetBinError(j+1,hdenumerator->GetBinError(j+1)/hdenumerator->GetBinContent(j+1));
      hdenumerator->Draw("same");
    }
    //pad2->RedrawAxis("g");
    //pad2->RedrawAxis();
  
    TString figurename=dirname + name;
    if(figurename.EndsWith(".pdf") || figurename.EndsWith(".png") )c->SaveAs(figurename);
    else c->SaveAs(figurename + ".pdf");
    delete c;
    delete h;
    delete h2;
  }

//----------------------------------------------------------------------

  void plotHistogramsWithFixedYRange(vector<TH1D*>& hist,TLegend* leg,TString dirname,TString name,TString lumi,double ymin,double ymax,bool use_logscale,TString info,TString info2){
    
    int n=hist.size();
    
    if(n==0){
      cout << "Error in plotHistogramsWithFixedYRange: vector of histograms is empty! Leaving function." << endl; 
      return;
    }
    
    vector<double> binEdges(hist[0]->GetXaxis()->GetNbins()+1);
    binEdges[0]=hist[0]->GetXaxis()->GetBinLowEdge(1);
    for(int i=1;i<=hist[0]->GetXaxis()->GetNbins();i++)binEdges[i]=hist[0]->GetXaxis()->GetBinUpEdge(i);
    
    
    TH2D* h = new TH2D("","",hist[0]->GetXaxis()->GetNbins(),&binEdges[0],2,ymin,ymax);
    h->SetStats(kFALSE);
    h->SetXTitle(hist[0]->GetXaxis()->GetTitle());
    //h->SetXTitle(xname);
    h->SetYTitle(hist[0]->GetYaxis()->GetTitle());
    //h->SetYTitle("N_{events} [1/GeV]");
    //h->SetYTitle(yname);
    h->GetYaxis()->SetTitleSize(0.055);
    h->GetYaxis()->SetTitleOffset(1);
    //h->GetYaxis()->SetLabelSize(0.04);
    h->GetXaxis()->SetTitleSize(0.055);
    h->GetXaxis()->SetTitleOffset(1.3);
    //h->GetXaxis()->SetLabelSize(0.04);
    h->GetXaxis()->SetLabelOffset(0.03);
    
    
    TCanvas* c = new TCanvas("c","",1200, 800);
    if(use_logscale)c->SetLogy();
  
    h->Draw();
    
    double size =0.043;
    double x=0.185;
    double y=0.875;
    WriteGeneralInfo("",lumi,size,x,y,0.055);
    y-=0.11;
    WriteInfo(info, 0.04, x,y);y-=0.055;
    //if(info.Contains("Parton")){
    //  WriteInfo("#bf{p^{t,1}_{T} > 500 GeV, p^{t,2}_{T} > 350 GeV}", 0.035, x,y);y-=0.055;
   // }
    WriteInfo(info2, 0.04, x,y);y-=0.05;

    for(int i=0;i<n;i++) {
      if(i<2)hist[i]->Draw("F same");
      else hist[i]->Draw("same");
    }

    TH1D* hcopy=(TH1D*)hist[0]->Clone();
    hcopy->SetFillColor(0);
    hcopy->Draw("p same");
    leg->Draw();
    
    c->RedrawAxis("g");
    
    TString figurename=dirname + name;
    if(figurename.EndsWith(".pdf") || figurename.EndsWith(".png") || figurename.EndsWith(".root"))c->SaveAs(figurename);
    else c->SaveAs(figurename + ".pdf");
    delete c;
    delete h;
    
    
  }

//----------------------------------------------------------------------
        
  void plotHistograms(vector<TH1D*>& hist,TLegend* leg,TString dirname,TString name,TString lumi,bool use_logscale,TString info,TString info2){
  
  
    int n=hist.size();
    if(n==0) return;
    const int nbins=hist[0]->GetNbinsX();
    //double legX1 = leg->GetX1();
    //cout << legX1 << endl;
    int imiddlebin=1;
    const double upper_edge = hist[0]->GetBinLowEdge(nbins-1) + hist[0]->GetBinWidth(nbins-1);
    const double low_edge = hist[0]->GetBinLowEdge(1);
    const double middle =(upper_edge + low_edge)*0.55;
    for(int i=1;i<nbins;i++){
      if(hist[0]->GetBinLowEdge(i+1) > middle){
        imiddlebin=i;
        break;
      }
      imiddlebin=nbins;
    }
    //cout << imiddlebin << endl;
    
    
    for(int i=0;i<n;i++) hist[i]->SetMarkerSize(0.7);
    double min1(FLT_MAX);
    double ymin_threshold=0.;
    double max1(ymin_threshold),max2(ymin_threshold);
    
    double y,y2;
    for(int i=0;i<n;i++){
      for(int j=1;j<=nbins;j++){
        y=hist[i]->GetBinContent(j);
        if(max1 < y) max1 = y;
        if ( (y > ymin_threshold) &&  (min1 > y) ) min1=y;
      }
      
      for(int j=imiddlebin;j<=nbins;j++){
        y2=hist[i]->GetBinContent(j);
        if(max2<y2)max2=y2;
        
      }
    }
    
    //cout << name << " " << max1 << " " << max2 << endl;
    //cout << imiddlebin << endl;
    //cout << low_edge << " " << upper_edge << " " << middle << endl;
    
    
    //double denum = use_logscale ? 10 : 100;
    double max_mult= use_logscale ? 10 : 0.8;
    TString xtitle=hist[0]->GetXaxis()->GetTitle();
    
    double ymin=0.;
    double ymax=max(2.*max1,2.*max2)*max_mult;

    /*if(xtitle.Contains("p_{T}^{t,1}") && info2.Contains("Normalized") ) ymax=34.;
    if(xtitle.Contains("|y^{t,1}|") && info2.Contains("Normalized") ) ymax=18.;
    if(xtitle.Contains("p_{T}^{t,2}") && info2.Contains("Normalized") ) ymax=30.;
    if(xtitle.Contains("|y^{t,2}|") && info2.Contains("Normalized") ) ymax=17.;
    if(xtitle.Contains("m^{t#bar{t}}") && info2.Contains("Normalized") ) ymax=41.;
    if(xtitle.Contains("p_{T}^{t#bar{t}}") && info2.Contains("Normalized") ) ymax=51.;
    if(xtitle.Contains("|y^{t#bar{t}}|") && info2.Contains("Normalized") ) ymax=23.;

    if(xtitle.Contains("p_{T}^{t,1}") && info2.Contains("Absolute") ) ymax=35.;
    if(xtitle.Contains("|y^{t,1}|") && info2.Contains("Absolute") ) ymax=29.;
    if(xtitle.Contains("p_{T}^{t,2}") && info2.Contains("Absolute") ) ymax=33.;
    if(xtitle.Contains("|y^{t,2}|") && info2.Contains("Absolute") ) ymax=26.;
    if(xtitle.Contains("m^{t#bar{t}}") && info2.Contains("Absolute") ) ymax=38.;
    if(xtitle.Contains("p_{T}^{t#bar{t}}") && info2.Contains("Absolute") ) ymax=42.;
    if(xtitle.Contains("|y^{t#bar{t}}|") && info2.Contains("Absolute") ) ymax=33.;
    */         
             
             if(use_logscale){
      ymax = ymax*(1+pow(ymax/ymin,0.75));
      ymin = ymin/(1+pow(ymax/ymin,1./6.));
      
    }
    
    
    
    if(ymin>=ymax)ymax=fabs(ymin)*1.01+0.01;
    cout << "ymin : ymax = " << ymin << " : " << ymax << endl;
    
    plotHistogramsWithFixedYRange(hist,leg,dirname,name,lumi,ymin,ymax,use_logscale,info,info2);
    
  }

//----------------------------------------------------------------------
  
  void plotHistograms2DConcatenated(vector<TH1D*>& hist,vector<double>& binning_x,vector<vector<double> >& binning_y,TLegend* leg,TString dirname,TString name,TString lumi,bool splitVerticalLines,bool use_logscale,TString info,TString info2){
    
    int n=hist.size();
    if(n==0) return;
    const int nbins=hist[0]->GetNbinsX();
    //double legX1 = leg->GetX1();
    //cout << legX1 << endl;
    int imiddlebin=1;
    const double upper_edge = hist[0]->GetBinLowEdge(nbins-1) + hist[0]->GetBinWidth(nbins-1);
    const double low_edge = hist[0]->GetBinLowEdge(1);
    const double middle =(upper_edge + low_edge)*0.55;
    for(int i=1;i<nbins;i++){
      if(hist[0]->GetBinLowEdge(i+1) > middle){
        imiddlebin=i;
        break;
      }
      imiddlebin=nbins;
    }
    //cout << imiddlebin << endl;
    
    
    
    for(int i=0;i<n;i++) hist[i]->SetMarkerSize(0.7);
    double min1(FLT_MAX);
    double ymin_threshold=0.;
    double max1(ymin_threshold),max2(ymin_threshold);
    
    double y,y2;
    for(int i=0;i<n;i++){
      for(int j=1;j<=nbins;j++){
        y=hist[i]->GetBinContent(j);
        if(max1 < y) max1 = y;
        if ( (y > ymin_threshold) &&  (min1 > y) ) min1=y;
      }
      
      for(int j=imiddlebin;j<=nbins;j++){
        y2=hist[i]->GetBinContent(j);
        if(max2<y2)max2=y2;
        
      }
    }
    
    //cout << name << " " << max1 << " " << max2 << endl;
    //cout << imiddlebin << endl;
    //cout << low_edge << " " << upper_edge << " " << middle << endl;
    
    cout << min1/100 << " " << max(1.5*max1,2.5*max2) << endl;
    //double denum = use_logscale ? 10 : 100;
    double max_mult= use_logscale ? 10 : 0.8;
    TString xtitle=hist[0]->GetXaxis()->GetTitle();
    if(xtitle.Contains("p_{T}^{t,1}"))max_mult=0.7;
    
    double ymin=0.;
    double ymax=max(2.*max1,2.*max2)*max_mult;
    if(use_logscale){
      ymax = max1*(1+pow(max1/min1,0.75));
      ymin = min1/(1+pow(max1/min1,1./6.));
      
    }
    
    if(ymin>=ymax)ymax=fabs(ymin)*1.01+0.01;
    cout << "ymin : ymax = " << ymin << " : " << ymax << endl;
    
    plotHistograms2DConcatenatedWithFixedYRange(hist,binning_x,binning_y,leg,dirname,name,lumi,splitVerticalLines,ymin,ymax,use_logscale,info,info2);

  }

//----------------------------------------------------------------------
  
  void plotHistograms2DConcatenatedWithFixedYRange(vector<TH1D*>& hist,vector<double>& binning_x,vector<vector<double> >& binning_y,TLegend* leg,TString dirname,TString name,TString lumi,bool splitVerticalLines,double ymin,double ymax,bool use_logscale,TString info,TString info2){
    
    int n=hist.size();
    
    TString xtitle=hist[0]->GetXaxis()->GetTitle();
    
    vector<double> binEdges(hist[0]->GetXaxis()->GetNbins()+1);
    binEdges[0]=hist[0]->GetXaxis()->GetBinLowEdge(1);
    for(int i=1;i<=hist[0]->GetXaxis()->GetNbins();i++)binEdges[i]=hist[0]->GetXaxis()->GetBinUpEdge(i);
    
    TH2D* h = new TH2D("","",hist[0]->GetXaxis()->GetNbins(),&binEdges[0],2.,ymin,ymax);
    h->SetStats(kFALSE);
    h->SetXTitle(hist[0]->GetXaxis()->GetTitle());
    //h->SetXTitle(xname);
    h->SetYTitle(hist[0]->GetYaxis()->GetTitle());
    //h->SetYTitle("N_{events} [1/GeV]");
    //h->SetYTitle(yname);
    h->GetYaxis()->SetTitleSize(0.055);
    h->GetYaxis()->SetTitleOffset(1);
    //h->GetYaxis()->SetLabelSize(0.04);
    h->GetXaxis()->SetTitleSize(0.055);
    h->GetXaxis()->SetTitleOffset(1.8);
    h->GetXaxis()->SetLabelSize(0.051);
    //h->GetXaxis()->SetLabelOffset(0.01);
    h->GetXaxis()->SetLabelOffset(999);
    
    h->SetNdivisions(1);
    
    
    
    
    TCanvas* c = new TCanvas("c","",1000, 600);
    
    gPad->SetBottomMargin(0.2);
    gPad->SetLogy(use_logscale);
    
    
    h->Draw();
    
    for(int i=0;i<n;i++) {
      if(i<2)hist[i]->Draw("F same");
      else hist[i]->Draw("same");
    }
    TH1D* hcopy=(TH1D*)hist[0]->Clone();
    hcopy->SetFillColor(0);
    hcopy->Draw("same");
    
    
    int lineStyle=4;
    int lineWidth=2;
    int lineColor=kGray+1;
    
    if (splitVerticalLines){
      int lineColor1=kBlack;
      int lineStyle1=1;
      drawVerticalLinesTwoStyles(hist,binning_x,binning_y,ymin,ymax,lineColor1,lineStyle1,lineColor,lineStyle,lineWidth);
    }
    else{
      drawVerticalLines(h,binning_x,binning_y,ymin,ymax,lineColor,lineStyle,lineWidth);
    }
    
    TString precision="4.f";
    TString precision2="4.f";
    
    std::tie(precision,precision2) = setPrecision(name);
    
    //if(name.Contains("ttbar_y_vs_ttbar_mass") || name.Contains("t1_pt_vs_ttbar_mass") ) precision="4.2f";
    //if(name.Contains("ttbar_y_vs_ttbar_mass") || name.Contains("ttbar_y_vs_t1_pt")) precision2="4.1f";
    
    TString variable1_name=xtitle;
    variable1_name.ReplaceAll("#otimes","");
    variable1_name=variable1_name(0,variable1_name.First("  "));
    //if(name.BeginsWith("ttbar_y"))variable1_name = "|y^{t#bar{t}}|";
    
    drawBinLabels2DConcatenated(h,binning_x,binning_y,precision,precision2,variable1_name);
    double ticksSize=0.02;
    drawTicksX(h,binning_x,binning_y,ymin,ymax,ticksSize);
    
    leg->Draw();
    
    
    
    double size =0.043;
    double x=0.173;
    double y=0.93;
    double shift=0.045;
    
    TPaveText paveText (x,y-4.*shift,0.4,y,"NDC"); 
    paveText.SetFillColor(0);
    paveText.SetBorderSize(0);
    paveText.SetTextSize(size);
    paveText.SetTextAlign(12);
    paveText.SetTextFont( 42 );
    
    TString atlasInfo="#bf{#it{ATLAS}} ";
    if(lumi!="") atlasInfo+="Simulation ";
    atlasInfo+= NamesAndTitles::getResultsStatus();
    
    paveText.AddText(atlasInfo);
    TString lumiInfo="#sqrt{s}=13 TeV, " + lumi;
    paveText.AddText(lumiInfo);
    paveText.AddText(info);
    paveText.AddText(info2);
    paveText.Draw();
    
    //WriteGeneralInfo("",lumi,size,x,y,0.05);
    //y-=0.1;
    //WriteInfo(info, 0.04, x,y);y-=0.045;
    //if(info.Contains("Parton")){
      //WriteInfo("#bf{p^{t,1}_{T} > 500 GeV, p^{t,2}_{T} > 350 GeV}", 0.035, x,y);y-=0.055;
    //}
    //WriteInfo(info2, 0.04, x,y);y-=0.05;
    
    
    c->RedrawAxis("g");
    
    TString figurename=dirname + name;
    if(figurename.EndsWith(".pdf") || figurename.EndsWith(".png") || figurename.EndsWith(".root"))c->SaveAs(figurename);
    else c->SaveAs(figurename + ".pdf");
    delete c;
    delete h;
    
    
    
  }

//----------------------------------------------------------------------  
  
  void drawVerticalLines(TH1* h,vector<double>& binning_x,vector<vector<double> >& binning_y,double ymin,double ymax,int lineColor,int lineStyle,double lineWidth){
    
    const int nbinsx = binning_x.size()-1;
    double xPosition=0.;
    
    auto line = std::make_shared<TLine>(0.5,0.1,0.5,0.9);
    line->SetLineColor(lineColor);
    line->SetLineStyle(lineStyle);
    line->SetLineWidth(lineWidth);
    
    int index = 0;
    for(int xbin=0;xbin<nbinsx-1;xbin++){
      const int nbinsy=binning_y[xbin].size()-1;
      index +=nbinsy;
      
      xPosition=h->GetXaxis()->GetBinUpEdge(index);
    
      line->SetX1(xPosition);
      line->SetX2(xPosition);
      line->SetY1(ymin);
      line->SetY2(ymax);
      line->DrawClone("same");
      
    }
    
    
  }

//----------------------------------------------------------------------
  
  void drawVerticalLinesTwoStyles(vector<TH1D*>& hist,vector<double>& binning_x,vector<vector<double> >& binning_y,double ymin,double ymax,int lineColor1,int lineStyle1,int lineColor2,int lineStyle2,double lineWidth){
    
    const int nbinsx = binning_x.size()-1;
    double xPosition=0.;
    
    const int nhistos=hist.size();
    
    auto line = std::make_shared<TLine>(0.5,0.1,0.5,0.9);
    line->SetLineWidth(lineWidth);
    
    int index = 0;
    for(int xbin=0;xbin<nbinsx-1;xbin++){
      const int nbinsy=binning_y[xbin].size()-1;
      index +=nbinsy;
      
      xPosition=hist[0]->GetXaxis()->GetBinUpEdge(index);
      
      double y2=0;
      for(int i=0;i<nhistos;i++){
        if(hist[i]->GetBinContent(index) > y2)y2=hist[i]->GetBinContent(index);
        if(hist[i]->GetBinContent(index+1) > y2)y2=hist[i]->GetBinContent(index+1);
      }
      
      
      line->SetLineColor(lineColor1);
      line->SetLineStyle(lineStyle1);
      line->SetX1(xPosition);
      line->SetX2(xPosition);
      line->SetY1(ymin);
      line->SetY2(y2);
      line->DrawClone("same");
      
      line->SetY1(y2*1.01);
      line->SetY2(ymax);
      line->SetLineColor(lineColor2);
      line->SetLineStyle(lineStyle2);
      line->DrawClone("same");
      
      
    }
    
    
  }
  
//----------------------------------------------------------------------
  
  void drawTicksX(TH1* h,vector<double>& binning_x,vector<vector<double> >& binning_y,double ymin,double ymax,double size,bool isLogScale){ // Will draw x-axis ticks on bin edges
    
    double xPosition=0;
    auto line = std::make_shared<TLine>(0.5,0.1,0.5,0.9);
    line->SetLineColor(kBlack);
    line->SetLineStyle(1);
    line->SetLineWidth(1);
    
    const int nbinsx=binning_x.size()-1;
        
    int index=0;
    
    for(int i=0;i<nbinsx;i++){
      const int nbinsy = binning_y[i].size()-1;
      for(int j=0;j<nbinsy;j++){
        index++;
        xPosition=h->GetXaxis()->GetBinUpEdge(index);
        
        double factor=1.0;
        if(j==nbinsy-1)factor=2.;
        
        line->SetX1(xPosition);
        line->SetX2(xPosition);
        
        line->SetY1(ymin);
        if(isLogScale)line->SetY2( exp( log(ymin) + size*factor*log(ymax/ymin)  )   ) ;
        else line->SetY2( ymin + (ymax - ymin)*size*factor );
        line->DrawClone("same");
        
        if(isLogScale)line->SetY1( exp( log(ymax) - size*factor*log(ymax/ymin)  ) );
        else line->SetY1(ymax - (ymax - ymin)*size*factor );
        line->SetY2(ymax);
        line->DrawClone("same");
      }
      
    }
  }

//----------------------------------------------------------------------

  void drawBinLabels2DConcatenated(TH1* hist,vector<double>& binning_x,vector<vector<double> >& binning_y,TString precision, TString precision2,TString variable1_name,double y1,double y2,const double size1,const double size2){
    
    drawBinLabels2DConcatenated_v1(hist,binning_x,binning_y,precision,precision2,variable1_name,y1,y2,size1,size2);
    //drawBinLabels2DConcatenated_v2(hist,binning_x,binning_y,precision,precision2,variable1_name,y1,y2,size1,size2);
    
  }

//----------------------------------------------------------------------
  
  void drawBinLabels2DConcatenated_v1(TH1* hist,vector<double>& binning_x,vector<vector<double> >& binning_y,TString precision, TString precision2,TString variable1_name,double y1,double y2,const double size1,const double size2){
    
    const int nbinsx=binning_x.size()-1;
    int index=0;
    
    TLatex label;
    
    if(y1==0. && y2==0.){
      y1 =  hist->GetYaxis()->GetBinLowEdge(1) - 0.02*(hist->GetYaxis()->GetXmax() - hist->GetYaxis()->GetXmin() );
      y2 = hist->GetYaxis()->GetBinLowEdge(1) - 0.13*(hist->GetYaxis()->GetXmax() - hist->GetYaxis()->GetXmin() );
    }
    
    double xpos=0.;
    
    TString binlabel="";
    for(int i=0;i<nbinsx;i++){
      const int nbinsy=binning_y[i].size()-1;
      
      if(i==0)binlabel=variable1_name+" #in " + Form("[%"+precision2+",%"+precision2+"]",binning_x[i],binning_x[i+1]);
      else binlabel=Form("[%"+precision2+",%"+precision2+"]",binning_x[i],binning_x[i+1]);
      xpos = (hist->GetXaxis()->GetBinLowEdge(index+1) + hist->GetXaxis()->GetBinUpEdge(index + nbinsy) )/2 - 0.*(hist->GetXaxis()->GetXmax() - hist->GetXaxis()->GetXmin() );
      
      label.SetTextAngle(0);
      label.SetTextSize(size2);
      label.SetTextAlign(23);
      label.DrawLatexNDC(xpos,y2,binlabel);
      
      for(int j=0;j<nbinsy;j++){
        index++;
        binlabel=Form("[%"+precision+",%"+precision+"]",binning_y[i][j],binning_y[i][j+1]);
        //hist->GetXaxis()->SetBinLabel(index,binlabel);
        
        xpos =  hist->GetXaxis()->GetBinCenter(index) + 0.2*hist->GetXaxis()->GetBinWidth(index);
        label.SetTextAngle(45);
        label.SetTextSize(size1);
        label.SetTextAlign(31);
        label.DrawLatexNDC(xpos,y1,binlabel);
        
        
      }
      
    }
    
    
  }
//----------------------------------------------------------------------

  void drawBinLabels2DConcatenated_v2(TH1* hist,vector<double>& binning_x,vector<vector<double> >& binning_y,TString precision, TString precision2,TString /*variable1_name*/,double y1,double y2,const double size1,const double size2){
    const int nbinsx=binning_x.size()-1;
    int index=0;
    
    TLatex label;
    
    if(y1==0. && y2==0.){
      y1 =  hist->GetYaxis()->GetBinLowEdge(1) - 0.065*(hist->GetYaxis()->GetXmax() - hist->GetYaxis()->GetXmin() );
      y2 = hist->GetYaxis()->GetBinLowEdge(1) - 0.13*(hist->GetYaxis()->GetXmax() - hist->GetYaxis()->GetXmin() );
    }
    
    double xpos=0.;
    
    TString binlabel="";
    for(int i=0;i<=nbinsx;i++){
      const int nbinsy=binning_y[i].size()-1;
      
      //if(i==0)binlabel=variable1_name+" " + Form("%"+precision2,binning_x[i]);
      //else binlabel=Form("%"+precision2,binning_x[i]);
      binlabel=Form("%"+precision2,binning_x[i]);
      xpos = hist->GetXaxis()->GetBinLowEdge(index+1) ;
      
      label.SetTextAngle(0);
      label.SetTextSize(size2);
      label.SetTextAlign(21);
      if(i==nbinsx)label.SetTextAlign(31);
      label.DrawLatex(xpos,y2,binlabel);
      
      if(i<nbinsx) for(int j=0;j<=nbinsy;j++){
        index++;
        if(i!=0 && j==0)continue;
        cout << "binning_y " << i << " " << j << " " << binning_y[i][j] << endl;
        binlabel=Form("%"+precision,binning_y[i][j]);
        if( (j==nbinsy) && ( (i+1) < nbinsy) ) binlabel = Form("%"+precision + "|%"+precision,binning_y[i][j],binning_y[i+1][0]);
        binlabel.ReplaceAll(" ","");
        //binlabel=" "+binlabel;
        
        //hist->GetXaxis()->SetBinLabel(index,binlabel);
        
        xpos =  hist->GetXaxis()->GetBinLowEdge(index);
        label.SetTextAngle(0);
        label.SetTextSize(size1);
        label.SetTextAlign(21);
        if(j==nbinsy && i==nbinsx-1)label.SetTextAlign(31);
        
        
        //if(j==nbinsy) binlabel+=" ]";
        //if(j==0) binlabel="["+binlabel;
        
        label.DrawLatex(xpos,y1,binlabel);
        
        if(j==nbinsy) index--;
      }
      
    }
    
    //xpos = hist->GetXaxis()->GetBinLowEdge(index+1);
    //label.SetTextAlign(11);
    //label.SetTextSize(size1);
    //label.DrawLatex(xpos,y1," p^{t2}_{T}");
    //label.SetTextSize(size2);
    //label.DrawLatex(xpos,y2," " +variable1_name);
    
    
  }
  
//----------------------------------------------------------------------

  void WriteGeneralInfo(TString cut_label, TString lumi, float size, float x, float y,float shift){
    
    TString label=NamesAndTitles::ATLASString(lumi);
    
    TString ToWrite="";
    TLatex l;
    l.SetNDC();
    l.SetTextFont(42);
    l.SetTextSize(size); 
    l.SetTextColor(1);
    //l.DrawLatex(x-0.005,y-0.07,cut_label.Data());
  
    l.DrawLatex(x,y,label);
    
    l.SetTextSize(0.9*size);
    ToWrite=NamesAndTitles::energyLumiString(lumi);
    l.DrawLatex(x,y-shift,ToWrite.Data());
    if(cut_label!="") l.DrawLatex(x,y-2.*shift,cut_label.Data());
    
  }
        
//----------------------------------------------------------------------	
        
  void WriteInfo(TString info, float size, float x, float y, int color){
    TLatex l;
    l.SetNDC();
    l.SetTextFont(42);
    l.SetTextSize(size); 
    l.SetTextColor(color);
    l.DrawLatex(x,y,info.Data());
  }

//----------------------------------------------------------------------

  void ATLASLabel(Double_t x,Double_t y,const char* text, float tsize, Color_t color){
    TLatex l; //l.SetTextAlign(12);
    if (tsize>0) l.SetTextSize(tsize); 
    l.SetNDC();
    l.SetTextFont(72);
    l.SetTextColor(color);
  
    //double delx = 0.115*696*gPad->GetWh()/(472*gPad->GetWw());
    double delx = 0.14;
  
    l.DrawLatex(x,y,"ATLAS");
    if (text) {
      TLatex p; 
      p.SetNDC();
      if (tsize>0) p.SetTextSize(tsize); 
      p.SetTextFont(42);
      p.SetTextColor(color);
      p.DrawLatex(x+delx,y,text);
      //    p.DrawLatex(x,y,"#sqrt{s}=900GeV");
    }
  }
  
}
