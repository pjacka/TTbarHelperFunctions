#include "TTbarHelperFunctions/TTBarConfig.h"
#include "TTbarHelperFunctions/functions.h"
#include "TTbarHelperFunctions/stringFunctions.h"

#include <exception>

//ClassImp(TTBarConfig)

TTBarConfig::TTBarConfig(const TString& configFileName){
  
  init(configFileName);
}



void TTBarConfig::init(const TString& configFileName){
  if (!functions::checkIfFileExists(configFileName.Data())){
    std::cerr << "Error in TTBarConfig::init: Config file doesn't exist. " << configFileName << std::endl;
  }
  m_env=std::make_unique<TEnv>(configFileName);
  
  readConfig();
  
}



void TTBarConfig::readConfig(){
  // Second parameters are default values 
  
  m_runReco = m_env->GetValue("runReco",true);
  m_runParticle = m_env->GetValue("runParticle",true);
  m_runParton = m_env->GetValue("runParton",true);
  
  m_useEWSF = m_env->GetValue("useEWSF",false);
  m_useTruth =	m_env->GetValue("useTruth",true);
  
  m_usePDF = m_env->GetValue("usePDF",false);
  m_doSignalModeling = m_env->GetValue("doSignalModeling",false);
  
  m_useRecoTruthMatching = m_env->GetValue("useRecoTruthMatching",false);
  m_normalizeToNominalCrossSection = m_env->GetValue("normalizeToNominalCrossSection",true);
  
  m_topPartonsDefinition = m_env->GetValue("TopPartonsDefinition","afterFSR");
  
  const std::string& FSRbranches = m_env->GetValue("FSRbranches","");
  vector<string> helpvec = functions::splitString(FSRbranches,";");
  for(const std::string& s : helpvec) {
    if(s=="") continue;
    vector<string> vec = functions::splitString(s,":");
    if(vec.size()==2) m_FSRbranches[vec[0]] = std::stoi(vec[1]);
    else {
      throw std::invalid_argument("Invalid argument in FSRbranches string: " + s);
    }
  }
  
  const std::string& ISRbranches = m_env->GetValue("ISRbranches","");
  helpvec = functions::splitString(ISRbranches,";");
  for(const std::string& s : helpvec) {
    if(s=="") continue;
    vector<string> vec = functions::splitString(s,":");
    vector<int> vecint;
    for(unsigned int i=1;i<vec.size();i++) vecint.push_back(std::stoi(vec[i]));
    if(vecint.empty()) {
      throw std::invalid_argument("Invalid argument in ISRbranches string: " + s);
    }
    m_ISRbranches[vec[0]]=vecint;
  }
  
  const std::string& PDFbranches = m_env->GetValue("PDFbranches","");
  helpvec = functions::splitString(PDFbranches,";");
  for(const std::string& s : helpvec) {
    if(s=="") continue;
    vector<string> vec = functions::splitString(s,":");
    if(vec.size()==2) {
      m_PDFbranches[vec[0]] = std::stoi(vec[1]);
    }
    else if(vec.size()==3) {
      int imin=std::stoi(vec[1]);
      int imax=std::stoi(vec[2]);
      if(imax<imin) throw std::invalid_argument("Invalid argument in PDFbranches string: " + s);
      for(int i=imin;i<=imax;i++) {
        m_PDFbranches[vec[0]+Form("%03d",i+1-imin)] = i;
      }
    }
    else {
      throw std::invalid_argument("Invalid argument in PDFbranches string: " + s);
    }
  }
  
  m_hardScatteringDirectory = m_env->GetValue("HardScatteringDirectory","ME");
  m_partonShoweringDirectory = m_env->GetValue("PartonShoweringDirectory","PS");
  m_PhPy8MECoffDirectory = m_env->GetValue("PhPy8MECoffDirectory","PhPy8MECoff");
  m_PhH704Directory = m_env->GetValue("PhH704Directory","PhH704");
  m_FSRupDirectory = m_env->GetValue("FSRupDirectory","ALPHAS_FSR_up");
  m_FSRdownDirectory = m_env->GetValue("FSRdownDirectory","ALPHAS_FSR_down");
  m_ISRupDirectory = m_env->GetValue("ISRupDirectory","IFSR_up");
  m_ISRdownDirectory = m_env->GetValue("ISRdownDirectory","IFSR_down");
  
  m_ISRmuRupDirectory = m_env->GetValue("ISRmuRupDirectory","");
  m_ISRmuRdownDirectory = m_env->GetValue("ISRmuRdownDirectory","");
  m_ISRmuFupDirectory = m_env->GetValue("ISRmuFupDirectory","");
  m_ISRmuFdownDirectory = m_env->GetValue("ISRmuFdownDirectory","");
  m_ISRVar3cUpDirectory = m_env->GetValue("ISRVar3cUpDirectory","");
  m_ISRVar3cDownDirectory = m_env->GetValue("ISRVar3cDownDirectory","");
  
  m_qcd_method = m_env->GetValue("qcd_method","");
  
  m_histos1D_config = m_env->GetValue("histos1D_config","");          
  m_histos2D_config = m_env->GetValue("histos2D_config","");          
  m_unfolding_histos_config = m_env->GetValue("unfolding_histos_config","");  
  m_unfolding_histosND_config = m_env->GetValue("unfolding_histosND_config","");
  m_useFineHistograms = m_env->GetValue("UseFineHistograms",false);
  m_fillClosureTestHistos = m_env->GetValue("fillClosureTestHistos",false);
  
  m_signalDSIDs = functions::MakeVectorInt(m_env->GetValue("signalDSIDs",""));
  m_singleTopTChanDSIDs = functions::MakeVectorInt(m_env->GetValue("SingleTopTChanDSIDs",""));
  m_WtSingleTopDSIDs = functions::MakeVectorInt(m_env->GetValue("WtSingleTopDSIDs",""));	
  m_ttWDSIDs = functions::MakeVectorInt(m_env->GetValue("ttWDSIDs",""));
  m_ttZDSIDs = functions::MakeVectorInt(m_env->GetValue("ttZDSIDs",""));
  m_ttHDSIDs = functions::MakeVectorInt(m_env->GetValue("ttHDSIDs",""));
  
  m_applyHtFilterCut = m_env->GetValue("applyHtFilterCut",false);	
  m_samplesToApplyHtFilterCut = functions::MakeVectorInt(m_env->GetValue("samplesToApplyHtFilterCut",""));
  m_HtFilterCutValue = m_env->GetValue("HtFilterCutValue",0.);
  
  m_useFilterEfficienciesCorrections = m_env->GetValue("useFilterEfficienciesCorrections",false); 
  m_filterEfficienciesCorrectionsFileName = m_env->GetValue("filterEfficienciesCorrectionsFileName","");
  
  m_sumWeightsFile = m_env->GetValue("SumWeightsFile","");
  
  const std::string& ljetType = m_env->GetValue("largeRJetType","");
  if(ljetType=="LJETS") m_largeRJetType=kLJET;
  else if(ljetType=="RCJETS") m_largeRJetType=kRCJET;
  else if(ljetType=="VarRCJETS") m_largeRJetType=kVarRCJET;
  else {
    cout << "Error: Unknown type of large-R jet, unable to run. Aborting!" << endl;
    cout << "large-R jet type: " << ljetType << endl;
    exit(-2);
  }
  
  m_useTrackJets = m_env->GetValue("useTrackJets",false); // Track jets not used by default
  
  m_useVarRCJets = m_env->GetValue("useVarRCJets",false); // Variable-R reclustered jets are not used by default
  
  m_useVarRCJetsSubstructure = m_env->GetValue("useVarRCJetsSubstructure",false); // Variable-R reclustered jets are not used by default
  
  m_varRCJetBranchName = m_env->GetValue("varRCJetBranchName","");
  
  m_varRCJetsHistString = m_env->GetValue("varRCJetsHistString","");
  
  m_useRCJets = m_env->GetValue("useRCJets",false); // Reclustered jets are not used by default
  
  m_useRCJetsSubstructure = m_env->GetValue("useRCJetsSubstructure",false);
  
  m_RCJetsHistString = m_env->GetValue("RCJetsHistString","");
  
  m_bTaggingInfoCarriers = m_env->GetValue("bTaggingInfoCarriers","");
  
  m_bTaggerName = m_env->GetValue("bTaggerName","");
  
  m_topTaggerName = m_env->GetValue("topTaggerName","");
  m_useTopTaggerSF = m_env->GetValue("useTopTaggerSF",false);
  
  m_useTriggers = m_env->GetValue("useTriggers",false);
  
  m_recoLevelToolsLoaderName = m_env->GetValue("RecoLevelToolsLoaderName","");
  m_recoLevelToolsLoaderConfigString = m_env->GetValue("RecoLevelToolsLoaderConfigString","");
  
  m_particleLevelToolsLoaderName = m_env->GetValue("ParticleLevelToolsLoaderName","");
  m_particleLevelToolsLoaderConfigString = m_env->GetValue("ParticleLevelToolsLoaderConfigString","");
  
  m_partonLevelToolsLoaderName = m_env->GetValue("PartonLevelToolsLoaderName","");
  m_partonLevelToolsLoaderConfigString = m_env->GetValue("PartonLevelToolsLoaderConfigString","");
  
  // Event reconstruction tool settings
  m_recoToolConfigString = m_env->GetValue("RecoToolConfigString","");
  
  // Classification tool settings
  m_eventClassificationToolConfigString = m_env->GetValue("EventClassificationToolConfigString","");
  
  // ---------------------------------------------------
  // Event selection
  // ---------------------------------------------------
  // Selection tool settings
  m_eventSelectionToolConfigString = m_env->GetValue("EventSelectionToolConfigString","");
  
  m_partonLevelSelectionToolName = m_env->GetValue("PartonLevelSelectionToolName","");
  m_partonLevelSelectionToolConfigString = m_env->GetValue("PartonLevelSelectionToolConfigString","");
  
  
  // ------------------------------------------------------------
  // Object Selection
  
  // Small-R jets
  m_jet_eta_max     = m_env->GetValue("jet_eta_max",2.5);
  m_jet_pt_min      = m_env->GetValue("jet_pt_min",25000.);
  
  // Large-R jets
  m_ljet_eta_max     = m_env->GetValue("ljet_eta_max",2.0);
  m_ljet_m_min       = m_env->GetValue("ljet_m_min",50000.);
  m_ljet_pt_min      = m_env->GetValue("ljet_pt_min",250000.);
  m_ljet_pt_max      = m_env->GetValue("ljet_pt_max",3000000.);
  m_ljet_mOverPt_max = m_env->GetValue("ljet_mOverPt_max",1.0);
  m_ljet_particle_useRapidity = m_env->GetValue("ljet_particle_useRapidity",false);
  
  // Reclustered jets
  m_rcjet_eta_max     = m_env->GetValue("rcjet_eta_max",2.0);
  m_rcjet_m_min       = m_env->GetValue("rcjet_m_min",0.);
  m_rcjet_pt_min      = m_env->GetValue("rcjet_pt_min",250000.);
  m_rcjet_pt_max      = m_env->GetValue("rcjet_pt_max",3000000.);
  m_rcjet_mOverPt_max = m_env->GetValue("rcjet_mOverPt_max",1.0);
  
  // Variable-R reclustered jets
  m_vrcjet_eta_max     = m_env->GetValue("vrcjet_eta_max",2.0);
  m_vrcjet_m_min       = m_env->GetValue("vrcjet_m_min",0.);
  m_vrcjet_pt_min      = m_env->GetValue("vrcjet_pt_min",250000.);
  m_vrcjet_pt_max      = m_env->GetValue("vrcjet_pt_max",3000000.);
  m_vrcjet_mOverPt_max = m_env->GetValue("vrcjet_mOverPt_max",1.0);
  
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  
  
  // --------------------------------------------------------------------
  m_lumi = m_env->GetValue("lumi",0.);
  m_lumi_uncert = m_env->GetValue("lumi_uncert",0.);
  m_lumi_string = m_env->GetValue("lumi_string","");
  
  m_lumi_data1516 = m_env->GetValue("lumi_data1516",0.);
  m_lumi_data1516_uncert = m_env->GetValue("lumi_data1516_uncert",0.);
  m_lumi_data1516_string = m_env->GetValue("lumi_data1516_string","");
  
  m_lumi_data17 = m_env->GetValue("lumi_data17",0.);
  m_lumi_data17_uncert = m_env->GetValue("lumi_data17_uncert",0.);
  m_lumi_data17_string = m_env->GetValue("lumi_data17_string","");
  
  m_lumi_data18 = m_env->GetValue("lumi_data18",0.);
  m_lumi_data18_uncert = m_env->GetValue("lumi_data18_uncert",0.);
  m_lumi_data18_string = m_env->GetValue("lumi_data18_string","");
  
  m_lumi_dataAllYears = m_env->GetValue("lumi_dataAllYears",0.);
  m_lumi_dataAllYears_uncert = m_env->GetValue("lumi_dataAllYears_uncert",0.);
  m_lumi_dataAllYears_string = m_env->GetValue("lumi_dataAllYears_string","");
  // --------------------------------------------------------------------
  
  m_ttbar_SF = m_env->GetValue("ttbar_SF",0.);
  m_factor_to_ttbar_cross_section = m_env->GetValue("factor_to_ttbar_cross_section",0.);
  
  m_samplesXsectionsFile = m_env->GetValue("samplesXsectionsFile","");
  
  // Reweighting histos configuration
  std::vector<int> vec = functions::MakeVectorInt(m_env->GetValue("reweighting",""));
  const int vecSize = vec.size();
  bool individualPoints = (vecSize == 1) || (vecSize > 2) || (vecSize == 2 && vec[0] > vec[1]);
  if(individualPoints) {
    m_reweightingIndexes = vec;
  }
  else {
    for(int i = vec[0] ; i <= vec[1];i++) m_reweightingIndexes.push_back(i);
  }
  std::sort( m_reweightingIndexes.begin(), m_reweightingIndexes.end() );
  m_reweightingIndexes.erase( std::unique( m_reweightingIndexes.begin(), m_reweightingIndexes.end() ), m_reweightingIndexes.end() );
  
  m_reweightingHistosFile = m_env->GetValue("reweightingHistosFile","");
  
  
  // Defines MC samples production (MC16a,MC16d,MC16e or All)
  m_mc_samples_production = m_env->GetValue("mc_samples_production","");

}
